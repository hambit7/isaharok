@extends('layouts.master')

@section('content')
<?php
$baseurl = URL::to('/');
?>
 <div class="main_content">
        <div class="sub_content">
        <div class="col-md-12 margin_top30">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.iOS App Configuration')</div>
                <div class="panel-body">

                    {!! Form::model($sitesettings, [
                        'method' => 'post',
                        'url' => [$baseurl.'/admin/mobileappconfig'],
                        'class' => 'form-horizontal',
                        'files' => true
                        ]) !!}

                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                        <div class="form-group {{ $errors->has('iossandbox') ? 'has-error' : ''}}">
                         {!! Form::label('iossandbox', Lang::get('app.iOS Sandbox Key'), ['class' => 'col-md-4 control-label']) !!}
                         <div class="col-md-6">
                            {!! Form::file('iossandbox', ['class' => 'filestyle','name' => 'iossandbox', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
                            {!! $errors->first('iossandbox', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('iosproduction') ? 'has-error' : ''}}">
                     {!! Form::label('iosproduction', Lang::get('app.iOS Production Key'), ['class' => 'col-md-4 control-label']) !!}
                     <div class="col-md-6">
                        {!! Form::file('iosproduction', ['class' => 'filestyle','name' => 'iosproduction', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
                        {!! $errors->first('iosproduction', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('ios_version') ? 'has-error' : ''}}">
                    {!! Form::label('ios_version', Lang::get('app.iOS App Version'), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('ios_version', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('ios_version', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('Update') ? 'has-error' : ''}}">
                    {!! Form::label('Update Type', Lang::get('app.Update Type'), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <div class="radio i-checks">
                            <label>
                                {!! Form::radio('ios_update','0', 1) !!}
                                <i></i>
                                Normal Update
                            </label>
                        </div>
                        <div class="radio i-checks">
                            <label>
                                {!! Form::radio('ios_update', '1', 0) !!}
                                <i></i>
                                Force Update
                            </label>
                        </div>
                        {!! $errors->first('ios_update', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group">
                 <div class="col-md-offset-4 col-md-4">
                    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>
</div>
</div>
@endsection