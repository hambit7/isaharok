@extends('layouts.master')

@section('content')
<?php
$baseurl = URL::to('/');
if(isset($search) && !empty($search))
{
    $search = $search;
}
else
{
    $search = "";
}
?>
<head>
 <style type="text/css">
 .centerText{
   text-align: center;
}
</style>
</head>
 <div class="main_content">
        <div class="sub_content">
        <div class="col-md-12 margin_top30">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.Help Pages')</div>
                <div class="panel-body">

                    

                    <a href="{{ url($baseurl.'/admin/helppages/create') }}" class="btn btn-primary btn-md" title="Add New Help Page" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>  @lang('app.Add Help Page')</a>

                    <br/>
                    <br/>

                    <div class="table-responsive clears">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>ID</th><th> @sortablelink(Lang::get('app.Title')) </th><th>@lang('app.Actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count=count($helppages);
                                if($count>0){ ?>
                                    @foreach($helppages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/helppages/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Help page"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/helppages', $item->id],
                                                'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Help Page" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Help page',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    <?php }
                                    else
                                        echo '<tr><td colspan="4" class="centerText">'.Lang::get('app.Sorry...! No Help Pages found').'.</td></tr>';
                                    ?>

                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $helppages->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection