@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Edit Help Page') "{{ $helppages->title }}"</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul-->
                        @endif

                        {!! Form::model($helppages, [
                            'method' => 'PATCH',
                            'url' => ['/admin/helppages', $helppages->id],
                            'class' => 'form-horizontal',
                            'files' => true,
							'onsubmit'=> 'return loadDescription();'
                        ]) !!}

                        @include ('admin.helppages.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection