@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    if (isset($search) && !empty($search)) {
        $search = $search;
    } else {
        $search = "";
    }
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Unapproved Users')</div>
                    <div class="panel-body">

                    <!--a href="{{ url('/admin/users/create') }}" class="btn btn-primary btn-xs" title="Add New User"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a-->

                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ url($baseurl.'/admin/unapproved') }}">
                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search;?>"
                                           class="input-sm form-control" placeholder="@lang('app.Search')">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="submit">@lang('app.Go!')</button>
                                </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>@lang('app.ID')</th>
                                    <th> @sortablelink(Lang::get('app.Firstname'),Lang::get('app.First Name'))</th>
                                    <th> @sortablelink(Lang::get('app.Email'))</th>
                                    <th>@lang('app.View')</th>
                                    <th>@lang('app.Photo')</th>
                                    <th>@lang('app.gesture-photo')</th>
                                    <th> @lang('app.Actions') </th>
                                </tr>
                                </thead>
                                <tbody>  <?php
                                $i = 1;
                                $noofusers = count($users);
                                if($noofusers > 0){ ?>

                                @foreach($users as $item)
                                    <tr id="<?php echo $item->id;?>">
                                        <td>{{ $i }}</td>
                                        <td>{{ $item->firstname }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/viewunapproveduser/' . $item->id) }}"
                                               class="btn btn-success btn-xs" title="View Unapproved Users"><span
                                                        class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                        </td>

                                        <td>
                                            @if ($item->gender == 'female')
                                                <a href="{{ url($baseurl.'/admin/gallery/' . $item->id) }}"
                                                   class="btn btn-success btn-xs" title="View girl photo"><span
                                                            class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/content/' . $item->id) }}"
                                               class="btn btn-success btn-xs" title="View  gesture photo"><span
                                                        class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                        </td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/changestatus/' . $item->id.'/'.'1') }}"
                                               title="Enable User"><input type="submit" class="btn btn-s-md btn-success"
                                                                          value="Enable"></button></a>
                                        </td>
                                        <td id="deleting">

                                            <div>
                                                <button data-user-delete="{{$item->id}}" class="btn btn-danger"
                                                        id="delete">Delete
                                                </button>
                                            </div>
                                        </td>
                                    <!--td>
                                            <a href="{{ url('/admin/users/' . $item->id) }}" class="btn btn-success btn-xs" title="View User"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit User"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/users', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete User',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                            </td-->
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                <?php }
                                else
                                    echo '<tr><td colspan="4" class="centerText">Sorry...! No User is found.</td></tr>';
                                ?>

                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $users->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
