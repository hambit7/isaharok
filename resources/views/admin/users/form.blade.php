<div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
    {!! Form::label('firstname', 'Firstname', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
        {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
    {!! Form::label('username', 'Username', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('username', null, ['class' => 'form-control']) !!}
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('birthday') ? 'has-error' : ''}}">
    {!! Form::label('birthday', 'Birthday', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('birthday', null, ['class' => 'form-control']) !!}
        {!! $errors->first('birthday', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('age') ? 'has-error' : ''}}">
    {!! Form::label('age', 'Age', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('age', null, ['class' => 'form-control']) !!}
        {!! $errors->first('age', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
    {!! Form::label('latitude', 'Latitude', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
        {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
    {!! Form::label('longitude', 'Longitude', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
        {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('peoplefor') ? 'has-error' : ''}}">
    {!! Form::label('peoplefor', 'Peoplefor', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('peoplefor', null, ['class' => 'form-control']) !!}
        {!! $errors->first('peoplefor', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('userstatus') ? 'has-error' : ''}}">
    {!! Form::label('userstatus', 'Userstatus', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('userstatus', null, ['class' => 'form-control']) !!}
        {!! $errors->first('userstatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('premiumstatus') ? 'has-error' : ''}}">
    {!! Form::label('premiumstatus', 'Premiumstatus', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('premiumstatus', null, ['class' => 'form-control']) !!}
        {!! $errors->first('premiumstatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('facebookid') ? 'has-error' : ''}}">
    {!! Form::label('facebookid', 'Facebookid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('facebookid', null, ['class' => 'form-control']) !!}
        {!! $errors->first('facebookid', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('profileimage') ? 'has-error' : ''}}">
    {!! Form::label('profileimage', 'Profileimage', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('profileimage', null, ['class' => 'form-control']) !!}
        {!! $errors->first('profileimage', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('visitors') ? 'has-error' : ''}}">
    {!! Form::label('visitors', 'Visitors', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('visitors', null, ['class' => 'form-control']) !!}
        {!! $errors->first('visitors', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('lastsearch') ? 'has-error' : ''}}">
    {!! Form::label('lastsearch', 'Lastsearch', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('lastsearch', null, ['class' => 'form-control']) !!}
        {!! $errors->first('lastsearch', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('agestatus') ? 'has-error' : ''}}">
    {!! Form::label('agestatus', 'Agestatus', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('agestatus', null, ['class' => 'form-control']) !!}
        {!! $errors->first('agestatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('distancestatus') ? 'has-error' : ''}}">
    {!! Form::label('distancestatus', 'Distancestatus', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('distancestatus', null, ['class' => 'form-control']) !!}
        {!! $errors->first('distancestatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('invisiblestatus') ? 'has-error' : ''}}">
    {!! Form::label('invisiblestatus', 'Invisiblestatus', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('invisiblestatus', null, ['class' => 'form-control']) !!}
        {!! $errors->first('invisiblestatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('adsstatus') ? 'has-error' : ''}}">
    {!! Form::label('adsstatus', 'Adsstatus', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('adsstatus', null, ['class' => 'form-control']) !!}
        {!! $errors->first('adsstatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('notifications') ? 'has-error' : ''}}">
    {!! Form::label('notifications', 'Notifications', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('notifications', null, ['class' => 'form-control']) !!}
        {!! $errors->first('notifications', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('gender', null, ['class' => 'form-control']) !!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
