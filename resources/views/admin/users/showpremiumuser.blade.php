@extends('layouts.master')

@section('content')
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Premium User')</div>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <!--  <tr>
                                        <th>ID</th><td>{{ $user->id }}</td>
                                    </tr> -->
                            @if ($user->gender == 'male')

                                <tr>

                                    <th>Photo</th>
                                    <td><img src=" {{ url('/uploads/user/' .$user->profileimage) }} " width="200"
                                             height="250"></td>
                                </tr>
                            @endif
                            <tr>
                                <th>First name</th>
                                <td>{{ $user->firstname }}</td>
                            </tr>
                            <tr>
                                <th>User name</th>
                                <td>{{ $user->username }}</td>
                            </tr>
                            <tr>
                                <th>Email ID</th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <th>Age</th>
                                <td>{{ $user->age }}</td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td>{{ $user->location }}</td>
                            </tr>
                            <tr>
                                <th>Premium Name</th>
                                <td>{{ $premium->premiumname }}</td>
                            </tr>
                            <tr>
                                <th>No of days</th>
                                <td>{{ $premium->noofdays }}</td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>{{ $premium->price }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
