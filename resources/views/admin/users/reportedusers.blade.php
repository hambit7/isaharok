@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    if (isset($search) && !empty($search)) {
        $search = $search;
    } else {
        $search = "";
    }
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Reported Users')</div>
                    <div class="panel-body">

                    <!--a href="{{ url('/admin/users/create') }}" class="btn btn-primary btn-xs" title="Add New User"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a-->

                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ url($baseurl.'/admin/reportedusers') }}">
                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search;?>"
                                           class="input-sm form-control" placeholder="@lang('app.Search')">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="submit">@lang('app.Go!')</button>
                                </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>@lang('app.ID')</th>
                                    <th> @sortablelink(Lang::get('app.Firstname'),Lang::get('app.First Name'))</th>
                                    <th> @lang('app.Reported Count') </th>
                                    <th>@lang('app.Actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $noofusers = count($users);
                                $i = 1;
                                if($noofusers > 0){ ?>

                                @foreach($users as $item)
                                    <tr id="<?php echo $item->reportuserid;?>">
                                        <td>{{ $i }}</td>
                                        <td>{{ $item->getReportedusers() }}</td>
                                        <td>{{ $item->getUsers() }}</td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/changestatus/' . $item->reportuserid.'/'.'0') }}"
                                               title="Disable User"><input type="submit"
                                                                           class="btn btn-s-md btn-warning"
                                                                           value="Disable"></button></a>

                                            <a href="{{ url($baseurl.'/admin/ignorereporting/' . $item->reportuserid) }}"
                                               title="Ignore"><input type="submit" class="btn btn-s-md btn-warning"
                                                                     value="Ignore"></button></a>
                                        </td>

                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                <?php }
                                else
                                    echo '<tr><td colspan="4" class="centerText">Sorry...! No User is found.</td></tr>';
                                ?>

                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $users->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
