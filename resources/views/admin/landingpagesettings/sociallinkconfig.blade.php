@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Social Apps Links')</div>
                    <div class="panel-body">

                        {!! Form::model($landingpagesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatelandingpage'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                       <div class="form-group {{ $errors->has('facebooklink') ? 'has-error' : ''}}">
							{!! Form::label('facebooklink', Lang::get('app.Facebook Link'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('facebooklink', null, ['class' => 'form-control']) !!}
								{!! $errors->first('facebooklink', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('googlelink') ? 'has-error' : ''}}">
							{!! Form::label('googlelink', Lang::get('app.Google+ Link'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('googlelink', null, ['class' => 'form-control']) !!}
								{!! $errors->first('googlelink', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('twitterlink') ? 'has-error' : ''}}">
							{!! Form::label('twitterlink', Lang::get('app.Twitter Link'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('twitterlink', null, ['class' => 'form-control']) !!}
								{!! $errors->first('twitterlink', '<p class="help-block">:message</p>') !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(Lang::get('app.Update'), ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection