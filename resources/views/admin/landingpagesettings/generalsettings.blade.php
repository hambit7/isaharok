@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.General Settings')</div>
                    <div class="panel-body">

                        {!! Form::model($landingpagesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatelandingpage'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                       <div class="form-group {{ $errors->has('landingpagetitle') ? 'has-error' : ''}}">
							{!! Form::label('landingpagetitle', Lang::get('app.Landing Page Title'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('landingpagetitle', null, ['class' => 'form-control']) !!}
								{!! $errors->first('landingpagetitle', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('landingpagesubtitle') ? 'has-error' : ''}}">
							{!! Form::label('landingpagesubtitle', Lang::get('app.Landing Page Sub-title'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('landingpagesubtitle', null, ['class' => 'form-control']) !!}
								{!! $errors->first('landingpagesubtitle', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('copyrightinfo') ? 'has-error' : ''}}">
							{!! Form::label('copyrightinfo', Lang::get('app.Copy Rights Information'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('copyrightinfo', null, ['class' => 'form-control']) !!}
								{!! $errors->first('copyrightinfo', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('landingpagelogo') ? 'has-error' : ''}}">
							{!! Form::label('landingpagelogo', Lang::get('app.Landing Page Logo'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('landingpagelogo', ['class' => 'filestyle','name' => 'landingpagelogo', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
								{!! $errors->first('landingpagelogo', '<p class="help-block">:message</p>') !!}
								<?php if($landingpagesettings->landingpagelogo != "") ?>
								<div><img src="<?php echo url('/uploads/landingpage/'.$landingpagesettings->landingpagelogo); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;background-color: #ededed;"></div>
							</div>
						</div>

						<div class="form-group {{ $errors->has('subpagelogo') ? 'has-error' : ''}}">
							{!! Form::label('subpagelogo', Lang::get('app.Inner Page Logo'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('subpagelogo', ['class' => 'filestyle','name' => 'subpagelogo', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
								{!! $errors->first('subpagelogo', '<p class="help-block">:message</p>') !!}
								<?php if($landingpagesettings->subpagelogo != "") ?>
								<div><img src="<?php echo url('/uploads/landingpage/'.$landingpagesettings->subpagelogo); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;background-color: #ededed;"></div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(Lang::get('app.Update'), ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection