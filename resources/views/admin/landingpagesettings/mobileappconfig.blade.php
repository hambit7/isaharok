@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Appstore Links')</div>
                    <div class="panel-body">

                        {!! Form::model($landingpagesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatelandingpage'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                       <div class="form-group {{ $errors->has('iosapplink') ? 'has-error' : ''}}">
							{!! Form::label('iosapplink', Lang::get('app.iOS app link'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('iosapplink', null, ['class' => 'form-control']) !!}
								{!! $errors->first('iosapplink', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('androidapplink') ? 'has-error' : ''}}">
							{!! Form::label('androidapplink', Lang::get('app.Android app link'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('androidapplink', null, ['class' => 'form-control']) !!}
								{!! $errors->first('androidapplink', '<p class="help-block">:message</p>') !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(Lang::get('app.Update'), ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection