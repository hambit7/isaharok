@extends('layouts.master')

@section('content')
<?php
$baseurl = URL::to('/');
?>
 <div class="main_content">
        <div class="sub_content">
        <div class="col-md-12 margin_top30">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.Edit Interest') </div>
                <div class="panel-body">
                    {!! Form::model($interests, [
                        'method' => 'PATCH',
                        'url' => ['/admin/interests', $interests->id],
                        'class' => 'form-horizontal'
                        ]) !!}

                        @include ('admin.interests.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection