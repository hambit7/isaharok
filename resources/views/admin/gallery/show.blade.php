@extends('layouts.master')

@section('content')
    <?php

    $baseurl = URL::to('/');
    if (isset($search) && !empty($search)) {
        $search = $search;
    } else {
        $search = "";
    }
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Gallery')</div>
                    <div class="panel-body">

                        <div class="home">

                            <div class="demo-gallery">
                                <ul id="lightgallery" class="list-unstyled row">
                                    @foreach ($images as $key=>$image)
                                        <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="{{$image}}"
                                            data-src="{{$image}}"
                                            data-sub-html="<h4>{{$model->firstname}}</h4><p>Image #{{$key + 1}}</p>">
                                            <a href="">
                                                <img class="img-responsive" src="{{$image}}" alt="Thumb-1">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js"></script>
                            <script src="https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js"></script>
                            <script>
                                lightGallery(document.getElementById('lightgallery'));
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection



