@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Add Relationship')</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul-->
                        @endif

                        {!! Form::open(['url' => '/admin/peoplefor/index', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin.peoplefor.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection