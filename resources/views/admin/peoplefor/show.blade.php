@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');

        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Relationship Details') </div>
                    <div class="panel-body">

                        <a href="{{ url($baseurl.'/admin/peoplefor/' . $peoplefor->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Peoplefor"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => [$baseurl.'/admin/peoplefor/'. $peoplefor->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Peoplefor',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $peoplefor->id }}</td>
                                    </tr>
                                    <tr><th> @lang('app.Name') </th><td> {{ $peoplefor->name }} </td></tr><tr><th> @lang('app.Image Name') </th><td> {{ HTML::image('/uploads/'.$peoplefor->imagename, 'a picture', array('class' => 'thumbcls1')) }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection