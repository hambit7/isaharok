@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        if(isset($search) && !empty($search))
        {
            $search = $search;
        }
        else
        {
            $search = "";
        }
        ?>
          <head>
 <style type="text/css">
   .centerText{
       text-align: center;
    }
 </style>
</head>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Manage Relationship')</div>
                    <div class="panel-body">

                        <a href="{{ url($baseurl.'/admin/peoplefor/create') }}" class="btn btn-primary btn-md" title="Add New Peoplefor">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"/></span> @lang('app.Add Relationship')</a>
                        <br/>
                        <br/>
                            <div class="col-sm-4 m-b-xs no-h-padding">

                <form class="form-horizontal" role="form" method="POST" action="{{ url($baseurl.'/admin/peoplefor') }}">

                       <div class="input-group">
                        <input type="text" name="search" value="<?php echo $search;?>" class="input-sm form-control" placeholder="@lang('app.Search')">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="submit">@lang('app.Go!')</button>
                        </span>
                      </div>
                        </form>
                    </div>

                        <div class="table-responsive clears">
                            <table class="table table-striped m-b-none table-borderless" data-ride="datatables">
                                <thead>
                                    <tr>
                                        <th>@lang('app.ID')</th><th> @sortablelink(Lang::get('app.Name')) </th><th> @lang('app.Image Name') </th><th>@lang('app.Actions')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                               $count=count($peoplefor);

                               if($count>0){ ?>
                                @foreach($peoplefor as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ HTML::image('/uploads/'.$item->imagename, 'a picture', array('class' => 'thumbcls')) }}</td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/peoplefor/' . $item->id) }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url($baseurl.'/admin/peoplefor/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/peoplefor', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Peoplefor" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Peoplefor',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                 <?php
                      }
                      else
                        echo '<tr><td colspan="4"class="centerText">'.Lang::get('app.Sorry...! No Relationship found.').'.</td></tr>';?>
                                </tbody>
                            </table>

                            <div class="pagination-wrapper"> {!! $peoplefor->render() !!} </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection