@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Edit Relationship') </div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul-->
                        @endif

                        {!! Form::model($peoplefor, [
                            'method' => 'PATCH',
                            'url' => ['/admin/peoplefor', $peoplefor->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.peoplefor.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection