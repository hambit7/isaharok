@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Create new photo')</div>
                    <div class="panel-body">

                    @if ($errors->any())
                        <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                                @endforeach
                                </ul-->
                        @endif

                        <form method="POST" enctype="multipart/form-data" action="{{url('admin/content')}}" class="">


                            @include ('admin.content.form')

                            <div class="field">
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
