@extends('layouts.master')

@section('content')
    <?php

    $baseurl = URL::to('/');
    if (isset($search) && !empty($search)) {
        $search = $search;
    } else {
        $search = "";
    }
    ?>


    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <a class="btn btn-success" href="/admin/content/create">Add photo</a>

                <div class="panel panel-default">
                    {{--                    <div class="panel-heading">@lang('app.Gallery')</div>--}}
                    <div class="panel-body">
                        <div class="home">

                            <div class="demo-gallery">
                                <ul id="lightgallery" class="list-unstyled row">
                                    @foreach ($images as $key=>$image)
                                        <li class="col-xs-6 col-sm-4 col-md-3"
                                            data-responsive="{{\App::make('url')->to('/') . $image->url}}"
                                            data-src="{{\App::make('url')->to('/') . $image->url}}"
                                            data-sub-html="<h4>{{$image->id}}</h4><p>Image #{{$key + 1}}</p>">
                                            <a href="">
                                                <img class="img-responsive"
                                                     src="{{\App::make('url')->to('/') . $image->url}}"
                                                     alt="Image #{{$key + 1}}">
                                            </a>
                                        </li>

                                    @endforeach
                                </ul>
                            </div>
                            <div>
                                <form action="{{url('admin/content')}}" method="POST" enctype="multipart/form-data"
                                      class="form-group-lg">
                                    {!! method_field('delete') !!}
                                    {!! csrf_field() !!}
                                    <select name="id" id="images-for-deleting" class="form-group">
                                        <option id="selected-image" selected="selected">
                                            Choose.....
                                        </option>
                                        @foreach($images as $key=>$image)
                                            <option class="form-control" value="{{$image->id}}">Image
                                                #{{$key + 1}}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" id="delete-image" class="btn btn-success" disabled>Delete
                                    </button>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js"></script>
        <script>
            lightGallery(document.getElementById('lightgallery'));
        </script>
@endsection



