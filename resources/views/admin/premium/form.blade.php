<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', Lang::get('app.Price'), ['class' => 'col-md-4 control-label']) !!}

    <div class="col-md-5">
        {!! Form::text('price', null, ['class' => 'form-control','style'=>'float:left;']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-1 currencycls"><?php echo $currency;?></div>
</div>

<div class="form-group {{ $errors->has('premiumname') ? 'has-error' : ''}}">
    {!! Form::label('premiumname', Lang::get('app.Name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('premiumname', null, ['class' => 'form-control']) !!}
        {!! $errors->first('premiumname', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('noofdays') ? 'has-error' : ''}}">
    {!! Form::label('noofdays', Lang::get('app.No of days'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('noofdays', null, ['class' => 'form-control','min' => '1']) !!}
        {!! $errors->first('noofdays', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('noofdays') ? 'has-error' : ''}}">
    {!! Form::label('market_id', Lang::get('app.market_id'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('market_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('market_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('platform') ? 'has-error' : ''}}">
    {!! Form::label('platform', Lang::get('app.Platform'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">

        <div class="radio i-checks">
            <label>
                {!! Form::radio('platform','android', 1) !!}
                <i></i>
                Android
            </label>
        </div>
        <div class="radio i-checks">
            <label>
                {!! Form::radio('platform', 'ios', 0) !!}
                <i></i>
                iOS
            </label>
        </div>
        {!! $errors->first('platform', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
