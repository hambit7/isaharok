@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    if (isset($search) && !empty($search)) {
        $search = $search;
    } else {
        $search = "";
    }
    ?>
    <head>
        <style type="text/css">
            .centerText {
                text-align: center;
            }
        </style>
    </head>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Premium')</div>
                    <div class="panel-body">

                        <a href="{{ url($baseurl.'/admin/premium/create') }}"
                           class="btn btn-primary btn-md" title="Add New Premium"><span class="glyphicon glyphicon-plus"
                                                                                        aria-hidden="true"/></span> @lang('app.Add New Premium')
                        </a>
                        <br/>
                        <br/>

                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ url($baseurl.'/admin/premium') }}">

                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search;?>"
                                           class="input-sm form-control" placeholder="@lang('app.Search')">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <span class="input-group-btn">
                          <button class="btn btn-sm btn-default" type="submit">@lang('app.Go!')</button>
                        </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive clears">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th> @sortablelink(Lang::get('app.Premiumname'),)</th>
                                    <th> @sortablelink(Lang::get('app.Price'),)</th>
                                    <th> @sortablelink(Lang::get('app.Noofdays'),)</th>
                                    <th>@lang('app.Platform')</th>
                                    <th>@lang('app.Actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $count = count($premium);
                                if($count > 0){ ?>
                                @foreach($premium as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->premiumname}}</td>
                                        <td><?php echo $currency;?>{{ $item->price }}</td>
                                        <td>{{ $item->noofdays }}</td>
                                        <td>{{ $item->platform }}</td>
                                        <td>
                                            <a href="{{ url($baseurl.'/admin/premium/' . $item->id) }}"
                                               class="btn btn-success btn-xs" title="View Premium"><span
                                                        class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url($baseurl.'/admin/premium/' . $item->id . '/edit') }}"
                                               class="btn btn-primary btn-xs" title="Edit Premium"><span
                                                        class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {{--{!! Form::open([--}}
                                            {{--'method'=>'DELETE',--}}
                                            {{--'url' => ['/admin/premium', $item->id],--}}
                                            {{--'style' => 'display:inline'--}}
                                            {{--]) !!}--}}
                                            {{--{!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Premium" />', array(--}}
                                            {{--'type' => 'submit',--}}
                                            {{--'class' => 'btn btn-danger btn-xs',--}}
                                            {{--'title' => 'Delete Premium',--}}
                                            {{--'onclick'=>'return confirm("Confirm delete?")'--}}
                                            {{--)) !!}--}}
                                            {{--{!! Form::close() !!}--}}
                                        </td>
                                        <td id="deleting">

                                            <div>
                                                <button data-user-premium-delete="{{$item->id}}" class="btn btn-danger"
                                                        id="deletepremium">Delete
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <?php }
                                else
                                    echo '<tr><td colspan="4" class="centerText">' . Lang::get('app.Sorry...! No Premium is found') . '.</td></tr>';
                                ?>

                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $premium->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

