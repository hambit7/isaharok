@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Create New Premium')</div>
                    <div class="panel-body">

                    @if ($errors->any())
                        <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                                @endforeach
                                </ul-->
                        @endif

                        {!! Form::open(['url' => '/admin/premium/index', 'class' => 'form-horizontal','onsubmit'=> 'return premiumcheck();' ,'files' => true]) !!}

                        @include ('admin.premium.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
