@extends('layouts.master')

@section('content')
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Premium Details') </div>
                    <div class="panel-body">

                        <a href="{{ url('admin/premium/' . $premium->id . '/edit') }}" class="btn btn-primary btn-xs"
                           title="Edit Premium"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/premium', $premium->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete Premium',
                                'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $premium->id }}</td>
                                </tr>
                                <tr>
                                    <th> @lang('app.Price') </th>
                                    <td> <?php echo $currency;?>{{ $premium->price }} </td>
                                </tr>
                                <tr>
                                    <th> @lang('app.No of days') </th>
                                    <td> {{ $premium->noofdays }} </td>
                                </tr>
                                <tr>
                                    <th> @lang('app.Platform') </th>
                                    <td> {{ $premium->platform }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
