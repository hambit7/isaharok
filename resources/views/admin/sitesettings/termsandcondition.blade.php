@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Terms and Condition Settings')</div>
                    <div class="panel-body">

						<?php //echo "<pre>";print_r($errors);echo "</pre>"; ?>
                        {!! Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

						<div class="form-group {{ $errors->has('termsandconditionsheading') ? 'has-error' : ''}}">
							{!! Form::label('termsandconditionsheading', Lang::get('app.Heading'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('termsandconditionsheading', null, ['class' => 'form-control']) !!}
								{!! $errors->first('termsandconditionsheading', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('termsandconditionscontent') ? 'has-error' : ''}}">
							{!! Form::label('termsandconditionscontent', Lang::get('app.Content'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::textarea('termsandconditionscontent', null, ['class' => 'form-control']) !!}
								{!! $errors->first('termsandconditionscontent', '<p class="help-block">:message</p>') !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
