@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Adsense Settings')</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul-->
                        @endif

                        {!! Form::model($adssettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updateadssettings'],
                            'class' => 'form-horizontal',
                            'files' => true,
                        ]) !!}
                        <?php
                        ?>

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

						<div class="form-group {{ $errors->has('googleads') ? 'has-error' : ''}}">
							{!! Form::label('googleads', Lang::get('app.Google Ads'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								<?php
								if(isset($sitesettings->googleads) && $sitesettings->googleads==1)
								{
									$adsEnable = true;
									$adsDisable = Null;
								}
								else if(isset($sitesettings->googleads) && $sitesettings->googleads==0)
								{
									$adsEnable = Null;
									$adsDisable = true;
								}
								?>
								<div class="radio i-checks">
									<label>
										{!! Form::radio('googleads', 1, $adsEnable) !!}
										<i></i>
										@lang('app.Enable')
									</label>
								</div>
								<div class="radio i-checks">
									<label>
										{!! Form::radio('googleads', 0, $adsDisable) !!}
										<i></i>
										@lang('app.Disable')
									</label>
								</div>
								{!! $errors->first('googleads', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('adsclient') ? 'has-error' : ''}}">
						    {!! Form::label('adsclient', Lang::get('app.Adsclient'), ['class' => 'col-md-4 control-label']) !!}
						    <div class="col-md-6">
						        {!! Form::text('adsclient', null, ['class' => 'form-control']) !!}
						        {!! $errors->first('adsclient', '<p class="help-block">:message</p>') !!}
						    </div>
						</div><!--div class="form-group {{ $errors->has('adslot') ? 'has-error' : ''}}">
						    {!! Form::label('adslot', Lang::get('app.Adslot'), ['class' => 'col-md-4 control-label']) !!}
						    <div class="col-md-6">
						        {!! Form::text('adslot', null, ['class' => 'form-control']) !!}
						        {!! $errors->first('adslot', '<p class="help-block">:message</p>') !!}
						    </div>
						</div-->


						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection