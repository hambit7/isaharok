@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
    
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.General Settings')</div>
                    <div class="panel-body">

                        {!! Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                       <div class="form-group {{ $errors->has('sitename') ? 'has-error' : ''}}">
							{!! Form::label('sitename', Lang::get('app.Site Name'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('sitename', null, ['class' => 'form-control']) !!}
								{!! $errors->first('sitename', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('maximumage') ? 'has-error' : ''}}">
							{!! Form::label('maximumage', Lang::get('app.Maximum Age Limit'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('maximumage', null, ['class' => 'form-control']) !!}
								{!! $errors->first('maximumage', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('maximumdistance') ? 'has-error' : ''}}">
							{!! Form::label('maximumdistance', Lang::get('app.Maximum Distance Limit'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('maximumdistance', null, ['class' => 'form-control']) !!}
								{!! $errors->first('maximumdistance', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('sitecurrency') ? 'has-error' : ''}}">
							{!! Form::label('sitecurrency', Lang::get('app.Default Currency (Eg : USD - $)'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::select('sitecurrency', $currencies, null, ['class' => 'form-control']) !!}
								{!! $errors->first('sitecurrency', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<?php /* ?>
						<div class="form-group {{ $errors->has('defaultimages') ? 'has-error' : ''}}">
							{!! Form::label('defaultimages', Lang::get('app.Default User Thumb'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('defaultimages', ['class' => 'filestyle','name' => 'defaultimages', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
								{!! $errors->first('defaultimages', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->defaultimages != "") ?>
								<div><img src="<?php echo url('/uploads/defaultuser/'.$sitesettings->defaultimages); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;"></div>
							</div>
						</div>

						<div class="form-group {{ $errors->has('sitelogo') ? 'has-error' : ''}}">
							{!! Form::label('sitelogo', Lang::get('app.Site Logo Image'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('sitelogo', ['class' => 'filestyle','name' => 'sitelogo', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
								{!! $errors->first('sitelogo', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->sitelogo != "") ?>
								<div><img src="<?php echo url('/uploads/defaultuser/'.$sitesettings->sitelogo); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;"></div>
							</div>
						</div>
						<?php */ ?>
						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection