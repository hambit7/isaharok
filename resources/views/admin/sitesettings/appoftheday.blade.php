@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.App Of The Day')</div>
                    <div class="panel-body">

                        {!! Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updateappoftheday'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                       <div class="form-group {{ $errors->has('apptitle') ? 'has-error' : ''}}">
							{!! Form::label('apptitle', Lang::get('app.Title'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('apptitle', null, ['class' => 'form-control']) !!}
								{!! $errors->first('apptitle', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('appdesc') ? 'has-error' : ''}}">
							{!! Form::label('appdesc', Lang::get('app.Description'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::textarea('appdesc', null, ['class' => 'form-control']) !!}
								{!! $errors->first('appdesc', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('appurl') ? 'has-error' : ''}}">
							{!! Form::label('appurl', Lang::get('app.Link Android'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('appurl', null, ['class' => 'form-control']) !!}
								{!! $errors->first('appurl', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('appiosurl') ? 'has-error' : ''}}">
							{!! Form::label('appiosurl', Lang::get('app.Link IOS'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('appiosurl', null, ['class' => 'form-control']) !!}
								{!! $errors->first('appiosurl', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('appimage1') ? 'has-error' : ''}}">
							{!! Form::label('appimage1', Lang::get('app.Image 1'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('app_image1', null, ['class' => 'form-control','name' => 'defaultimages']) !!}
								{!! $errors->first('appimage1', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->appimage1 != ""){ ?>
								<input type="hidden" value="<?php echo $sitesettings->appimage1;?>" name="appimage1">
								<img src="<?php echo url('/uploads/appimage/'.$sitesettings->appimage1); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;">
									<?php } ?>
							</div>
						</div>
						<div class="form-group {{ $errors->has('appimage2') ? 'has-error' : ''}}">
							{!! Form::label('appimage2', Lang::get('app.Image 2'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('app_image2', null, ['class' => 'form-control','name' => 'defaultimages']) !!}
								{!! $errors->first('appimage2', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->appimage2 != ""){ ?>
								<img src="<?php echo url('/uploads/appimage/'.$sitesettings->appimage2); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;">
									<?php } ?>
							</div>
						</div>
						<div class="form-group {{ $errors->has('appimage3') ? 'has-error' : ''}}">
							{!! Form::label('appimage3', Lang::get('app.Image 3'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('app_image3', null, ['class' => 'form-control','name' => 'defaultimages']) !!}
								{!! $errors->first('appimage3', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->appimage3 != "") { ?>
								<img src="<?php echo url('/uploads/appimage/'.$sitesettings->appimage3); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;">
									<?php } ?>
							</div>
						</div>
						<div class="form-group {{ $errors->has('appimage4') ? 'has-error' : ''}}">
							{!! Form::label('appimage4', Lang::get('app.Image 4'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('app_image4', null, ['class' => 'form-control','name' => 'defaultimages']) !!}
								{!! $errors->first('appimage4', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->appimage4 != "") { ?>
								<img src="<?php echo url('/uploads/appimage/'.$sitesettings->appimage4); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;">
									<?php } ?>
							</div>
						</div>
						<div class="form-group {{ $errors->has('appimage5') ? 'has-error' : ''}}">
							{!! Form::label('appimage5', Lang::get('app.Image 5'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('app_image5', null, ['class' => 'form-control','name' => 'defaultimages']) !!}
								{!! $errors->first('appimage5', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->appimage5 != "") { ?>
								<img src="<?php echo url('/uploads/appimage/'.$sitesettings->appimage5); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;">
									<?php } ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection