@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.SMTP Settings')</div>
                    <div class="panel-body">

						<?php //echo "<pre>";print_r($errors);echo "</pre>"; ?>
                        {!! Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

						<div class="form-group {{ $errors->has('smtpenable') ? 'has-error' : ''}}">
							{!! Form::label('smtpenable', Lang::get('app.SMTP'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								<div class="radio i-checks">
									<label>
										{!! Form::radio('smtpenable', 1, Null) !!}
										<i></i>
										@lang('app.Enable')
									</label>
								</div>
								<div class="radio i-checks">
									<label>
										{!! Form::radio('smtpenable', 0, Null) !!}
										<i></i>
										@lang('app.Disable')
									</label>
								</div>
								{!! $errors->first('smtpenable', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('smtphost') ? 'has-error' : ''}}">
							{!! Form::label('smtphost', Lang::get('app.SMTP Host'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('smtphost', null, ['class' => 'form-control']) !!}
								{!! $errors->first('smtphost', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('smtpport') ? 'has-error' : ''}}">
							{!! Form::label('smtpport', Lang::get('app.SMTP Port'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('smtpport', null, ['class' => 'form-control']) !!}
								{!! $errors->first('smtpport', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('smtpusername') ? 'has-error' : ''}}">
							{!! Form::label('smtpusername', Lang::get('app.SMTP Email'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('smtpusername', null, ['class' => 'form-control']) !!}
								{!! $errors->first('smtpusername', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('smtppassword') ? 'has-error' : ''}}">
							{!! Form::label('smtppassword', Lang::get('app.SMTP Password'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::password('smtppassword', ['class' => 'form-control']) !!}
								{!! $errors->first('smtppassword', '<p class="help-block">:message</p>') !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection