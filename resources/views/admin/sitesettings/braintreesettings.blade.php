@extends('layouts.master')

@section('content')
        <?php
        $baseurl = URL::to('/');
        ?>
     <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Brain Tree Payment Settings')</div>
                    <div class="panel-body">

						<?php //echo "<pre>";print_r($errors);echo "</pre>"; ?>
                        {!! Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

						<div class="form-group {{ $errors->has('brainTreeType') ? 'has-error' : ''}}">
							{!! Form::label('brainTreeType', Lang::get('app.Environment'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::radio('brainTreeType', 'live', Null) !!} @lang('app.Live')
								</br>
								{!! Form::radio('brainTreeType', 'sandbox', Null) !!} @lang('app.Sandbox')
								{!! $errors->first('brainTreeType', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('brainTreeMerchantId') ? 'has-error' : ''}}">
							{!! Form::label('brainTreeMerchantId', Lang::get('app.Brain Tree Merchant Id'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('brainTreeMerchantId', null, ['class' => 'form-control']) !!}
								{!! $errors->first('brainTreeMerchantId', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('brainTreePublicKey') ? 'has-error' : ''}}">
							{!! Form::label('brainTreePublicKey', Lang::get('app.Brain Tree Public Key'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('brainTreePublicKey', null, ['class' => 'form-control']) !!}
								{!! $errors->first('brainTreePublicKey', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('brainTreePrivateKey') ? 'has-error' : ''}}">
							{!! Form::label('brainTreePrivateKey', Lang::get('app.Brain Tree Private Key'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('brainTreePrivateKey', null, ['class' => 'form-control']) !!}
								{!! $errors->first('brainTreePrivateKey', '<p class="help-block">:message</p>') !!}
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection