@extends('layouts.master')

@section('content')
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.Site settings')</div>
                    <div class="panel-body">

                        {!! Form::model($adminData, [
                            'method' => 'post',
                            'url' => ['/admin/updateprofile'],
                            'class' => 'form-horizontal',
                        ]) !!}

						<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
                       <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
							{!! Form::label('email', Lang::get('app.Email'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::text('email', null, ['class' => 'form-control']) !!}
								{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
							{!! Form::label('password', Lang::get('app.Password'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
								<div class="col-md-12" style="font-size:14px;">@lang("app.Do not modify anything, if you don’t want to change the password")</div>
								{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection