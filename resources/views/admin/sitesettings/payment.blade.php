@extends('layouts.master')

@section('content')
<?php
$baseurl = URL::to('/');
?>
 <div class="main_content">
        <div class="sub_content">
		<div class="col-md-12 margin_top30">
			<div class="panel panel-default">
				<div class="panel-heading">@lang('app.Payment Settings')</div>
				<div class="panel-body">

					@if ($errors->any())
                            <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul-->
                            @endif

                            {!! Form::model($sitesettings, [
                            	'method' => 'post',
                            	'url' => [$baseurl.'/admin/updatesitesettings'],
                            	'class' => 'form-horizontal',
                            	'files' => true,
                            	]) !!}
                            <?php
                            ?>

                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

                            <div class="form-group {{ $errors->has('paymenttype') ? 'has-error' : ''}}">
                            	{!! Form::label('googleads', Lang::get('app.Payment Type'), ['class' => 'col-md-4 control-label']) !!}
                            	<div class="col-md-6">
                            		<?php
                            		$inappsetting="";
                            		$braintreesetting="";
                            		if(isset($sitesettings->paymenttype) && $sitesettings->paymenttype=='inapp'|| $sitesettings->paymenttype=='')
                            		{
                            			$inapp = true;
                            			$braintree = Null;
                            		}
                            		else if(isset($sitesettings->paymenttype) && $sitesettings->paymenttype=='braintree')
                            		{
                            			$inapp = Null;
                            			$braintree = true;
                            			
                            		}
                            		?>
                            		<div class="radio i-checks">
                            			<label>
                            				{!! Form::radio('paymenttype', 'inapp',$inapp, ['class' => 'inapp']) !!}
                            				<i></i>
                            				@lang('app.Inapp')
                            			</label>
                            		</div>
                            		<div class="radio i-checks">
                            			<label>
                            				{!! Form::radio('paymenttype', 'braintree', $braintree,['class' => 'braintree']) !!}
                            				<i></i>
                            				@lang('app.Braintree')
                            			</label>
                            		</div>
                            		{!! $errors->first('paymenttype', '<p class="help-block">:message</p>') !!}
                            	</div>
                            </div>
						<!--div class="form-group {{ $errors->has('adslot') ? 'has-error' : ''}}">
						    {!! Form::label('adslot', Lang::get('app.Adslot'), ['class' => 'col-md-4 control-label']) !!}
						    <div class="col-md-6">
						        {!! Form::text('adslot', null, ['class' => 'form-control']) !!}
						        {!! $errors->first('adslot', '<p class="help-block">:message</p>') !!}
						    </div>
						</div-->
						<?php
						if($sitesettings->paymenttype == 'braintree')
						{
							$inappsetting = "style=display:none";
						}
						else
						{
							$braintreesetting =  "style=display:none";
						}
						?>

						<div class="inappsetting" <?php echo $inappsetting; ?> >
							<div class="form-group {{ $errors->has('licensetoken') ? 'has-error' : ''}}">
								{!! Form::label('licensetoken', Lang::get('app.License Token'), ['class' => 'col-md-4 control-label']) !!}
								<div class="col-md-6">
									{!! Form::text('licensetoken', null, ['class' => 'form-control']) !!}
									{!! $errors->first('licensetoken', '<p class="help-block">:message</p>') !!}
								</div>
							</div>
						</div>
						
						<div class="braintreesetting" <?php echo $braintreesetting; ?>>
							<div class="form-group {{ $errors->has('brainTreeType') ? 'has-error' : ''}}">
								{!! Form::label('brainTreeType', Lang::get('app.Environment'), ['class' => 'col-md-4 control-label']) !!}
								<div class="col-md-6">
									<div class="radio i-checks">
										<label>
											{!! Form::radio('brainTreeType', 'live', Null) !!} <i></i>@lang('app.Live')
										</label>
									</div>
									
									<div class="radio i-checks">
										<label>
											{!! Form::radio('brainTreeType', 'sandbox', Null) !!} <i></i>@lang('app.Sandbox')
										</label>
									</div>
									{!! $errors->first('brainTreeType', '<p class="help-block">:message</p>') !!}
								</div>
							</div>
							<div class="form-group {{ $errors->has('brainTreeMerchantId') ? 'has-error' : ''}}">
								{!! Form::label('brainTreeMerchantId', Lang::get('app.Brain Tree Merchant Id'), ['class' => 'col-md-4 control-label']) !!}
								<div class="col-md-6">
									{!! Form::text('brainTreeMerchantId', null, ['class' => 'form-control']) !!}
									{!! $errors->first('brainTreeMerchantId', '<p class="help-block">:message</p>') !!}
								</div>
							</div>
							<div class="form-group {{ $errors->has('brainTreePublicKey') ? 'has-error' : ''}}">
								{!! Form::label('brainTreePublicKey', Lang::get('app.Brain Tree Public Key'), ['class' => 'col-md-4 control-label']) !!}
								<div class="col-md-6">
									{!! Form::text('brainTreePublicKey', null, ['class' => 'form-control']) !!}
									{!! $errors->first('brainTreePublicKey', '<p class="help-block">:message</p>') !!}
								</div>
							</div>
							<div class="form-group {{ $errors->has('brainTreePrivateKey') ? 'has-error' : ''}}">
								{!! Form::label('brainTreePrivateKey', Lang::get('app.Brain Tree Private Key'), ['class' => 'col-md-4 control-label']) !!}
								<div class="col-md-6">
									{!! Form::text('brainTreePrivateKey', null, ['class' => 'form-control']) !!}
									{!! $errors->first('brainTreePrivateKey', '<p class="help-block">:message</p>') !!}
								</div>
							</div>
						</div>
						

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
							</div>
						</div>

						{!! Form::close() !!}

					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection