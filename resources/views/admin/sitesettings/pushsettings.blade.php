@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.PushNotification Settings')</div>
                    <div class="panel-body">

                    @if ($errors->any())
                        <!--ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                                @endforeach
                                </ul-->
                        @endif

                        {!! Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl.'/admin/updatepushsettings'],
                            'class' => 'form-horizontal',
                            'files' => false,
                            ]) !!}


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group {{ $errors->has('push_messages_status') ? 'has-error' : ''}}">
                            {!! Form::label('googleads', Lang::get('app.Status'), ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">

                                <div class="radio i-checks">
                                    <label>
                                        {!!  Form::radio('push_messages_status', 'true',
                                       1) !!}
                                        <i></i>
                                        @lang('app.Enable')
                                    </label>
                                </div>
                                <div class="radio i-checks">
                                    <label>
                                        {!! Form::radio('push_messages_status', 'false', 0) !!}
                                        <i></i>
                                        @lang('app.Disable')
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
