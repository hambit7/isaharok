@extends('layouts.master')

@section('content')
    <?php
    $baseurl = URL::to('/');
    if (isset($search) && !empty($search)) {
        $search = $search;
    } else {
        $search = "";
    }
    ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('app.girls')</div>
                    <div class="panel-body">


                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="GET"
                                  action="{{ url($baseurl.'/admin/girls') }}">
                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search; ?>"
                                           class="input-sm form-control" placeholder="@lang('app.Search')">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="submit">@lang('app.Go!')</button>
                                    <button class="btn btn-sm btn-default"><a
                                                href="{{ request()->url() }}">Clear</a></button>
                                </span>
                                </div>
                            </form>
                        </div>


                        <div class="col-sm-5 m-b-xs">
                            <a href="{{ url('/admin/girls/create') }}" class="btn btn-primary btn-xs"
                               title="Add New Girl"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>@lang('#')</th>
                                    {{--<th>@lang('app.ID')</th>--}}
                                    <th> @sortablelink(Lang::get('app.Firstname'),Lang::get('app.First Name'))</th>
                                    <th> @sortablelink(Lang::get('app.Email'))</th>
                                    <th> @sortablelink(Lang::get('app.Confirmed'))</th>
                                    <th>@lang('app.View')</th>
                                    <th>@lang('app.Photo')</th>
                                    <th>@lang('app.gesture-photo')</th>
                                    <th>@lang('app.Last_active')</th>
                                    <th>@lang('app.Activate')</th>
                                    <th>@lang('app.Delete')</th>
                                    {{--<th>@lang('app.Actions')</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @if($girls)

                                    @foreach($girls as $key=>$item)
                                        <tr>
                                            <td>{{ $key +1 }}</td>
                                            <td id="girlID" hidden>{{ $item->id }}</td>
                                            <td>{{ $item->firstname }}</td>
                                            <td>{{ $item->email }}</td>
                                            @if($item->userstatus)
                                                <td id="is_activated">{{  "Yes"}}</td>
                                            @else
                                                <td id="is_activated">{{  "No"}}</td>
                                            @endif
                                            <td>
                                                <a href="{{ url($baseurl.'/admin/viewapproveduser/' . $item->id) }}"
                                                   class="btn btn-success btn-xs" title="View girl profile"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"/></a>
                                            </td>
                                            <td>
                                                <a href="{{ url($baseurl.'/admin/gallery/' . $item->id) }}"
                                                   class="btn btn-success btn-xs" title="View girl photo"><span
                                                            class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                            </td>
                                            <td>
                                                <a href="{{ url($baseurl.'/admin/content/' . $item->id) }}"
                                                   class="btn btn-success btn-xs" title="View girl gesture photo"><span
                                                            class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                            </td>
                                            <td>{{date('Y-m-d h:m', $item->onlinetimestamp)}}</td>

                                            <td id="activated">

                                                <div>
                                                    <button data-user="{{$item->id}}" class="btn btn-default"
                                                            id="active">Activate
                                                    </button>
                                                </div>
                                            </td>

                                            <td id="deleting">

                                                <div>
                                                    <button data-user-delete="{{$item->id}}" class="btn btn-danger"
                                                            id="delete">Delete
                                                    </button>
                                                </div>
                                            </td>


                                        <!--td>
                                            <a href="{{ url('/admin/users/' . $item->id) }}" class="btn btn-success btn-xs" title="View User"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit User"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/users', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete User',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                                </td-->


                                    @endforeach
                                @endIf
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $girls->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



