@extends('layouts.master')
@section('content')

    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-10 margin_top30">
                <div class="panel panel-default">
                    {{--@if (count($errors) > 0)--}}
                    {{--<div class="alert alert-danger">--}}
                    {{--<ul>--}}
                    {{--@foreach ($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    <div class="panel-heading">@lang('app.girls')</div>
                    <div class="panel-body">

                        {!! Form::model($girls, [
                        'method' => 'post',
                        'url' => ['/admin/girls'],
                        'enctype'=> 'multipart/form-data',
                         'files' => true
                        ]) !!}
                        {{--<form id="girlsform">--}}
                        <div class="form-group col-md-7 {{ $errors->has('email') ? 'has-error' : ''}}">
                            {!! Form::label('email','Email address *', []) !!}
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('phone') ? 'has-error' : ''}}">
                            {!! Form::label('phone','Phone *', []) !!}
                            {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'gphone']) !!}
                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('password') ? 'has-error' : ''}}">
                            {!! Form::label('password','Password *', []) !!}
                            {{ Form::password('password',["class" => "form-control",]) }}
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('firstname') ? 'has-error' : ''}}">
                            {!! Form::label('firstname','Name *', []) !!}
                            {!! Form::text('firstname', null, ['class' => 'form-control', 'id' => 'gphone']) !!}
                            {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
                        </div>


                        <div class="form-group col-md-7 {{ $errors->has('firstimage') ? 'has-error' : ''}}">
                            {!! Form::label('firstimage','First image *', []) !!}
                            {!! Form::file('firstimage', ['class' => 'form-control', 'multiple' =>'multiple']) !!}
                            {!! $errors->first('firstimage', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('secondimage') ? 'has-error' : ''}}">
                            {!! Form::label('secondimage','Second image *', []) !!}
                            {!! Form::file('secondimage', ['class' => 'form-control', 'id' => 'secondimage', 'multiple' =>'multiple']) !!}
                            {!! $errors->first('secondimage', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('secondimage') ? 'has-error' : ''}}">
                            {!! Form::label('thirdimage','Third image *', []) !!}
                            {!! Form::file('thirdimage', ['class' => 'form-control', 'id' => 'thirdimage', 'multiple' =>'multiple']) !!}
                            {!! $errors->first('thirdimage', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('age') ? 'has-error' : ''}}">
                            {!! Form::label('age','Age *', []) !!}
                            {!! Form::number('age', null, ['class' => 'form-control', 'id' => 'age']) !!}
                            {!! $errors->first('age', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('info') ? 'has-error' : ''}}">
                            {!! Form::label('info','Info', []) !!}
                            {!! Form::textarea('info', null, ['class' => 'form-control', 'id' => 'info']) !!}
                            {!! $errors->first('info', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-md-7 {{ $errors->has('bio') ? 'has-error' : ''}}">
                            {!! Form::label('bio','Biography', []) !!}
                            {!! Form::textarea('bio', null, ['class' => 'form-control', 'id' => 'bio']) !!}
                            {!! $errors->first('bio', '<p class="help-block">:message</p>') !!}
                        </div>
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="exampleInputEmail1">Email address *</label>--}}
                        {{--<input type="email" name="email" class="form-control " id="exampleInputEmail1"--}}
                        {{--aria-describedby="emailHelp" placeholder="Enter email" required>--}}
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with--}}
                        {{--anyone else.--}}
                        {{--</small>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="gphone">Phone *</label>--}}
                        {{--<input type="text" class="form-control" name="phone" id="gphone"--}}
                        {{--aria-describedby="gphone" required>--}}
                        {{--</div>                            {{--<div class="form-group col-md-7">--}}
                        {{--<label for="exampleInputEmail1">Email address *</label>--}}
                        {{--<input type="email" name="email" class="form-control " id="exampleInputEmail1"--}}
                        {{--aria-describedby="emailHelp" placeholder="Enter email" required>--}}
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with--}}
                        {{--anyone else.--}}
                        {{--</small>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="gphone">Phone *</label>--}}
                        {{--<input type="text" class="form-control" name="phone" id="gphone"--}}
                        {{--aria-describedby="gphone" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="exampleInputPassword1">Password *</label>--}}
                        {{--<input type="password" min="8" class="form-control" id="exampleInputPassword1"--}}
                        {{--placeholder="">--}}
                        {{--<small id="emailHelp" class="form-text text-muted">Minnimum 8 simbols.</small>--}}

                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="name">Name *</label>--}}
                        {{--<input type="text" class="form-control" name="firstname" id="name"--}}
                        {{--aria-describedby="name" placeholder="Enter name" required>--}}
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        {{--</div>--}}
                        {{--<div id="image-preview-div" style="display: none">--}}
                        {{--<label for="exampleInputFile">Selected image:</label>--}}
                        {{--<br>--}}
                        {{--<img id="preview-img" src="noimage">--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="firstimage">First image *</label>--}}
                        {{--<input type="file" multiple="" accept="image/x-png,image/gif,image/jpeg"--}}
                        {{--name="firstimage" id="firstimg" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="secondimage">Second image *</label>--}}
                        {{--<input type="file" multiple="" accept="image/x-png,image/gif,image/jpeg"--}}
                        {{--name="secondimage" id="secondimage" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="thirdimage">Third image *</label>--}}
                        {{--<input type="file" multiple="" accept="image/x-png,image/gif,image/jpeg"--}}
                        {{--name="thirdimage" id="thirdimage" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="age">Age *</label>--}}
                        {{--<input type="number" class="form-control" min="0" id="age" name="age" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="ginfo">Info</label>--}}
                        {{--<textarea rows="8" class="form-control" min="0" id="ginfo" name="info"></textarea>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-7">--}}
                        {{--<label for="biography">Biography</label>--}}
                        {{--<textarea rows="20" class="form-control" min="0" id="biography" name="bio"></textarea>--}}
                        {{--</div>--}}

                        <div class="form-group col-md-7">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        {{--</form>--}}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
