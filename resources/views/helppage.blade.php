@extends('layouts.helppagelayout')
<?php $baseurl = URL::to('/'); ?>
@section('title')
    <?php echo "Howzu"; ?>
@endsection
@section('content')
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="logo-h">
                        <a href="<?php echo $baseurl; ?>">
                            <img src="<?php echo url('/uploads/landingpage/' . $landingpagesettings->subpagelogo); ?>"
                                 alt="Ios app download">
                        </a>
                    </div>
                    <span class="pull-right copyright-t"><?php echo $landingpagesettings->copyrightinfo ?></span>
                    <?php if(!empty($landingpagesettings->facebooklink) || !empty($landingpagesettings->googlelink) || !empty($landingpagesettings->twitterlink)){ ?>
                    <div class="social-media-m">
                        <div class="social-media-icon">
                            <?php if(!empty($landingpagesettings->facebooklink)) ?>
                            <a href="<?php echo $landingpagesettings->facebooklink ?>" target="_blank"><img
                                        src="<?php echo url('images/facebook.png'); ?>" alt="facebook"></a>
                            <?php if(!empty($landingpagesettings->twitterlink)) ?>
                            <a href="<?php echo $landingpagesettings->twitterlink ?>" target="_blank"><img
                                        src="<?php echo url('images/twitter.png'); ?>" alt="twitter"></a>
                            <?php if(!empty($landingpagesettings->googlelink)) ?>
                            <a href="<?php echo $landingpagesettings->googlelink ?>" target="_blank"><img
                                        src="<?php echo url('images/google-plus.png'); ?>" alt="google plus"></a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>


    <!---------------------------------------------------- Middle container -------------------------------------------------------->
    <div class="help-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <div class="full-horizontal-line col-xs-12 col-sm-12 col-md-12 col-lg-12 "></div>
                </div>
            </div>
            <div class="row">
                <div class="help col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="help-left nav nav-tabs col-xs-12 col-sm-3 col-md-3 col-lg-3 no-hor-padding ">
                        <?php
                        foreach ($allHelpPages as $helpKey => $helppage) {
                            if ($helppage->title == $helppageModel->title) {
                                echo '<li class="active col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">';
                            } else {
                                echo '<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">';
                            }
                            $slug = str_replace(" ", "-", $helppage->title);
                            echo "<a href=" . $baseurl . "/helppage/" . $slug . ">{$helppage->title}</a></li>";
                        }
                        ?>
                        <?php if(!empty($landingpagesettings->facebooklink) || !empty($landingpagesettings->googlelink) || !empty($landingpagesettings->twitterlink)){ ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 social-media">
                            <span class="bold social-media-lable col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">Social media:</span>
                            <div class="social-media-icon">
                                <?php if(!empty($landingpagesettings->facebooklink)) ?>
                                <a href="<?php echo $landingpagesettings->facebooklink ?>" target="_blank"><img
                                            src="<?php echo url('images/facebook.png'); ?>" alt="facebook"></a>
                                <?php if(!empty($landingpagesettings->twitterlink)) ?>
                                <a href="<?php echo $landingpagesettings->twitterlink ?>" target="_blank"><img
                                            src="<?php echo url('images/twitter.png'); ?>" alt="twitter"></a>
                                <?php if(!empty($landingpagesettings->googlelink)) ?>
                                <a href="<?php echo $landingpagesettings->googlelink ?>" target="_blank"><img
                                            src="<?php echo url('images/google-plus.png'); ?>" alt="google plus"></a>
                            </div>
                        </div>
                        <?php } ?>
                    </ul>


                    <div class="help-left-r dropdown">
                        <button class="btn-cus btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                            Help Links
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-cus">
                            <?php
                            foreach ($allHelpPages as $helpKey => $helppage) {
                                if ($helppage->title == $helppageModel->title) {
                                    echo '<li class="active col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">';
                                } else {
                                    echo '<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">';
                                }
                                $slug = str_replace(" ", "-", $helppage->title);
                                echo "<a href=" . $baseurl . "/helppage/" . $slug . ">{$helppage->title}</a></li>";
                            }
                            ?>
                        </ul>
                    </div>


                    <div class="help-right">
                        <div class="tab-content col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <div id="home"
                                 class="tab-pane fade in active col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding ">
                                <h3 class="bold help_content"><?php echo $helppageModel->title; ?></h3>
                                <p class="help_content"><?php echo $helppageModel->description; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
