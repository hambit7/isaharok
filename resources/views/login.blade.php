@extends('layouts.loginlayout')
@section('title', $siteSettings->sitename)

@section('content')
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp section-container">
        <div class="container aside-xl middle-container">
            @if(Session::has('flash_message'))
                <div class="alert alert-info" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <a class="navbar-brand block" href="javascript:void(0);"><?php echo $siteSettings->sitename; ?></a>
            <section class="m-b-lg">
                <header class="wrapper text-center">
                    <strong>Sign in to get in touch</strong>
                </header>
                <?php
                $baseurl = URL::to('/');
                ?>
                <form action="<?php echo $baseurl . '/admin';?>" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="list-group">
                        <div class="list-group-item">
                            <input type="email" placeholder="Email" class="form-control no-border" name="email"
                                   required>
                        </div>
                        <div class="list-group-item">
                            <input type="password" placeholder="Password" class="form-control no-border" name="password"
                                   required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
                    <!-- <div class="text-center m-t m-b"><a href="#"><small>Forgot password?</small></a></div>
                    <div class="line line-dashed"></div>
                    <p class="text-muted text-center"><small>Do not have an account?</small></p>
                    <a href="signup.html" class="btn btn-lg btn-default btn-block">Create an account</a> -->
                </form>
            </section>
        </div>
    </section>
@endsection
