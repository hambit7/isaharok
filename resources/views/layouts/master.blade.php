  <?php
  session_start();
  $baseurl = URL::to('/');
  ?>
  <!DOCTYPE html>
  <html lang="en" class="app">
  <head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

      {{--    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.css') }}" type="text/css" />--}}
      <link rel="stylesheet" href="/css/bootstrap.css" type="text/css"/>
      <link rel="stylesheet" href="/css/animate.css" type="text/css"/>
      {{--    <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}" type="text/css" />--}}
      <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css"/>
      {{--    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}" type="text/css" />--}}
      <link rel="stylesheet" href="/css/icon.css" type="text/css"/>
      {{--    <link rel="stylesheet" href="{{ URL::asset('assets/css/icon.css') }}" type="text/css" />--}}
      <link rel="stylesheet" href="/css/font.css" type="text/css"/>
      {{--    <link rel="stylesheet" href="{{ URL::asset('assets/css/font.css') }}" type="text/css" />--}}
      <link rel="stylesheet" href="/css/app.css" type="text/css"/>
      <link rel="stylesheet" href="/css/admin.css" type="text/css"/>
      <link rel="stylesheet" href="/public/js/calendar/bootstrap_calendar.css" type="text/css"/>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery.js@1.1.2/src/css/lightgallery.css"
            type="text/css"/>
  <!--[if lt IE 9]>
      <script src="/public/js/ie/html5shiv.js"></script>
      <script src="/public/js/ie/respond.min.js"></script>
      <script src="/public/js/ie/excanvas.js"></script>
      <link rel="stylesheet" href="sweetalert2.min.css">
      <![endif]-->
</head>
<body class="">
  <section class="vbox">
    <header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
      <div class="navbar-header aside-md dk">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
          <i class="fa fa-bars"></i>
        </a>
          <a href="<?php echo $baseurl; ?>/admin/dashboard" class="navbar-brand">
          <!-- <img src="<?php echo $baseurl; ?>/images/logo.png" class="m-r-sm" alt="scale"> -->
          <span class="hidden-nav-xs"><?php echo $siteSettings->sitename; ?></span>
        </a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
          <i class="fa fa-cog"></i>
        </a>
      </div>
      <?php
      $locale = App::getLocale();
      ?>
      <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">


      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="thumb-sm avatar pull-left">
            <img src="<?php echo $baseurl; ?>/images/a0.png" alt="...">
          </span>
          Admin <b class="caret"></b>
        </a>
        <ul class="dropdown-menu animated fadeInRight">
          <li>
            <a href="<?php echo $baseurl; ?>/admin/profile" title="profile" >@lang('app.Profile')</a>
          </li>
          <li>
            <a href="<?php echo $baseurl; ?>/adminlogout" title="logout" >@lang('app.Logout')</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <section>
    <section class="hbox stretch">
      <!-- .aside -->
        <aside class="bg-black aside-md hidden-print mt-3 pb-3 mb-3 d-flex" id="nav">
        <section class="vbox">
          <section class="w-f scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
              <!-- nav -->
              <nav class="nav-primary hidden-xs">
                <!-- <div class="text-muted text-sm hidden-nav-xs padder m-t-sm m-b-sm">Start</div> -->
                <ul class="nav nav-main" data-ride="collapse">
					<?php /*<li <?php echo $controller == "DashboardController" ? "class='active'" : ""; ?> >
<a href="javascript:void(0);" class="auto">
<span class="pull-right text-muted">
<i class="i i-circle-sm-o text"></i>
<i class="i i-circle-sm text-active"></i>
</span>
<!-- <b class="badge bg-danger pull-right">4</b> -->
<i class="i i-stack icon">
</i>
<span class="font-bold">@lang('app.Dashboard')</span>
</a>
</li> */?>

<!---------- Users Menu ------------>


<li <?php echo $controller == "DashboardController" && $action == "index" ? "class='active'" : ""; ?> >
  <a href="<?php echo $baseurl; ?>/admin/dashboard" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
    <!-- <b class="badge bg-danger pull-right">4</b> -->
    <i class="i i-statistics icon">
    </i>
    <span class="font-bold">@lang('app.Overview')</span>

  </a>
</li>

<li <?php echo $controller == "UsersController" ? "class='active'" : ""; ?> >
  <a href="javascript:void(0);" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
    <!-- <b class="badge bg-danger pull-right">4</b> -->
    <i class="fa fa-user icon">
    </i>
    <span class="font-bold">@lang('app.Manage Users')</span>
  </a>
  <ul class="nav dk" <?php echo $controller == "UsersController" ? "style='display:block;'" : ""; ?>>
    <li <?php echo $action == "approved" ? "class='active'" : ""; ?>>
      <a href="<?php echo $baseurl; ?>/admin/approved" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.Approved Users')</span>
      </a>
    </li>
    <li <?php echo $action == "unapproved" ? "class='active'" : ""; ?> >
      <a href="<?php echo $baseurl; ?>/admin/unapproved" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.Unapproved Users')</span>
      </a>
    </li>
      <li hidden="hidden"
      <?php echo $action == "reportedusers" ? "class='active'" : ""; ?> >
      <a href="<?php echo $baseurl; ?>/admin/reportedusers" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.Reported Users')</span>
      </a>
    </li>
    <li <?php echo $action == "premiumuser" ? "class='active'" : ""; ?>>
      <a href="<?php echo $baseurl; ?>/admin/premiumuser" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.Premium Users')</span>
      </a>
    </li>

    <li <?php echo $action == "premiumuser" ? "class='active'" : ""; ?>>
      <a href="<?php echo $baseurl; ?>/admin/girls" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.girls')</span>
      </a>
    </li>



  </ul>
</li>
<li <?php echo $controller == "PremiumController" ? "class='active'" : ""; ?> >
  <a href="<?php echo $baseurl; ?>/admin/premium" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
    <!-- <b class="badge bg-danger pull-right">4</b> -->
    <i class="i i-stack icon">
    </i>
    <span class="font-bold">@lang('app.Manage Premium')</span>
  </a>
</li>

<!-- Sitesettings menu -->
<li <?php echo $controller == "SitesettingsController" && $action != "termsandcondition" && $action != "appoftheday" ? "class='active'" : ""; ?> >
  <a href="javascript:void(0);" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
    <!-- <b class="badge bg-danger pull-right">4</b> -->
    <i class="i i-settings icon">
    </i>
    <span class="font-bold">@lang('app.Site Settings')</span>
  </a>
  <ul class="nav dk" <?php echo $controller == "SitesettingsController" && $action != "termsandcondition" && $action != "appoftheday" ? "style='display:block;'" : ""; ?>>
    <li <?php echo $action == "sitesettings" ? "class='active'" : ""; ?>>
      <a href="<?php echo $baseurl; ?>/admin/sitesettings" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.General Settings')</span>
      </a>
    </li>
    <li <?php echo $action == "smtpsettings" ? "class='active'" : ""; ?> >
      <a href="<?php echo $baseurl; ?>/admin/smtpsettings" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.SMTP Settings')</span>
      </a>
    </li>
    <li <?php echo $action == "adssettings" ? "class='active'" : ""; ?> >
      <a href="<?php echo $baseurl; ?>/admin/adssettings" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.Adsense Settings')</span>
      </a>
    </li>
    <li <?php echo $action == "paymentsettings" ? "class='active'" : ""; ?> >
      <a href="<?php echo $baseurl; ?>/admin/payment" class="auto">
        <i class="i i-dot"></i>

        <span>@lang('app.Payment Settings')</span>
      </a>
    </li>
      <div hidden="hidden">
          <li <?php echo $action == "pushsettings" ? "class='active'" : ""; ?> >
              <a href="<?php echo $baseurl; ?>/admin/pushsettings" class="auto">
                  <i class="i i-dot"></i>

                  <span>@lang('app.PushNotification Settings')</span>
              </a>
          </li>
      </div>
  </ul>
</li>

<li <?php echo $controller == "DashboardController" && $action == "androidConfig" ? "class='active'" : ""; ?> >
  <a href="<?php echo $baseurl; ?>/admin/androidconfig" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
    <!-- <b class="badge bg-danger pull-right">4</b> -->
    <i class="fa fa-android icon">
    </i>
    <span class="font-bold">@lang('app.Android App Config')</span>

  </a>
</li>
<li <?php echo $controller == "DashboardController" && $action == "iosConfig" ? "class='active'" : ""; ?> >
  <a href="<?php echo $baseurl; ?>/admin/iosconfig" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
    <!-- <b class="badge bg-danger pull-right">4</b> -->
    <i class="fa fa-apple icon">
    </i>
    <span class="font-bold">@lang('app.iOS App Config')</span>

  </a>
</li>

                        <li <?php echo $controller == "ContentController" && $action == "create" ? "class='active'" : ""; ?> >
                            <a href="<?php echo $baseurl; ?>/admin/content" class="auto">
    <span class="pull-right text-muted">
      <i class="i i-circle-sm-o text"></i>
      <i class="i i-circle-sm text-active"></i>
    </span>
                                <!-- <b class="badge bg-danger pull-right">4</b> -->
                                <i class="fa fa-image">
                                </i>
                                <span class="font-bold">@lang('app.Content')</span>

                            </a>
                        </li>

                </ul>

</nav>
<!-- / nav -->
</div>
</section>
<!--
<footer class="footer hidden-xs no-padder text-center-nav-xs">
  <a href="<?php echo $baseurl; ?>/adminlogout" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
    <i class="i i-logout"></i>
  </a>
  <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
    <i class="i i-circleleft text"></i>
    <i class="i i-circleright text-active"></i>
  </a>
</footer>-->
<footer class="footer hidden-xs no-padder text-center-nav-xs">
  <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive pull-left m-l-xs m-r-xs arrw-btn">
    <i class="i i-circleleft text"></i>
    <i class="i i-circleright text-active"></i>
  </a>

  <a href="<?php echo $baseurl; ?>/adminlogout" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
    <i class="i i-logout"></i>
  </a>
  <span class="pull-left hidden-nav-xs m-l-xs m-r-xs"><?php echo $siteSettings->sitename;?>
  <?php echo " - v1.0"; ?></span>
</footer>

</section>
</aside>
<!-- /.aside -->
<section class="scrollable">
  <section id="content" class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
    <section class="hbox stretch">
      <section class="vbox">
        <section class="padder">

          @if(Session::has('flash_message'))
          <div class="alert alert-info col-lg-6 col-sm-6" style="margin: 15px auto 0px;float: none;text-align: center;">
           <button type="button" class="close" data-dismiss="alert">×</button>
           {{ Session::get('flash_message') }}
         </div>
         @endif
         @yield('content')
         <?php
         if($action != "index" || $controller != "DashboardController"){ ?>
          <div class="">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <span><?php echo $landingpagesettings->copyrightinfo;?></span>
                </div>
              </div>
            </div>
          </div>

        <?php }

        ?>
      </section>
    </section>

  </section>
</section>
</section>
</section>
</section>
</section>
<script type="text/javascript">
  var APP_URL = {!! json_encode(url('/')) !!};
</script>
  <script src="/js/jquery.min.js"></script>
<!-- Bootstrap -->
  <script src="/js/bootstrap.js"></script>
<!-- file input -->
  <script src="/js/file-input/bootstrap-filestyle.min.js"></script>
<!-- App -->
  <script src="/js/app.js"></script>
  <script src="/js/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="/js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="/js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="/js/charts/flot/jquery.flot.min.js"></script>
  <script src="/js/charts/flot/jquery.flot.tooltip.min.js"></script>
  <script src="/js/charts/flot/jquery.flot.spline.js"></script>
  <script src="/js/charts/flot/jquery.flot.pie.min.js"></script>
  <script src="/js/charts/flot/jquery.flot.resize.js"></script>
  <script src="/js/charts/flot/jquery.flot.grow.js"></script>
  <script src="/js/charts/flot/demo.js"></script>

  <script src="/js/calendar/bootstrap_calendar.js"></script>
  <script src="/js/calendar/demo.js"></script>

  <script src="/js/sortable/jquery.sortable.js"></script>
  <script src="/js/app.plugin.js"></script>
  <script src="/js/admin.js"></script>

<!-- wysiwyg -->
  <script src="/js/wysiwyg/jquery.hotkeys.js"></script>
  <script src="/js/wysiwyg/bootstrap-wysiwyg.js"></script>
  <script src="/js/wysiwyg/demo.js"></script>
  <script src="/js/plugins/jquery.maskedinput.min.js"></script>
  <script src="/js/plugins/upload-image.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/lightgallery.js@1.1.2/lib/js/lightgallery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


</body>
</html>
