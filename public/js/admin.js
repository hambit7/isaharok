function updateLang(org) {
    lang = $(org).val();
    console.log(lang);
    window.location = APP_URL + '/setlanguage/' + lang;

}

function premiumcheck() {
    premiumname = $("#premiumname").val();
    price = $("#price").val();
    noofdays = $("#noofdays").val();
    if ($.trim(premiumname) == "") {
    }
}

function changeuserstatus(id, status) {
    $("#" + id).remove();
    $.ajax({
        url: APP_URL + '/admin/changestatus/' + id,
        type: "get",
        dataType: "html",
        data: {'status': status},
        success: function (responce) {

        }
    });
}

function ignorereporteduser(id) {
    $("#" + id).remove();
    $.ajax({
        url: APP_URL + '/admin/ignorereporting/' + id,
        type: "get",
        dataType: "html",
        success: function (responce) {

        }
    });
}

function sendnotification() {
    var notificationText = $('.notification').val();
    $('.form-group').removeClass('has-error');
    if (notificationText == "") {
        $('.form-group').addClass('has-error');
        return false;
    } else {
        $.ajax({
            url: APP_URL + '/admin/sendnotification',
            type: "post",
            data: {'notificationText': notificationText},
            beforeSend: function () {
                $('.sendload').html('Sending....');
            },
            success: function () {
                $('.notification').val("");
                $('.sendload').html('Sent !');
                setTimeout(function () {
                    $('.sendload').html('Send');
                }, 3000);
            }
        });
    }

}

$(document).ready(function () {
    $("tr").each(function (index) {
        if ($(this).find("#is_activated").text() == 'No') {
            $(this).find("#active").text("Activate")

        } else {
            $(this).find("#active").text("Active").addClass("btn-success").attr('disabled', true);
        }
    });


    $('.braintree').on("click", function () {
        $(".inappsetting").hide();
        $(".braintreesetting").show();

    });
    $(".inapp").on("click", function () {

        $(".inappsetting").show();


        $(".braintreesetting").hide();
    });

    $("tr").on('click', '#active', function () {

        var status = 1;

        var id = $(this).data('user');

        $.ajax({
            url: 'girls/activate', // url where to submit the request
            type: "post", // type of action POST || GET
            // dataType: 'json', // data type
            data: {
                'is_activated': status,
                'id': id
            }, // post data || get data
            success: function (result) {
                if (result == "true") {
                    // $(this).text("Active").addClass("btn-success");
                    location.reload();
                }
            },
        });
    });

    $("tr").on('click', '#addpremiumsatatus', function () {

        var premiumstatus = 1;

        var id = $(this).data('user-addpremium');
        var premium_size = $(this).siblings('#premiumdatesize').val();

        if (premium_size == '') {
            alert("Please, input date value");
        } else {
            $.ajax({
                url: 'premium/add', // url where to submit the request
                type: "post", // type of action POST || GET
                // dataType: 'json', // data type
                data: {
                    'premium': premiumstatus,
                    'id': id,
                    'premium_size': premium_size
                }, // post data || get data
                success: function (result) {
                    if (result == "true") {
                        // $(this).text("Active").addClass("btn-success");
                        location.reload();
                    }
                },
            });
        }
    });

    $("tr").on('click', '#removepremiumsatatus', function () {

        var premiumstatus = 0;

        var id = $(this).data('user-removepremium');

        $.ajax({
            url: 'premium/add', // url where to submit the request
            type: "post", // type of action POST || GET
            // dataType: 'json', // data type
            data: {
                'premium': premiumstatus,
                'id': id
            }, // post data || get data
            success: function (result) {
                if (result == "true") {
                    // $(this).text("Active").addClass("btn-success");
                    location.reload();
                }
            },
        });
    });

    $("tr").on('click', '#delete', function () {


        var id = $(this).data('user-delete');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete user!'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your user has been deleted.',
                    'success'
                );
                $.ajax({
                    url: 'users/delete',
                    type: "post",

                    data: {
                        'id': id
                    }, // post data || get data
                    success: function (result) {
                        if (result) {
                            location.reload();
                        }
                    },
                });
            }
        })

    });

    $("tr").on('click', '#deletepremium', function () {


        var id = $(this).data('user-premium-delete');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete user!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '/admin/premium/delete',
                    type: "post",

                    data: {
                        'id': id
                    }, // post data || get data
                    success: function (result) {
                        if (result) {
                            location.reload();
                        }
                        Swal.fire(
                            'Deleted!',
                            'Premium has been deleted.',
                            'success'
                        );
                    },
                });

            }
        })

    });

    $(function () {
        $("#gphone").mask("999999999999");
    });

    $("#girlsform").submit(function (e) {
        e.preventDefault();
        var url = '/admin/girls';
        var data = new FormData(this);

        $.ajax({
            type: "POST",
            url: url,
            dataType: "JSON",
            data: data,
            processData: false,
            contentType: false, // serializes the form's elements.
            success: function (data) {
                console.log(data);

                // alert(data); // show response from the php script.
            }
        });
        location.reload();
        // avoid to execute the actual submit of the form.
    });
    // $(function() {
    //     $("#datepicker").datepicker();
    // });
});
