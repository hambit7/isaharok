'use strict';

var os = require('os');
var socket = require('socket.io');
var express = require('express');
var http = require('http');

var app = express();
var server = http.createServer(app);

var io = socket.listen(server);

io.sockets.on('connection', function (client) {
    console.log("New client !");

    client.on('message', function (data) {
        console.log('Message received ' + data.sender_id + ":" + data.message);
        io.sockets.in('/normal/' + data.receiver_id).emit('message', {
            sender_id: data.sender_id,
            receiver_id: data.receiver_id,
            message: data.message
        });
    });
    client.on('join', function (data) {
        console.log('Message received ' + data.join_id);
        client.join('/normal/' + data.join_id);
        //io.sockets.emit( 'join', { name: data.joinid } );
    });
    client.on('typing', function (data) {
        io.sockets.in('/normal/' + data.receiver_id).emit('typing', data);
    });
    client.on('create or join', function (room) {
        console.log('Received request to create or join room ', room);

        client.join(room);
        var clients = io.sockets.adapter.rooms[room].sockets;
        var numClients = (typeof clients !== 'undefined') ? Object.keys(clients).length : 0;

        log('Room ' + room + ' now has ' + numClients + ' client(s)');

        if (numClients === 1) {
            log('Client ID ' + client.id + ' created room ' + room);
            client.emit('created', room, client.id);

        } else if (numClients === 2) {
            log('Client ID ' + client.id + ' joined room ' + room);
            io.sockets.in(room).emit('join', room);
            client.emit('joined', room, client.id);
            io.sockets.in(room).emit('ready');
        } else {
            console.log('full', room);
        }
    });

    function log() {
        var array = ['Message from server:'];
        array.push.apply(array, arguments);
        console.log.apply(console, array);
    }

    client.on('rtcmessage', function (data) {
        log('Client says: ', data);
        if (data.type != "candidate")
            io.sockets.in(data.room).emit('rtcmessage', data.message);
        else
            io.sockets.in(data.room).emit('rtcmessage', data);
        //client.broadcast.emit('rtcmessage', message);
    });

    client.on('bye', function (room) {
        console.log('received bye from ', room);
        console.log('received count in ', (typeof io.sockets.adapter.rooms[room] !== 'undefined') ? io.sockets.adapter.rooms[room].length : 0);
        client.leave(room);
        console.log('received count out ', (typeof io.sockets.adapter.rooms[room] !== 'undefined') ? io.sockets.adapter.rooms[room].length : 0);
        //client.broadcast.emit('bye', room);
        io.sockets.in(room).emit('bye', room);

    });


});

server.listen(8085);
//root:PCPEDZ9vsgB2
