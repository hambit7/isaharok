<?php


return array(
    'images' => array(

        'paths' => array(
            'input' => 'uplods/images',
            'output' => 'uploads/resized'
        ),

        'sizes' => array(
            'small' => array(
                'width' => 150,
                'height' => 100
            ),
            'big' => array(
                'width' => 600,
                'height' => 400
            )
        )
    )

);
