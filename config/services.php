<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'firebase' => [
        'type' => env('FIREBASE_API_TYPE'),
        'project_id' => env('FIREBASE_API_PROJECT_ID'),
        'private_key_id' => env('FIREBASE_API_PRIVATE_KEY_ID'),
//        'private_key' => env('FIREBASE_API_PRIVATE_KEY'),
        'private_key' => "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCZEnYvOm5Ee1Q0\nxuzJzDtQ9Ljt4EjPG5OZJrQhr45J8t7fpZiOBQ2n+sEw7UbKV+9H02qPOyzSzr9Y\nqGzG74jss9JLoVYf1oEX8TO83NTJoFV8da3A97nVxxNbQi+Rpa7UAkRmPhQbJpg9\n0puASNttMTVCAXvxv/Sn7kTjnHEeDqsbxwzo9gavJMVthOuQsi/lsfh76vN59rhb\nkZMdrd1u1aX0m/glNtiGySKVLtWXKCxeyYXMPZi+KceNJYRE3yYiwt8r35Scgg/9\nsrCMYaPdtMAWRBgTUIdL7eoWNQ2D7GFUreakNiLrTNypXVhjuxIpAXEvQt2aNyA7\nx4ddTwvbAgMBAAECggEADDR6IurkRuaUgL7Ju49ivWI96o2jEIxw2NnKrBLs4/2P\nAsMX5l8FxzqJs+Z/afvX8uCrEu+Tc92TSTTos3X+82ylHG2pSdGXKnsLdogfGVKE\n7m8NoyC9ewusAe9FjrXoujPSE1rozH9nzSohRvnIMS0mw3QN12lITymC6akcJdrC\n9hubqEhZn0L33nru3QQi0S3k7pY3kkEEpiOD8LQ/WDJqXb7lCaNJTlxJ/0KGn/eu\nFDcu8eWp4AuFYP9PiHYbzYlNRX5cSpyqjK7PdPYveR2pYo+snu9Pxvcve9zAi2M6\n7190QlgB1J8/GOVN7rsp1Q5xg5Q9+QuJ+HlmAQuGVQKBgQDOoK8fEUdwsL+t5m5C\nUvjkzVypaazZ94YexZLrYaeiSZbg40tpLf+zx8UYED6ZiiyRFvsHPqVjwGrHYKvo\np9RxikatjXoXYAolIjlb2GDiJKHg+wtoDMECA/hcVNg8nR7actAZYZYytZ9e/RSB\n38D2ieiOJCelB6sb8EYRJOMUJQKBgQC9pc9P5wQBbKsLQhIQS5BL41Csf0OBXvEb\nWqi/F5pZ/N1ARVq1ygF6J2jxrb8UBplg1wR/oH/k9HesmFGB9B2UDw/DQMsnszk5\n4u1oNmik82PtIZZZO8rJ7YAA4Xbb6ueS9BJi/DZa7eDYw49RuZeoiC4+SYH54AAZ\nXDfH9r+f/wKBgQCIlYzM84YOxrcdw7lUnDYsQFelINaCim0cwCs67qV50D0s0PKT\nRzXEaCQvoOVQZxbInbE3+8Al7RZoTs20D+SNXQMVS/TNqM1r+HCD9scsFVzwM+I5\nWFzo8SjmF6uMfavcaJKIhIpdYNNFXfOFWhvanBSeiOWv4B7KbAxU3tZABQKBgAmv\ndSX8g1zbQuUBS9DfOxBVHKKioTzzL1cZSzLvLqfX3SjSIe7U0Tx+1MI7AF4ydBTZ\nBlbKSJ3VymLbKJZ9dCFCn6h2H6n4YjzNlRhUZwW9lGXg6D/IaPPlGDtzSFG07ny3\n0FST0ubT7T7ExgYDd3ZY9huiP2wWf4rMBeFFfcXbAoGBALd1QshMIeVa+/cB4Ft/\na2o21EEsPIx4E++M4Qx+vMYSBTSmUqu5C+tgW1P3eT9j7qXh3OZLv3p/9wQn/u0T\nikw1PmWs7AGfTRLL+t3ZbFqc+NpUgLXUmbxNxiAyn8g4eVLYItsKUfkwmh5+h/zr\nvjUlpFO1o9Y7MkuIE2GonZbT\n-----END PRIVATE KEY-----\n",
        'client_email' => env('FIREBASE_API_CLIENT_EMAIL'),
        'client_id' => env('FIREBASE_API_CLIENT_ID'),
        'auth_uri' => env('FIREBASE_API_AUTH_URI'),
        'token_uri' => env('FIREBASE_API_TOKEN_URI'),
        'auth_provider_x509_cert_url' => env('FIREBASE_API_AUTH_PROVIDER_X509_CERT_URL'),
        'client_x509_cert_url' => env('FIREBASE_API_CLIENT_X509_CERT_URL')
    ],

];
