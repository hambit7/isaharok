<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');

?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Relationship Details'); ?> </div>
                    <div class="panel-body">

                        <a href="<?php echo e(url($baseurl . '/admin/peoplefor/' . $peoplefor->id . '/edit')); ?>"
                           class="btn btn-primary btn-xs" title="Edit Peoplefor"><span
                                    class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        <?php echo Form::open([
                            'method' => 'DELETE',
                            'url' => [$baseurl . '/admin/peoplefor/' . $peoplefor->id],
                            'style' => 'display:inline'
                        ]); ?>

                        <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Peoplefor',
                            'onclick' => 'return confirm("Confirm delete?")'
                        )); ?>

                        <?php echo Form::close(); ?>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td><?php echo e($peoplefor->id); ?></td>
                                </tr>
                                <tr>
                                    <th> <?php echo app('translator')->get('app.Name'); ?> </th>
                                    <td> <?php echo e($peoplefor->name); ?> </td>
                                </tr>
                                <tr>
                                    <th> <?php echo app('translator')->get('app.Image Name'); ?> </th>
                                    <td> <?php echo e(HTML::image('/uploads/' . $peoplefor->imagename, 'a picture', array('class' => 'thumbcls1'))); ?> </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
