<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <head>
        <style type="text/css">
            .centerText {
                text-align: center;
            }
        </style>
    </head>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Interests'); ?></div>
                    <div class="panel-body">

                        <a href="<?php echo e(url($baseurl . '/admin/interests/create')); ?>"
                           class="btn btn-primary btn-md" title="Add New Interest">
                            <span class="glyphicon glyphicon-plus"
                                  aria-hidden="true"/></span> <?php echo app('translator')->get('app.Add New Interest'); ?>
                        </a>
                        <br/>
                        <br/>
                        <div class="col-sm-4 m-b-xs no-h-padding">

                            <form class="form-horizontal" role="form" method="POST"
                                  action="<?php echo e(url($baseurl . '/admin/interests')); ?>">

                                <div class="input-group">
                                    <input type="text" name="search" autocomplete="off" value="<?php echo $search; ?>"
                                           class="input-sm form-control"
                                           placeholder="<?php echo app('translator')->get('app.Search'); ?>">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <span class="input-group-btn">
                                  <button class="btn btn-sm btn-default"
                                          type="submit"><?php echo app('translator')->get('app.Go!'); ?></button>
                              </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive clears">
                            <table class="table table-striped m-b-none table-borderless" data-ride="datatables">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('app.ID'); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Name'))); ?> </th>
                                    <th><?php echo app('translator')->get('app.Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = count($interests);

                                if ($count > 0) { ?>
                                    <?php foreach ($interests as $item): ?>
                                        <tr>
                                            <td><?php echo e($item->id); ?></td>
                                            <td><?php echo e($item->name); ?></td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/interests/' . $item->id . '/edit')); ?>"
                                                   class="btn btn-primary btn-xs" title="Edit Interest"><span
                                                            class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                                <?php echo Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/admin/interests', $item->id],
                                                    'style' => 'display:inline'
                                                ]); ?>

                                                <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Interest" />', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete Interest',
                                                    'onclick' => 'return confirm("Confirm delete?")'
                                                )); ?>

                                                <?php echo Form::close(); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php
                                } else
                                    echo '<tr><td colspan="4"class="centerText">' . Lang::get('app.Sorry...! No interests found') . '.</td></tr>'; ?>
                                </tbody>
                            </table>

                            <div class="pagination-wrapper"> <?php echo $interests->render(); ?> </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
