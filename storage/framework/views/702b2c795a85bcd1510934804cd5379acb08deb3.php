<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Approved Users'); ?></div>
                    <div class="panel-body">

                        <!--a href="<?php echo e(url('/admin/users/create')); ?>" class="btn btn-primary btn-xs" title="Add New User"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a-->

                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="<?php echo e(url($baseurl . '/admin/approved')); ?>">
                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search; ?>"
                                           class="input-sm form-control"
                                           placeholder="<?php echo app('translator')->get('app.Search'); ?>">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default"
                                            type="submit"><?php echo app('translator')->get('app.Go!'); ?></button>
                                </span>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-4 m-b-xs"><a href="<?php echo e(url('/admin/writer/users')); ?>"
                                                        class="btn btn-success no-margin"> To CSV </a></div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('app.ID'); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Firstname'), Lang::get('app.First Name'))); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Email'))); ?></th>
                                    <th><?php echo app('translator')->get('app.View'); ?></th>
                                    <th><?php echo app('translator')->get('app.gesture-photo'); ?></th>
                                    <th><?php echo app('translator')->get('app.Actions'); ?></th>
                                    <th>Premium</th>
                                    <th>Premium off</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $noofusers = count($users);
                                $i = 1;
                                if ($noofusers > 0) { ?>
                                    <?php foreach ($users as $item): ?>
                                        <tr id="<?php echo $item->id; ?>">
                                            <td><?php echo e($i); ?></td>
                                            <td><?php echo e($item->firstname); ?></td>
                                            <td><?php echo e($item->email); ?></td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/viewapproveduser/' . $item->id)); ?>"
                                                   class="btn btn-success btn-xs" title="View Approved Users"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"/></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/content/' . $item->id)); ?>"
                                                   class="btn btn-success btn-xs" title="View  gesture photo"><span
                                                            class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/changestatus/' . $item->id . '/' . '0')); ?>"
                                                   title="Disable User"><input type="submit"
                                                                               class="btn btn-s-md btn-warning"
                                                                               value="Disable"></button></a>
                                                <!--  <?php echo Form::button('Disable', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-s-md btn-warning',
                                                    'title' => 'Disable User',
                                                    'value' => 'Disable',
                                                    'onclick' => 'return changeuserstatus(' . $item->id . ',0);',
                                                )); ?> -->
                                            </td>
                                            <td id="addpremium">
                                                <?php if ($item->gender == 'male' && $item->premiumstatus == '0'): ?>
                                                    <div>
                                                        <?php /*                                                <form>*/ ?>
                                                        <input type="number" id="premiumdatesize" class="form-control"
                                                               placeholder="Days"><br>

                                                        <button data-user-addpremium="<?php echo e($item->id); ?>"
                                                                class="btn btn-success"
                                                                id="addpremiumsatatus">Add premium
                                                        </button>
                                                        <?php /*                                                </form>*/ ?>
                                                    </div>
                                                <?php elseif ($item->gender == 'male' && $item->premiumstatus == '1'): ?>
                                                    <div id="datapremium">


                                                        <button data-user-addpremium="<?php echo e($item->id); ?>"
                                                                class="btn btn-default" disabled
                                                                id="addpremiumsatatus">Add premium
                                                        </button>
                                                    </div>

                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if ($item->premium_to != 0): ?>
                                                    <?php echo e(date('d-m-Y', $item->premium_to)); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td id="deleting">

                                                <div>
                                                    <button data-user-delete="<?php echo e($item->id); ?>"
                                                            class="btn btn-danger"
                                                            id="delete">Delete
                                                    </button>
                                                </div>
                                            </td>
                                            <!--td>
                                            <a href="<?php echo e(url('/admin/users/' . $item->id)); ?>" class="btn btn-success btn-xs" title="View User"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="<?php echo e(url('/admin/users/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs" title="Edit User"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            <?php echo Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/users', $item->id],
                                                'style' => 'display:inline'
                                            ]); ?>

                                    <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete User',
                                                'onclick' => 'return confirm("Confirm delete?")'
                                            )); ?>

                                    <?php echo Form::close(); ?>

                                            </td-->
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                <?php } else
                                    echo '<tr><td colspan="4" class="centerText">Sorry...! No User is found.</td></tr>';
                                ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $users->render(); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php /*    <script>*/ ?>
<?php /*        $( function() {*/ ?>
<?php /*            $( "#datepicker" ).datepicker();*/ ?>
<?php /*        } );*/ ?>
<?php /*    </script>*/ ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
