<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <head>
        <style type="text/css">
            .centerText {
                text-align: center;
            }
        </style>
    </head>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Manage Relationship'); ?></div>
                    <div class="panel-body">

                        <a href="<?php echo e(url($baseurl . '/admin/peoplefor/create')); ?>"
                           class="btn btn-primary btn-md" title="Add New Peoplefor">
                            <span class="glyphicon glyphicon-plus"
                                  aria-hidden="true"/></span> <?php echo app('translator')->get('app.Add Relationship'); ?>
                        </a>
                        <br/>
                        <br/>
                        <div class="col-sm-4 m-b-xs no-h-padding">

                            <form class="form-horizontal" role="form" method="POST"
                                  action="<?php echo e(url($baseurl . '/admin/peoplefor')); ?>">

                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search; ?>"
                                           class="input-sm form-control"
                                           placeholder="<?php echo app('translator')->get('app.Search'); ?>">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <span class="input-group-btn">
                          <button class="btn btn-sm btn-default"
                                  type="submit"><?php echo app('translator')->get('app.Go!'); ?></button>
                        </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive clears">
                            <table class="table table-striped m-b-none table-borderless" data-ride="datatables">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('app.ID'); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Name'))); ?> </th>
                                    <th> <?php echo app('translator')->get('app.Image Name'); ?> </th>
                                    <th><?php echo app('translator')->get('app.Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = count($peoplefor);

                                if ($count > 0) { ?>
                                    <?php foreach ($peoplefor as $item): ?>
                                        <tr>
                                            <td><?php echo e($item->id); ?></td>
                                            <td><?php echo e($item->name); ?></td>
                                            <td><?php echo e(HTML::image('/uploads/' . $item->imagename, 'a picture', array('class' => 'thumbcls'))); ?></td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/peoplefor/' . $item->id)); ?>"
                                                   class="btn btn-success btn-xs" title="View"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"/></a>
                                                <a href="<?php echo e(url($baseurl . '/admin/peoplefor/' . $item->id . '/edit')); ?>"
                                                   class="btn btn-primary btn-xs" title="Edit"><span
                                                            class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                                <?php echo Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/admin/peoplefor', $item->id],
                                                    'style' => 'display:inline'
                                                ]); ?>

                                                <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Peoplefor" />', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete Peoplefor',
                                                    'onclick' => 'return confirm("Confirm delete?")'
                                                )); ?>

                                                <?php echo Form::close(); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php
                                } else
                                    echo '<tr><td colspan="4"class="centerText">' . Lang::get('app.Sorry...! No Relationship found.') . '.</td></tr>'; ?>
                                </tbody>
                            </table>

                            <div class="pagination-wrapper"> <?php echo $peoplefor->render(); ?> </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
