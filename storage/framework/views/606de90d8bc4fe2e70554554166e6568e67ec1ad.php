<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.General Settings'); ?></div>
                    <div class="panel-body">

                        <?php echo Form::model($landingpagesettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/updatelandingpage'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]); ?>


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('landingpagetitle') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('landingpagetitle', Lang::get('app.Landing Page Title'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('landingpagetitle', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('landingpagetitle', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('landingpagesubtitle') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('landingpagesubtitle', Lang::get('app.Landing Page Sub-title'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('landingpagesubtitle', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('landingpagesubtitle', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('copyrightinfo') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('copyrightinfo', Lang::get('app.Copy Rights Information'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('copyrightinfo', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('copyrightinfo', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('landingpagelogo') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('landingpagelogo', Lang::get('app.Landing Page Logo'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::file('landingpagelogo', ['class' => 'filestyle', 'name' => 'landingpagelogo', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']); ?>

                                <?php echo $errors->first('landingpagelogo', '<p class="help-block">:message</p>'); ?>

                                <?php if ($landingpagesettings->landingpagelogo != "") ?>
                                <div>
                                    <img src="<?php echo url('/uploads/landingpage/' . $landingpagesettings->landingpagelogo); ?>"
                                         style="max-width:150px;max-height:150px;margin-top:10px;background-color: #ededed;">
                                </div>
                            </div>
                        </div>

                        <div class="form-group <?php echo e($errors->has('subpagelogo') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('subpagelogo', Lang::get('app.Inner Page Logo'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::file('subpagelogo', ['class' => 'filestyle', 'name' => 'subpagelogo', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']); ?>

                                <?php echo $errors->first('subpagelogo', '<p class="help-block">:message</p>'); ?>

                                <?php if ($landingpagesettings->subpagelogo != "") ?>
                                <div>
                                    <img src="<?php echo url('/uploads/landingpage/' . $landingpagesettings->subpagelogo); ?>"
                                         style="max-width:150px;max-height:150px;margin-top:10px;background-color: #ededed;">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(Lang::get('app.Update'), ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
