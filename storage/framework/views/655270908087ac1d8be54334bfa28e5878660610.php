<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Premium Users'); ?></div>
                    <div class="panel-body">

                        <!--a href="<?php echo e(url('/admin/users/create')); ?>" class="btn btn-primary btn-xs" title="Add New User"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a-->


                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="<?php echo e(url($baseurl . '/admin/premiumuser')); ?>">
                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search; ?>"
                                           class="input-sm form-control"
                                           placeholder="<?php echo app('translator')->get('app.Search'); ?>">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default"
                                            type="submit"><?php echo app('translator')->get('app.Go!'); ?></button>
                                </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('app.ID'); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Firstname'), Lang::get('app.First Name'))); ?></th>
                                    <th>
                                        <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Email'))); ?>
                                    </th>
                                    <th>
                                        <?php echo app('translator')->get('app.Actions'); ?>
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                $noofusers = count($users);
                                if ($noofusers > 0) { ?>
                                    <?php foreach ($users as $item): ?>
                                        <tr id="<?php echo $item->id; ?>">
                                            <td><?php echo e($i); ?></td>
                                            <td><?php echo e($item->firstname); ?></td>
                                            <td><?php echo e($item->email); ?></td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/viewpremiumuser/' . $item->id)); ?>"
                                                   class="btn btn-success btn-xs" title="View Premium Users"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"/></a>
                                            </td>


                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/changestatus/' . $item->id . '/' . '0')); ?>"
                                                   title="Disable User"><input type="submit"
                                                                               class="btn btn-s-md btn-warning"
                                                                               value="Disable"></button></a>
                                            </td>

                                            <?php /*<td id="deleting">*/ ?>

                                            <?php /*<div>*/ ?>
                                            <?php /*<button data-user-delete="<?php echo e($item->id); ?>" class="btn btn-danger"*/ ?>
                                            <?php /*id="delete">Delete*/ ?>
                                            <?php /*</button>*/ ?>
                                            <?php /*</div>*/ ?>
                                            <?php /*</td>*/ ?>
                                            <!--td>
                                            <a href="<?php echo e(url('/admin/users/' . $item->id)); ?>" class="btn btn-success btn-xs" title="View User"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="<?php echo e(url('/admin/users/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs" title="Edit User"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>

                                            </td-->
                                            <td id="removepremium">
                                                <?php if ($item->gender == 'male' && $item->premiumstatus == '1'): ?>
                                                    <div>
                                                        <button data-user-removepremium="<?php echo e($item->id); ?>"
                                                                class="btn btn-danger"
                                                                id="removepremiumsatatus">Remove premium
                                                        </button>
                                                    </div>

                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                <?php } else
                                    echo '<tr><td colspan="4" class="centerText">Sorry...! No User is found.</td></tr>';
                                ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $users->render(); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
