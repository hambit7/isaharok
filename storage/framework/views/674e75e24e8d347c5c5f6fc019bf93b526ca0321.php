<?php $myclass = app('App\Classes\MyClass'); ?>


<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
$chartColors = ['#ff3d7d', '#3db4ff', '#ff463d', '#1ac67b', '#1a6bc6'];
?>

    <section class="row m-b-md">
        <div class="col-sm-6">
            <h3 class="m-b-xs text-black"><?php echo app('translator')->get('app.Dashboard'); ?></h3>
        </div>

    </section>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel b-a">
                <div class="row m-n">
                    <div class="col-md-6 b-b b-r">
                        <a href="javascript:void(0);" class="block padder-v hover">
										<span class="i-s i-s-2x pull-left m-r-sm">
											<i class="i i-hexagon2 i-s-base text-danger hover-rotate"></i>
											<i class="i i-users2 i-1x text-white"></i>
										</span>
                            <span class="clear">
											<span class="h3 block m-t-xs text-danger"><?php echo $totaluser; ?></span>
											<small class="text-muted text-u-c"><?php echo app('translator')->get('app.Users'); ?></small>
										</span>
                        </a>
                    </div>
                    <div class="col-md-6 b-b">
                        <a href="<?php echo $baseurl; ?>/admin/premiumuser" class="block padder-v hover">
										<span class="i-s i-s-2x pull-left m-r-sm">
											<i class="i i-hexagon2 i-s-base text-success-lt hover-rotate"></i>
											<i class="i i-users2 i-sm text-white"></i>
										</span>
                            <span class="clear">
											<span class="h3 block m-t-xs text-success"><?php echo $premiumuser; ?></span>
											<small class="text-muted text-u-c"><?php echo app('translator')->get('app.Premium Users'); ?></small>
										</span>
                        </a>
                    </div>
                    <div class="col-md-6 b-b b-r">
                        <a href="<?php echo $baseurl; ?>/admin/approved" class="block padder-v hover">
										<span class="i-s i-s-2x pull-left m-r-sm">
											<i class="i i-hexagon2 i-s-base text-info hover-rotate"></i>
											<i class="i i-users2 i-sm text-white"></i>
										</span>
                            <span class="clear">
											<span class="h3 block m-t-xs text-info"><?php echo $activeusers; ?></span>
											<small class="text-muted text-u-c"><?php echo app('translator')->get('app.Active Users'); ?></small>
										</span>
                        </a>
                    </div>
                    <div class="col-md-6 b-b">
                        <a href="<?php echo $baseurl; ?>/admin/unapproved" class="block padder-v hover">
										<span class="i-s i-s-2x pull-left m-r-sm">
											<i class="i i-hexagon2 i-s-base text-primary hover-rotate"></i>
											<i class="i i-users2 i-sm text-white"></i>
										</span>
                            <span class="clear">
											<span class="h3 block m-t-xs text-primary"><?php echo $inactiveusers; ?></span>
											<small class="text-muted text-u-c"><?php echo app('translator')->get('app.InActive Users'); ?></small>
										</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="panel b-a">
                <div class="panel-heading no-border bg-primary lt text-center">
                    <a href="#">
                        <i class="fa fa-android fa fa-3x m-t m-b text-white"></i>
                    </a>
                </div>
                <div class="padder-v text-center clearfix">
                    <div class="col-xs-6 b-r">
                        <div class="h3 font-bold"><?php echo $myclass->custom_number_format($androidDevices); ?></div>
                        <small class="text-muted"><?php echo app('translator')->get('app.Device'); ?></small>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo $baseurl; ?>/admin/androidconfig" class="block hover">
                            <div class="h3 font-bold">
                                <i class="fa fa-cog fa fa-1x text-muted  hover-rotate"></i>
                            </div>
                            <small class="text-muted"><?php echo app('translator')->get('app.Configure'); ?></small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="panel b-a">
                <div class="panel-heading no-border bg-info lter text-center">
                    <a href="#">
                        <i class="fa fa-apple fa fa-3x m-t m-b text-white"></i>
                    </a>
                </div>
                <div class="padder-v text-center clearfix">
                    <div class="col-xs-6 b-r">
                        <div class="h3 font-bold"><?php echo $myclass->custom_number_format($iosDevices); ?></div>
                        <small class="text-muted"><?php echo app('translator')->get('app.Device'); ?></small>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo $baseurl; ?>/admin/iosconfig" class="block hover">
                            <div class="h3 font-bold">
                                <i class="fa fa-cog fa fa-1x text-muted  hover-rotate"></i>
                            </div>
                            <small class="text-muted"><?php echo app('translator')->get('app.Configure'); ?></small>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>

<?php if (!empty($premiumUsageDetails)) { ?>
    <div class="row">
        <div class="col-lg-4">
            <section class="panel panel-default txtnotify">
                <header class="panel-heading font-bold"><i
                            class="fa fa-bullhorn fa fa-1x text-muted  hover-rotate"></i> <?php echo app('translator')->get('app.Send Notification'); ?>
                </header>
                <div class="panel-body">
                    <div class="form-group">
                        <h4><?php echo app('translator')->get('app.Notification Text'); ?></h4>
                        <textarea class="form-control notification" rows="8" style="resize: none;"></textarea>
                    </div>
                    <button class="btn btn-md btn-default sendload" onclick="sendnotification();">
                        <?php echo app('translator')->get('app.Send'); ?>
                    </button>
                </div>
            </section>
        </div>
        <?php foreach ($premiumUsageDetails as $premiumUsage) {
            $colorCode = rand(0, 4);
            if ($totalRevenue > 0)
                $premiumRevenuePrecent = round((($premiumUsage['premiumRevenue'] * 100) / $totalRevenue), 2);
            else
                $premiumRevenuePrecent = 0;
            ?>
            <div class="col-lg-4">
                <section class="panel panel-default">
                    <header class="panel-heading">
                        <?php echo $premiumUsage['premiumName']; ?>
                        <?php if ($premiumUsage['premiumPlatform'] == "android") {
                            echo '<i class="fa fa-android icon">
											</i>';
                        } else {
                            echo '<i class="fa fa-apple icon">
											</i>';
                        }
                        ?>
                    </header>
                    <div class="panel-body text-center">
                        <h4><?php echo $myclass->custom_number_format($premiumUsage['premiumPrice']); ?>
                            <small> <?php echo $siteCurrency[0]; ?></small>
                        </h4>
                        <small class="text-muted block"><?php echo $premiumUsage['premiumPeriod']; ?><?php echo app('translator')->get('app.Days Validity'); ?></small>
                        <div class="inline">
                            <div class="easypiechart" data-percent="<?php echo $premiumUsage['premiumPercentage']; ?>"
                                 data-bar-color="<?php echo $chartColors[$colorCode]; ?>" data-line-width="14"
                                 data-loop="false" data-size="188" data-scale-color='#fff' data-line-cap='butt'>
                                <div>
                                    <span class="h2 m-l-sm step"><?php echo $premiumUsage['premiumPercentage']; ?></span>%
                                    <div class="text text-sm"><?php echo app('translator')->get('app.Users'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <small><?php echo app('translator')->get('app.Covered'); ?> <?php echo $premiumRevenuePrecent; ?>
                            % <?php echo app('translator')->get('app.of total revenue'); ?> <?php echo $myclass->custom_number_format($totalRevenue) . " " . $siteCurrency[0]; ?></small>
                    </div>
                </section>
            </div>
        <?php } ?>
    </div>
<?php } ?>
    <div class="brand-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span><?php echo $landingpagesettings->copyrightinfo; ?></span>
                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
