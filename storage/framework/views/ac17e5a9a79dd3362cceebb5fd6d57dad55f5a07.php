<?php $__env->startSection('content'); ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Approved User'); ?></div>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <!--  <tr>
                                        <th>ID</th><td><?php echo e($user->id); ?></td>
                                </tr> -->
                            <?php if ($user->gender == 'male'): ?>

                                <tr>

                                    <th>Photo</th>
                                    <td><?php echo e($user->image); ?></td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <th>First name</th>
                                <td><?php echo e($user->firstname); ?></td>
                            </tr>
                            <tr>
                                <th>User name</th>
                                <td><?php echo e($user->username); ?></td>
                            </tr>
                            <tr>
                                <th>Email ID</th>
                                <td><?php echo e($user->email); ?></td>
                            </tr>
                            <tr>
                                <th>Age</th>
                                <td><?php echo e($user->age); ?></td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td><?php echo e($user->phone); ?></td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td><?php echo e($user->location); ?></td>
                            </tr>
                            <?php if ($user->gender == 'female'): ?>
                                </tr>
                                <td>
                                    <a href="<?php echo e(url('/admin/gallery/' . $user->id)); ?>"
                                       class="btn btn-success" title="View girl photo"><span
                                                class="glyphicon glyphicon-camera" aria-hidden="false"/></a>
                                </td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
