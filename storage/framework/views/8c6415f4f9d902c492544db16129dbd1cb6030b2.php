<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.girls'); ?></div>
                    <div class="panel-body">


                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="GET"
                                  action="<?php echo e(url($baseurl . '/admin/girls')); ?>">
                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search; ?>"
                                           class="input-sm form-control"
                                           placeholder="<?php echo app('translator')->get('app.Search'); ?>">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default"
                                            type="submit"><?php echo app('translator')->get('app.Go!'); ?></button>
                                    <button class="btn btn-sm btn-default"><a href="<?php echo e(request()->url()); ?>">Clear</a></button>
                                </span>
                                </div>
                            </form>
                        </div>


                        <div class="col-sm-5 m-b-xs">
                            <a href="<?php echo e(url('/admin/girls/create')); ?>" class="btn btn-primary btn-xs"
                               title="Add New Girl"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('#'); ?></th>
                                    <?php /*<th><?php echo app('translator')->get('app.ID'); ?></th>*/ ?>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Firstname'), Lang::get('app.First Name'))); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Email'))); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Confirmed'))); ?></th>
                                    <th><?php echo app('translator')->get('app.View'); ?></th>
                                    <th><?php echo app('translator')->get('app.Photo'); ?></th>
                                    <th><?php echo app('translator')->get('app.gesture-photo'); ?></th>
                                    <th><?php echo app('translator')->get('app.Last_active'); ?></th>
                                    <th><?php echo app('translator')->get('app.Activate'); ?></th>
                                    <th><?php echo app('translator')->get('app.Delete'); ?></th>
                                    <?php /*<th><?php echo app('translator')->get('app.Actions'); ?></th>*/ ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($girls): ?>

                                <?php foreach ($girls

                                as $key => $item): ?>
                                <tr>
                                    <td><?php echo e($key + 1); ?></td>
                                    <td id="girlID" hidden><?php echo e($item->id); ?></td>
                                    <td><?php echo e($item->firstname); ?></td>
                                    <td><?php echo e($item->email); ?></td>
                                    <?php if ($item->userstatus): ?>
                                        <td id="is_activated"><?php echo e("Yes"); ?></td>
                                    <?php else: ?>
                                        <td id="is_activated"><?php echo e("No"); ?></td>
                                    <?php endif; ?>
                                    <td>
                                        <a href="<?php echo e(url($baseurl . '/admin/viewapproveduser/' . $item->id)); ?>"
                                           class="btn btn-success btn-xs" title="View girl profile"><span
                                                    class="glyphicon glyphicon-eye-open"
                                                    aria-hidden="true"/></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo e(url($baseurl . '/admin/gallery/' . $item->id)); ?>"
                                           class="btn btn-success btn-xs" title="View girl photo"><span
                                                    class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo e(url($baseurl . '/admin/content/' . $item->id)); ?>"
                                           class="btn btn-success btn-xs" title="View girl gesture photo"><span
                                                    class="glyphicon glyphicon-camera" aria-hidden="true"/></a>
                                    </td>
                                    <td><?php echo e(date('Y-m-d h:m', $item->onlinetimestamp)); ?></td>

                                    <td id="activated">

                                        <div>
                                            <button data-user="<?php echo e($item->id); ?>" class="btn btn-default"
                                                    id="active">Activate
                                            </button>
                                        </div>
                                    </td>

                                    <td id="deleting">

                                        <div>
                                            <button data-user-delete="<?php echo e($item->id); ?>"
                                                    class="btn btn-danger"
                                                    id="delete">Delete
                                            </button>
                                        </div>
                                    </td>


                                    <!--td>
                                            <a href="<?php echo e(url('/admin/users/' . $item->id)); ?>" class="btn btn-success btn-xs" title="View User"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="<?php echo e(url('/admin/users/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs" title="Edit User"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            <?php echo Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/users', $item->id],
                                        'style' => 'display:inline'
                                    ]); ?>

                                        <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete User',
                                        'onclick' => 'return confirm("Confirm delete?")'
                                        )); ?>

                                        <?php echo Form::close(); ?>

                                                </td-->


                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $girls->render(); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
