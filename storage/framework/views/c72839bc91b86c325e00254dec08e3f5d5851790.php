<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.SMTP Settings'); ?></div>
                    <div class="panel-body">

                        <?php //echo "<pre>";print_r($errors);echo "</pre>"; ?>
                        <?php echo Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]); ?>


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('smtpenable') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('smtpenable', Lang::get('app.SMTP'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('smtpenable', 1, Null); ?>

                                        <i></i>
                                        <?php echo app('translator')->get('app.Enable'); ?>
                                    </label>
                                </div>
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('smtpenable', 0, Null); ?>

                                        <i></i>
                                        <?php echo app('translator')->get('app.Disable'); ?>
                                    </label>
                                </div>
                                <?php echo $errors->first('smtpenable', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('smtphost') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('smtphost', Lang::get('app.SMTP Host'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('smtphost', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('smtphost', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('smtpport') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('smtpport', Lang::get('app.SMTP Port'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('smtpport', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('smtpport', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('smtpusername') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('smtpusername', Lang::get('app.SMTP Email'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('smtpusername', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('smtpusername', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('smtppassword') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('smtppassword', Lang::get('app.SMTP Password'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::password('smtppassword', ['class' => 'form-control']); ?>

                                <?php echo $errors->first('smtppassword', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
