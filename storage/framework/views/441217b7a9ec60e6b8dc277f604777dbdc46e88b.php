<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Adsense Settings'); ?></div>
                    <div class="panel-body">

                        <?php if ($errors->any()): ?>
                            <!--ul class="alert alert-danger">
                                <?php foreach ($errors->all() as $error): ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; ?>
                            </ul-->
                        <?php endif; ?>

                        <?php echo Form::model($adssettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/updateadssettings'],
                            'class' => 'form-horizontal',
                            'files' => true,
                        ]); ?>

                        <?php
                        ?>

                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('googleads') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('googleads', Lang::get('app.Google Ads'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php
                                if (isset($sitesettings->googleads) && $sitesettings->googleads == 1) {
                                    $adsEnable = true;
                                    $adsDisable = Null;
                                } else if (isset($sitesettings->googleads) && $sitesettings->googleads == 0) {
                                    $adsEnable = Null;
                                    $adsDisable = true;
                                }
                                ?>
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('googleads', 1, $adsEnable); ?>

                                        <i></i>
                                        <?php echo app('translator')->get('app.Enable'); ?>
                                    </label>
                                </div>
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('googleads', 0, $adsDisable); ?>

                                        <i></i>
                                        <?php echo app('translator')->get('app.Disable'); ?>
                                    </label>
                                </div>
                                <?php echo $errors->first('googleads', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('adsclient') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('adsclient', Lang::get('app.Adsclient'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('adsclient', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('adsclient', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div><!--div class="form-group <?php echo e($errors->has('adslot') ? 'has-error' : ''); ?>">
						    <?php echo Form::label('adslot', Lang::get('app.Adslot'), ['class' => 'col-md-4 control-label']); ?>

						    <div class="col-md-6">
						        <?php echo Form::text('adslot', null, ['class' => 'form-control']); ?>

						        <?php echo $errors->first('adslot', '<p class="help-block">:message</p>'); ?>

						    </div>
						</div-->


                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
