<?php $baseurl = URL::to('/'); ?>
<?php $__env->startSection('title'); ?>
<?php echo "Howzu"; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="home-banner">
        <div class="gradient container-1">
            <div class="header-area">
                <div class="logo">
                    <a class="header-logo img-responsive" href="<?php echo $baseurl; ?>"
                       style="background-image:url('<?php echo url('/uploads/landingpage/' . $landingpagesettings->landingpagelogo); ?>')"></a>
                </div>
                <div class="banner-text">
                    <span class="top-head bold"><?php echo $landingpagesettings->landingpagetitle; ?></span>
                    <span class="top-sub-head medium"><?php echo $landingpagesettings->landingpagesubtitle; ?></span>
                    <?php if (!empty($landingpagesettings->iosapplink) || !empty($landingpagesettings->androidapplink)) { ?>
                        <div class="app-links">
                            <?php if (!empty($landingpagesettings->iosapplink)) ?>
                            <div class="ios-app-icon"><a href="<?php echo $landingpagesettings->iosapplink ?>"
                                                         target="_blank"><img src="images/ios.png"
                                                                              alt="Ios app download"></a></div>
                            <?php if (!empty($landingpagesettings->androidapplink)) ?>
                            <div class="android-app-icon"><a href="<?php echo $landingpagesettings->androidapplink ?>"
                                                             target="_blank"><img src="images/android.png"
                                                                                  alt="Android app download"></a></div>
                        </div>
                    <?php } ?>
                </div>


            </div>

        </div>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span><?php echo $landingpagesettings->copyrightinfo ?></span>
                        <?php foreach ($helppageModel as $helpKey => $helppage) {
                            $slug = str_replace(" ", "-", $helppage->title);
                            echo $helpKey > 0 ? "I " : ""; ?><a class="bold"
                                                                href="<?php echo $baseurl . "/helppage/" . $slug; ?>"><?php echo $helppage->title; ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.landingpagelayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
