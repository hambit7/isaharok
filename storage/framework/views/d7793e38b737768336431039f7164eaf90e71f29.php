<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Android App Configuration'); ?></div>
                    <div class="panel-body">

                        <?php echo Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/mobileappconfig'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]); ?>


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('androidFcmKey') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('androidFcmKey', Lang::get('app.Android FCM Server Key'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('androidFcmKey', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('androidFcmKey', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>

                        <div class="form-group <?php echo e($errors->has('android_version') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('android_version', Lang::get('app.Android App Version'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('android_version', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('android_version', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>

                        <div class="form-group <?php echo e($errors->has('Update') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('Update Type', Lang::get('app.Update Type'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">

                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('android_update', '0', 1); ?>

                                        <i></i>
                                        Normal Update
                                    </label>
                                </div>
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('android_update', '1', 0); ?>

                                        <i></i>
                                        Force Update
                                    </label>
                                </div>
                                <?php echo $errors->first('android_update', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
