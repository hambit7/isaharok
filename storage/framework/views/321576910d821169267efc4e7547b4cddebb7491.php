<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <?php echo Form::label('name', Lang::get('app.Name'), ['class' => 'col-md-4 control-label']); ?>

    <div class="col-md-6">
        <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

        <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

    </div>
</div>
<div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
    <?php echo Form::label('imagename', Lang::get('app.Image Name'), ['class' => 'col-md-4 control-label']); ?>

    <div class="col-md-6">
        <?php echo Form::file('image', null, ['class' => 'form-control', 'name' => 'imagename']); ?>

        <?php echo $errors->first('image', '<p class="help-block">:message</p>'); ?>

        <?php
        if (isset($peoplefor->imagename) && $peoplefor->imagename != "") {
            $baseurl = URL::to('/');
            echo '<img src="' . $baseurl . '/uploads/' . $peoplefor->imagename . '" style="width:200px;height:150px;">';
            echo '<input type="hidden" name="imagename" value="' . $peoplefor->imagename . '">';
        }
        //echo $peoplefor->imagename;
        //echo '<img scr="uploads/'.$peoplefor->imagename.'">';
        ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']); ?>

    </div>
</div>
