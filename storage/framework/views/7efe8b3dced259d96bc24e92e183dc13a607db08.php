<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Terms and Condition Settings'); ?></div>
                    <div class="panel-body">

                        <?php //echo "<pre>";print_r($errors);echo "</pre>"; ?>
                        <?php echo Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]); ?>


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('termsandconditionsheading') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('termsandconditionsheading', Lang::get('app.Heading'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('termsandconditionsheading', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('termsandconditionsheading', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('termsandconditionscontent') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('termsandconditionscontent', Lang::get('app.Content'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::textarea('termsandconditionscontent', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('termsandconditionscontent', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
