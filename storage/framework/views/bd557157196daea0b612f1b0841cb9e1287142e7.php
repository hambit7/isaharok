<?php $__env->startSection('content'); ?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Site settings'); ?></div>
                    <div class="panel-body">

                        <?php echo Form::model($adminData, [
                            'method' => 'post',
                            'url' => ['/admin/updateprofile'],
                            'class' => 'form-horizontal',
                        ]); ?>


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('email', Lang::get('app.Email'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('email', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('email', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('password') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('password', Lang::get('app.Password'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::password('password', ['class' => 'form-control', 'autocomplete' => 'new-password']); ?>

                                <div class="col-md-12"
                                     style="font-size:14px;"><?php echo app('translator')->get("app.Do not modify anything, if you don’t want to change the password"); ?></div>
                                <?php echo $errors->first('password', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
