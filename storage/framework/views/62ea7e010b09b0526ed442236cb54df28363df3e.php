<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Edit Help Page'); ?>
                        "<?php echo e($helppages->title); ?>"
                    </div>
                    <div class="panel-body">

                        <?php if ($errors->any()): ?>
                            <!--ul class="alert alert-danger">
                                <?php foreach ($errors->all() as $error): ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; ?>
                            </ul-->
                        <?php endif; ?>

                        <?php echo Form::model($helppages, [
                            'method' => 'PATCH',
                            'url' => ['/admin/helppages', $helppages->id],
                            'class' => 'form-horizontal',
                            'files' => true,
                            'onsubmit' => 'return loadDescription();'
                        ]); ?>


                        <?php echo $__env->make('admin.helppages.form', ['submitButtonText' => 'Update'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
