<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <head>
        <style type="text/css">
            .centerText {
                text-align: center;
            }
        </style>
    </head>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Premium'); ?></div>
                    <div class="panel-body">

                        <a href="<?php echo e(url($baseurl . '/admin/premium/create')); ?>"
                           class="btn btn-primary btn-md" title="Add New Premium"><span class="glyphicon glyphicon-plus"
                                                                                        aria-hidden="true"/></span> <?php echo app('translator')->get('app.Add New Premium'); ?>
                        </a>
                        <br/>
                        <br/>

                        <div class="col-sm-4 m-b-xs no-h-padding">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="<?php echo e(url($baseurl . '/admin/premium')); ?>">

                                <div class="input-group">
                                    <input type="text" name="search" value="<?php echo $search; ?>"
                                           class="input-sm form-control"
                                           placeholder="<?php echo app('translator')->get('app.Search'); ?>">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <span class="input-group-btn">
                          <button class="btn btn-sm btn-default"
                                  type="submit"><?php echo app('translator')->get('app.Go!'); ?></button>
                        </span>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive clears">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Premiumname'),)); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Price'),)); ?></th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Noofdays'),)); ?></th>
                                    <th><?php echo app('translator')->get('app.Platform'); ?></th>
                                    <th><?php echo app('translator')->get('app.Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $count = count($premium);
                                if ($count > 0) { ?>
                                    <?php foreach ($premium as $item): ?>
                                        <tr>
                                            <td><?php echo e($item->id); ?></td>
                                            <td><?php echo e($item->premiumname); ?></td>
                                            <td><?php echo $currency; ?><?php echo e($item->price); ?></td>
                                            <td><?php echo e($item->noofdays); ?></td>
                                            <td><?php echo e($item->platform); ?></td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/premium/' . $item->id)); ?>"
                                                   class="btn btn-success btn-xs" title="View Premium"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"/></a>
                                                <a href="<?php echo e(url($baseurl . '/admin/premium/' . $item->id . '/edit')); ?>"
                                                   class="btn btn-primary btn-xs" title="Edit Premium"><span
                                                            class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                                <?php /*<?php echo Form::open([*/ ?>
                                                <?php /*'method'=>'DELETE',*/ ?>
                                                <?php /*'url' => ['/admin/premium', $item->id],*/ ?>
                                                <?php /*'style' => 'display:inline'*/ ?>
                                                <?php /*]); ?>*/ ?>
                                                <?php /*<?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Premium" />', array(*/ ?>
                                                <?php /*'type' => 'submit',*/ ?>
                                                <?php /*'class' => 'btn btn-danger btn-xs',*/ ?>
                                                <?php /*'title' => 'Delete Premium',*/ ?>
                                                <?php /*'onclick'=>'return confirm("Confirm delete?")'*/ ?>
                                                <?php /*)); ?>*/ ?>
                                                <?php /*<?php echo Form::close(); ?>*/ ?>
                                            </td>
                                            <td id="deleting">

                                                <div>
                                                    <button data-user-premium-delete="<?php echo e($item->id); ?>"
                                                            class="btn btn-danger"
                                                            id="deletepremium">Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php } else
                                    echo '<tr><td colspan="4" class="centerText">' . Lang::get('app.Sorry...! No Premium is found') . '.</td></tr>';
                                ?>

                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $premium->render(); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
