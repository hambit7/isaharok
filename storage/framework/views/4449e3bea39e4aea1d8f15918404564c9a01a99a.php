<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
if (isset($search) && !empty($search)) {
    $search = $search;
} else {
    $search = "";
}
?>
    <head>
        <style type="text/css">
            .centerText {
                text-align: center;
            }
        </style>
    </head>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Help Pages'); ?></div>
                    <div class="panel-body">


                        <a href="<?php echo e(url($baseurl . '/admin/helppages/create')); ?>"
                           class="btn btn-primary btn-md" title="Add New Help Page"><span
                                    class="glyphicon glyphicon-plus"
                                    aria-hidden="true"></span> <?php echo app('translator')->get('app.Add Help Page'); ?>
                        </a>

                        <br/>
                        <br/>

                        <div class="table-responsive clears">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th> <?php echo \Kyslik\ColumnSortable\SortableLink::render(array(Lang::get('app.Title'))); ?> </th>
                                    <th><?php echo app('translator')->get('app.Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = count($helppages);
                                if ($count > 0) { ?>
                                    <?php foreach ($helppages as $item): ?>
                                        <tr>
                                            <td><?php echo e($item->id); ?></td>
                                            <td><?php echo e($item->title); ?></td>
                                            <td>
                                                <a href="<?php echo e(url($baseurl . '/admin/helppages/' . $item->id . '/edit')); ?>"
                                                   class="btn btn-primary btn-xs" title="Edit Help page"><span
                                                            class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                                <?php echo Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/admin/helppages', $item->id],
                                                    'style' => 'display:inline'
                                                ]); ?>

                                                <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Help Page" />', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete Help page',
                                                    'onclick' => 'return confirm("Confirm delete?")'
                                                )); ?>

                                                <?php echo Form::close(); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php } else
                                    echo '<tr><td colspan="4" class="centerText">' . Lang::get('app.Sorry...! No Help Pages found') . '.</td></tr>';
                                ?>

                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> <?php echo $helppages->render(); ?> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
