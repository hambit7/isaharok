<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');

?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.General Settings'); ?></div>
                    <div class="panel-body">

                        <?php echo Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]); ?>


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('sitename') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('sitename', Lang::get('app.Site Name'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('sitename', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('sitename', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('maximumage') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('maximumage', Lang::get('app.Maximum Age Limit'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('maximumage', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('maximumage', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('maximumdistance') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('maximumdistance', Lang::get('app.Maximum Distance Limit'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::text('maximumdistance', null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('maximumdistance', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('sitecurrency') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('sitecurrency', Lang::get('app.Default Currency (Eg : USD - $)'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php echo Form::select('sitecurrency', $currencies, null, ['class' => 'form-control']); ?>

                                <?php echo $errors->first('sitecurrency', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <?php /* ?>
						<div class="form-group {{ $errors->has('defaultimages') ? 'has-error' : ''}}">
							{!! Form::label('defaultimages', Lang::get('app.Default User Thumb'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('defaultimages', ['class' => 'filestyle','name' => 'defaultimages', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
								{!! $errors->first('defaultimages', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->defaultimages != "") ?>
								<div><img src="<?php echo url('/uploads/defaultuser/'.$sitesettings->defaultimages); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;"></div>
							</div>
						</div>

						<div class="form-group {{ $errors->has('sitelogo') ? 'has-error' : ''}}">
							{!! Form::label('sitelogo', Lang::get('app.Site Logo Image'), ['class' => 'col-md-4 control-label']) !!}
							<div class="col-md-6">
								{!! Form::file('sitelogo', ['class' => 'filestyle','name' => 'sitelogo', 'data-icon' => 'false', 'data-classButton' => 'btn btn-default', 'data-classInput' => 'form-control inline v-middle input-s']) !!}
								{!! $errors->first('sitelogo', '<p class="help-block">:message</p>') !!}
								<?php if($sitesettings->sitelogo != "") ?>
								<div><img src="<?php echo url('/uploads/defaultuser/'.$sitesettings->sitelogo); ?>"
									style="max-width:150px;max-height:150px;margin-top:10px;"></div>
							</div>
						</div>
						<?php */ ?>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
