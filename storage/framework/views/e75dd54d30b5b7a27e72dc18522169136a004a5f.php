<?php $__env->startSection('content'); ?>
<?php
$baseurl = URL::to('/');
?>
    <div class="main_content">
        <div class="sub_content">
            <div class="col-md-12 margin_top30">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo app('translator')->get('app.Payment Settings'); ?></div>
                    <div class="panel-body">

                        <?php if ($errors->any()): ?>
                            <!--ul class="alert alert-danger">
                                <?php foreach ($errors->all() as $error): ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; ?>
                            </ul-->
                        <?php endif; ?>

                        <?php echo Form::model($sitesettings, [
                            'method' => 'post',
                            'url' => [$baseurl . '/admin/updatesitesettings'],
                            'class' => 'form-horizontal',
                            'files' => true,
                        ]); ?>

                        <?php
                        ?>

                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                        <div class="form-group <?php echo e($errors->has('paymenttype') ? 'has-error' : ''); ?>">
                            <?php echo Form::label('googleads', Lang::get('app.Payment Type'), ['class' => 'col-md-4 control-label']); ?>

                            <div class="col-md-6">
                                <?php
                                $inappsetting = "";
                                $braintreesetting = "";
                                if (isset($sitesettings->paymenttype) && $sitesettings->paymenttype == 'inapp' || $sitesettings->paymenttype == '') {
                                    $inapp = true;
                                    $braintree = Null;
                                } else if (isset($sitesettings->paymenttype) && $sitesettings->paymenttype == 'braintree') {
                                    $inapp = Null;
                                    $braintree = true;

                                }
                                ?>
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('paymenttype', 'inapp', $inapp, ['class' => 'inapp']); ?>

                                        <i></i>
                                        <?php echo app('translator')->get('app.Inapp'); ?>
                                    </label>
                                </div>
                                <div class="radio i-checks">
                                    <label>
                                        <?php echo Form::radio('paymenttype', 'braintree', $braintree, ['class' => 'braintree']); ?>

                                        <i></i>
                                        <?php echo app('translator')->get('app.Braintree'); ?>
                                    </label>
                                </div>
                                <?php echo $errors->first('paymenttype', '<p class="help-block">:message</p>'); ?>

                            </div>
                        </div>
                        <!--div class="form-group <?php echo e($errors->has('adslot') ? 'has-error' : ''); ?>">
						    <?php echo Form::label('adslot', Lang::get('app.Adslot'), ['class' => 'col-md-4 control-label']); ?>

						    <div class="col-md-6">
						        <?php echo Form::text('adslot', null, ['class' => 'form-control']); ?>

						        <?php echo $errors->first('adslot', '<p class="help-block">:message</p>'); ?>

						    </div>
						</div-->
                        <?php
                        if ($sitesettings->paymenttype == 'braintree') {
                            $inappsetting = "style=display:none";
                        } else {
                            $braintreesetting = "style=display:none";
                        }
                        ?>

                        <div class="inappsetting" <?php echo $inappsetting; ?> >
                            <div class="form-group <?php echo e($errors->has('licensetoken') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('licensetoken', Lang::get('app.License Token'), ['class' => 'col-md-4 control-label']); ?>

                                <div class="col-md-6">
                                    <?php echo Form::text('licensetoken', null, ['class' => 'form-control']); ?>

                                    <?php echo $errors->first('licensetoken', '<p class="help-block">:message</p>'); ?>

                                </div>
                            </div>
                        </div>

                        <div class="braintreesetting" <?php echo $braintreesetting; ?>>
                            <div class="form-group <?php echo e($errors->has('brainTreeType') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('brainTreeType', Lang::get('app.Environment'), ['class' => 'col-md-4 control-label']); ?>

                                <div class="col-md-6">
                                    <div class="radio i-checks">
                                        <label>
                                            <?php echo Form::radio('brainTreeType', 'live', Null); ?>
                                            <i></i><?php echo app('translator')->get('app.Live'); ?>
                                        </label>
                                    </div>

                                    <div class="radio i-checks">
                                        <label>
                                            <?php echo Form::radio('brainTreeType', 'sandbox', Null); ?>
                                            <i></i><?php echo app('translator')->get('app.Sandbox'); ?>
                                        </label>
                                    </div>
                                    <?php echo $errors->first('brainTreeType', '<p class="help-block">:message</p>'); ?>

                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('brainTreeMerchantId') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('brainTreeMerchantId', Lang::get('app.Brain Tree Merchant Id'), ['class' => 'col-md-4 control-label']); ?>

                                <div class="col-md-6">
                                    <?php echo Form::text('brainTreeMerchantId', null, ['class' => 'form-control']); ?>

                                    <?php echo $errors->first('brainTreeMerchantId', '<p class="help-block">:message</p>'); ?>

                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('brainTreePublicKey') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('brainTreePublicKey', Lang::get('app.Brain Tree Public Key'), ['class' => 'col-md-4 control-label']); ?>

                                <div class="col-md-6">
                                    <?php echo Form::text('brainTreePublicKey', null, ['class' => 'form-control']); ?>

                                    <?php echo $errors->first('brainTreePublicKey', '<p class="help-block">:message</p>'); ?>

                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('brainTreePrivateKey') ? 'has-error' : ''); ?>">
                                <?php echo Form::label('brainTreePrivateKey', Lang::get('app.Brain Tree Private Key'), ['class' => 'col-md-4 control-label']); ?>

                                <div class="col-md-6">
                                    <?php echo Form::text('brainTreePrivateKey', null, ['class' => 'form-control']); ?>

                                    <?php echo $errors->first('brainTreePrivateKey', '<p class="help-block">:message</p>'); ?>

                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']); ?>

                            </div>
                        </div>

                        <?php echo Form::close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
