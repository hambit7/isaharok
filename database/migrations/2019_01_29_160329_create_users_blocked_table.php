<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBlockedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hz_blocked_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 60);
            $table->string('blocked_user_id', 60);
            $table->timestamps();
//            $table->foreign('user_id')->references('id')->on('hz_users')->onDelete('cascade');
//            $table->foreign('blocked_user_id')->references('id')->on('hz_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hz_blocked_users');
    }
}
