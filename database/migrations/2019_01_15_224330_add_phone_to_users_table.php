<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hz_users', function ($table) {
//            $table->bigInteger('phone')->unique()->default(null)->after('phone_is_verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hz_users', function ($table) {
            $table->dropColumn('phone');
        });
    }
}
