<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Premiumpayment;

class PremiumCanseledJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    protected $user;
    protected $premiumpayment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Premiumpayment $premiumpayment)
    {
        $this->user = $user;
        $this->premiumpayment = $premiumpayment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->premiumstatus = 0;
        $this->user->save();

        $this->premiumpayment->delete();

    }
}
