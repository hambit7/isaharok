<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\CloudMessagingService;
use App\Models\User;
use App\Models\Userdevices;

class creatingUserPushMessage extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $gender;
    public $name;
    public $userId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($gender, $name, $userId)
    {
        $this->gender = $gender;
        $this->name = $name;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CloudMessagingService $pushService)
    {
        $message = ($this->gender == 'female') ? "New girl $this->name has joined us! Chat her right now." : "New guy $this->name has joined us! Chat him right now.";

        $data = [
            'body' => $message,
            'user_id' => "$this->userId",
            'title' => "Sahar",
            'type' => "new_user"
        ];

        $params = User::where('gender', '<>', $this->gender)
            ->pluck('id')
            ->toArray();

        $userdevices = Userdevices::whereIn('user_id', $params)->get();

        if (!is_null($userdevices)) {
            $pushService->sendMessageToDevice($data, $message, $userdevices);
        }
    }

}
