<?php

namespace App\Listeners;

use App\Events\NewUserEvent;
use App\Services\CloudMessagingService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Userdevices;
use App\Classes\MyClass;

class NewUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserEvent $event
     * @return void
     */
    public function handle(NewUserEvent $event)
    {

        $pushService = new CloudMessagingService();
        $data = [
            'body' => "new member $event->name  has joined us!",
            'user_id' => "$event->userId",
            'title' => "Admin notification",
            'type' => "new_user"
        ];

        $params = User::where('gender', '<>', $event->gender)
            ->pluck('id')
            ->toArray();

        $userdevices = Userdevices::whereIn('user_id', $params)->get();

        $message = "New member $event->name  has joined us!";

        if (!is_null($userdevices)) {
            $pushService->sendMessageToDevice($data, $message, $userdevices);
        }
    }
}
