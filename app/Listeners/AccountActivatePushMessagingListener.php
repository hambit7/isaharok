<?php

namespace App\Listeners;

use App\Events\AccountActivatePushMessagingEvent;
use App\Models\Userdevices;
use App\Classes\MyClass;
use App\Services\CloudMessagingService;


class AccountActivatePushMessagingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  AccountActivatePushMessagingEvent $event
     * @return void
     */
    public function handle(AccountActivatePushMessagingEvent $event)
    {
        $params = [$event->model->id];
        $message = "Woohoo! Your account has been approved and activated. You can now use Sahar.";
        $userdevices = Userdevices::whereIn('user_id', $params)->get();

        $pushService = new CloudMessagingService();
        $data = [
            'body' => $message,
            'title' => "Sahar",
        ];

        if (!is_null($userdevices)) {
            $pushService->sendMessageToDevice($data, $message, $userdevices);
        }
    }
}
