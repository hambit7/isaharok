<?php

namespace App\Providers;

use App\Models\Sitesettings;
use App\Models\Landingpagesettings;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('layouts.master', function ($view) {
            $siteSettings = Sitesettings::findorfail(1);

            $landingpagesettings = Landingpagesettings::findOrFail(1);

            $action = app('request')->route()->getAction();

            $controller = class_basename($action['controller']);

            list($controller, $action) = explode('@', $controller);

            $view->with(compact('controller', 'action', 'siteSettings', 'landingpagesettings'));
        });

        view()->share('name', 'Sahar Admin');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
