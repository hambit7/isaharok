<?php
namespace App\Classes;

use Illuminate\Http\Request;
use Session;
use App\Models\Userdevices;
use App\Models\Sitesettings;
use App\Classes\PushNotification;
use App\Models\User;
use Uuid;

class MyClass
{

    public static function pushnot($deviceToken = null, $message = null, $badge = null, $notifytype = "notification")
    {

        $userdevicedatas = Userdevices::where('deviceToken', $deviceToken)->first();

        if ($userdevicedatas->type == 0) {
            $messageDetails = json_decode($message, true);
            try {
                if ($userdevicedatas->mode == 0) {
                    $certifcUrl = 'certificate/ApnsDev.pem';
                    $push = new PushNotification("sandbox", $certifcUrl);
                } else {
                    $certifcUrl = 'certificate/ios_production.pem';
                    $push = new PushNotification("production", $certifcUrl);
                }
                $push->setDeviceToken($deviceToken);
                $push->setPassPhrase("howzu");
                $push->setBadge($badge);
                $push->setNotifytype($notifytype);
                // latest update
                if ($messageDetails['type'] == "audio" || $messageDetails['type'] == "video" || $messageDetails['type'] == "bye") {
                    $CustomMessage = $messageDetails;
                } else if ($messageDetails['message'] == "") {
                    $CustomMessage = array();
                    $CustomMessage['message'] = $messageDetails['user_name'] . ': sent a photo';
                    $CustomMessage['userinfo'] = $messageDetails;

                } else if ($messageDetails['type'] == "chat_message") {
                    $CustomMessage = array();
                    $CustomMessage['message'] = $messageDetails['user_name'] . ': ' . $messageDetails['message'];
                    $CustomMessage['userinfo'] = $messageDetails;
                } else {
                    $CustomMessage = array();
                    $CustomMessage['message'] = $messageDetails['user_name'] . ': ' . $messageDetails['message'];
                    $CustomMessage['userinfo'] = $messageDetails;
                }
                $push->setMessageBody($CustomMessage);
                $push->sendNotification();
            } catch (Exception $e) {
                echo "problem with the device token: " . $deviceToken . " Exception caused: " . $e->getMessage();
            }
        } else {
            if ($notifytype == "audio" || $notifytype == "video") {
                MyClass::send_call_notification($deviceToken, $message, $notifytype);
            } else {
                MyClass::send_push_notification($deviceToken, $message, $notifytype);
            }
        }
    }

    public static function send_push_notification($registatoin_ids, $message, $notifytype)
    {
        $siteSettings = Sitesettings::first();

        $url = 'https://fcm.googleapis.com/fcm/send';
        $messageToBeSent = array();
        $messageToBeSent['data']['title'] = $notifytype;
        $messageToBeSent['data']['message'] = json_decode($message, true);
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $messageToBeSent,
            'time_to_live' => 30
        );

        $headers = array(
            'Authorization:  key=' . $siteSettings->androidFcmKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        if ($result === false) {

        }
        $errormsg = curl_error($ch);

        curl_close($ch);
    }


    public static function send_call_notification($registatoin_ids, $message, $notifytype)
    {
        $siteSettings = Sitesettings::first();
        $url = 'https://fcm.googleapis.com/fcm/send';

        $messageToBeSent = array();
        $messageToBeSent['data']['title'] = $notifytype;
        $messageToBeSent['data']['message'] = json_decode($message, true);
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $messageToBeSent,
            'time_to_live' => 30,
            'priority' => "high"
        );

        $headers = array(
            'Authorization:  key=' . $siteSettings->androidFcmKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {

        }
        $errormsg = curl_error($ch);
        curl_close($ch);
    }


    public static function custom_number_format($n, $precision = 0)
    {
        if ($n < 1000) {
            // Anything less than a million
            $n_format = number_format($n);
        } else if ($n < 1000000) {
            // Anything less than a million
            $n_format = number_format($n / 1000, $precision) . 'K';
        } else if ($n < 1000000000) {
            // Anything less than a billion
            $n_format = number_format($n / 1000000, $precision) . 'M';
        } else {
            // At least a billion
            $n_format = number_format($n / 1000000000, $precision) . 'B';
        }

        return $n_format;
    }

    public static function push_lang($lang)
    {
        Session::set('locale', $lang);
        return;
    }

    // New changes for v3
    public static function checkToken($token, $userId)
    {

        if (!is_null($token)) {

            $userModel = User::where([['id', $userId], ['userstatus', "1"], ['phone_is_verified', "1"], ['access_token', $token]])->first();

            if (!is_null($userModel)) {
                $currentTime = time();
                $expiryTime = $userModel->timestamp;
                if ($currentTime > $expiryTime) {
                    $userModel->token = Uuid::generate();
                    $setexpiryTime = strtotime("+7 day");
                    $userModel->timestamp = $setexpiryTime;
                    $userModel->save();
                    return 0;
                }
                return 1;
            }
        }
        return 0;
    }

    public static function randomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    public static function iospushnot()
    {
        //34b7cf6b 161a2c46 9407c740 de91fadb a54a6f18 f7b3ef68 f9ebaa90 d097d13d
        $deviceToken = '34b7cf6b161a2c469407c740de91fadba54a6f18f7b3ef68f9ebaa90d097d13d';
        $passphrase = 'howzu';
        $message = 'My first push notification!';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'certificate/ApnsDev.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195',
            $err,
            $errstr,
            60,
            STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT,
            $ctx
        );

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
            $resdata = 'Message not delivered' . PHP_EOL;
        else
            $resdata = 'Message successfully delivered' . PHP_EOL;

        // Close the connection to the server
        fclose($fp);

        return $resdata;
    }

}

?>
