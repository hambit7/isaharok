<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockedUser extends Model
{
    protected $table = 'hz_blocked_users';

    protected $fillable = [
        'user_id',
        'blocked_user_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
