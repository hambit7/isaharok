<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;


class Admin extends Model implements \Illuminate\Contracts\Auth\Authenticatable
{
    use AuthenticableTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hz_admins';

}




