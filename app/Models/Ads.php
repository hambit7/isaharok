<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_ads';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['adsclient', 'adslot'];

    public $timestamps = false;


}
