<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'hz_likes';

    protected $fillable = [
        'recepient_id',
        'author_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'author_id');
    }
}
