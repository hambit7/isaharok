<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gesture extends Model
{
    protected $table = 'hz_gestures';

    protected $fillable = [
        'url',
        'gender'
    ];

    public function image()
    {
        return $this->hasMany(Image::class, 'gestune_id');
    }
}
