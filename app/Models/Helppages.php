<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Helppages extends Model
{
    Use Sortable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hz_helppages';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $guarded = array();

    public $timestamps = false;

    public function getUpdatedAtColumn()
    {
        return null;
    }
}
