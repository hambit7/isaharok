<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_chats';

    /**
     * The database primary key value.
     *q
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['userid', 'chatuserid', 'lastreplied', 'lastmessage', 'lasttoread', 'lastupdate', 'fav_userid', 'fav_chatuser', 'status', 'isDeleted', 'cdate'];

    protected $guarded = array();

    public $timestamps = false;


    public function getUpdatedAtColumn()
    {
        return null;
    }

    public function sender()
    {
        return $this->hasOne('App\Models\User', 'id', 'userid');
    }

    public function receiver()
    {
        return $this->hasOne('App\Models\User', 'id', 'chatuserid');
    }

}
