<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Premiumpayment extends Model
{
    Use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_premiumpayment';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['premiumid', 'userid', 'status'];

    public $timestamps = false;


}
