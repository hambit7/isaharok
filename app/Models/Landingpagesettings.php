<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Landingpagesettings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hz_landingpagesettings';

    protected $guarded = array();

    public $timestamps = false;

    public function getUpdatedAtColumn()
    {
        return null;
    }
}
