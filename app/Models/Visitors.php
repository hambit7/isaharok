<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_visitors';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['userid', 'visitorid', 'unreadstatus', 'created_at'];

    public $timestamps = true;

    public function sender()
    {
        return $this->hasOne('App\Models\User', 'id', 'userid');
    }

    public function setUpdatedAt($value)
    {
        return new \DateTime();
    }

}
