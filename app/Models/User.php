<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class User extends Model
{
    Use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_users';

//    public $timestamps = [ 'created_at' ];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'password',
        'firstname',
        'username',
        'birthday',
        'age',
        'searchfilter',
        'location',
        'latitude',
        'longitude',
        'peoplefor',
        'userstatus',
        'phone_is_verified',
        'phone',
        'phoneHash',
        'is_activated',
        'premiumstatus',
        'facebookid',
        'profileimage',
        'visitors',
        'lastsearch',
        'agestatus',
        'distancestatus',
        'invisiblestatus',
        'adsstatus',
        'notifications',
        'gender',
        'token',
        'images',
        'timestamp',
        'unauthorized',
        'access_token',
        'premium_from',
        'premium_to'
    ];


    protected $hidden = [
        'password'
    ];
    protected $casts = [
        'is_activated' => 'boolean'
    ];

    protected $appends = [
        'phoneHash'
    ];



    public function requests()
    {
        return $this->hasMany('App\Models\Requests', 'friendid', 'id');
    }

    public function requestsInverse()
    {
        return $this->hasMany('App\Models\Requests', 'userid', 'id');
    }

    public function blockedUsers()
    {
        return $this->hasMany(BlockedUser::class, 'user_id');
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getPhoneHashAttribute()
    {
        return md5($this->phone . env('SALT_MDFIVE', 'tMi9czY67MnpEGfidpDr'));
    }

    public function author()
    {
        return $this->hasMany('App\Models\Like', 'author_id');
    }

//
    public function recepient()
    {
        return $this->hasMany('App\Models\Like', 'recepient_id');
    }

}
