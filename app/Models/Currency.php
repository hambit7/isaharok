<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_currencies';

    /**
     * The database primary key value.
     *q
     * @var string
     */
    protected $primaryKey = 'id';

    protected $guarded = array();

    public $timestamps = false;

    public function getUpdatedAtColumn()
    {
        return null;
    }
}
