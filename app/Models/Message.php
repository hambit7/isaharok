<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_messages';

    /**
     * The database primary key value.
     *q
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['userid', 'chatid', 'message', 'imagename', 'isDeleted', 'createddate'];

    protected $guarded = array();

    public $timestamps = false;

    public function getUpdatedAtColumn()
    {
        return null;
    }

    public function requests()
    {
        return $this->hasMany('App\Models\Requests', 'friendid', 'id');
    }

}
