<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userdevices extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_userdevices';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deviceToken',
        'user_id',
        'badge', 'type',
        'mode',
        'lang_type',
        'cdate',
        'deviceId'];

    public $timestamps = false;


}
