<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Sitesettings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hz_sitesettings';

    protected $guarded = array();

    public $timestamps = false;

    public $brainTreeType;
    public $brainTreeMerchantId;
    public $brainTreePublicKey;
    public $brainTreePrivateKey;

    public $fillable = [
        'push_messages_status'
    ];

    public function getUpdatedAtColumn()
    {
        return null;
    }

    protected $appends = [
        'max_age',
        'max_distance',
        'admin_enable_ads'
    ];

    protected $casts = [
        'googleads' => 'boolean',
        'push_messages_status' => 'boolean'
    ];

    /**
     * @return mixed
     */
    public function getMaxAgeAttribute()
    {
        return $this->maximumage;
    }

    /**
     * @return mixed
     */
    public function getMaxDistanceAttribute()
    {
        return $this->maximumdistance;
    }

    public function getAdminEnableAdsAttribute()
    {
        return $this->googleads;
    }


}
