<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Peoplefor extends Model
{
    Use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_peoplefors';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'imagename'];

    protected $appends = [
        'icon'
    ];

    public function getIconAttribute()
    {
        return url('/uploads/' . $this->imagename);
    }

    public $timestamps = false;

}
