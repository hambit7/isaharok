<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Reporting extends Model
{
    Use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_reporting';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['userid', 'reportuserid'];
    public $timestamps = false;

    public function getUsers()
    {
        return Reporting::where('reportuserid', $this->reportuserid)->count();
    }

    public function getReportedusers()
    {
        $user = User::where('id', $this->reportuserid)->first();
        if (!is_null($user)) {
            return User::where('id', $this->reportuserid)->first()->firstname;
        }
        return '';

    }

    public function sender()
    {
        return $this->hasOne('App\Models\User', 'id', 'reportuserid');
    }

}
