<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'hz_images';

    protected $fillable = [
        'url',
        'user_id',
        'gesture_id'
    ];

    public function gestune()
    {
        return $this->belongsTo(Gesture::class, 'gesture_id');
    }
}
