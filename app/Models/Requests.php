<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hz_requests';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid',
        'friendid',
        'status'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sender()
    {
        return $this->hasOne('App\Models\User', 'id', 'userid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function receiver()
    {
        return $this->hasOne('App\Models\User', 'id', 'friendid');
    }
}
