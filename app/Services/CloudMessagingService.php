<?php
/**
 * Created by PhpStorm.
 * User: hambi
 * Date: 12/10/2018
 * Time: 12:48 AM
 */

namespace App\Services;

use App\Models\Userdevices;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Database;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging\MessageToTopic;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\ApnsConfig;
use Kreait\Firebase\Messaging\AndroidConfig;


class CloudMessagingService
{

    const IOS = 0;
    const ANDROID = 1;

    /**
     * @return Database
     */
    protected function initDatabase()
    {
        $serviceAccount = ServiceAccount::fromArray(config('services.firebase'));

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(env('FIREBASE_API_DATEBASE_URI'))
            ->create();

        return $firebase->getDatabase();
    }

    /**
     * @return Firebase\Messaging
     */
    protected function ininCloud()
    {
        $serviceAccount = ServiceAccount::fromArray(config('services.firebase'));
        $firebase = (new Firebase\Factory())
            ->withServiceAccount($serviceAccount)
            ->create();

        return $messaging = $firebase->getMessaging();
    }

    /**
     * @param array $data
     */
    public function sendMessageToDevice($data, $messageBody = null, $devices = null)
    {
        $userName = $data['user_name'] ?? " ";
        $type = $data['type'] ?? " ";
        $notificationTitle = ($type == 'chat_message') ? "$userName send message" : "Sahar";

        $notification = [
            'title' => $notificationTitle,
            'body' => $messageBody
        ];

        $responce = [
            'status' => 'success',
            'message' => 'Message sent'
        ];

        $dataBadge = [];

        try {
            if (!is_null($devices)) {

                foreach ($devices as $device) {

                    if ($device->type == self::ANDROID) {
                        try {

                            $this->ininCloud()
                                ->send(CloudMessage::withTarget('token', $device->deviceToken)
                                    ->withData($data)
                                    ->withAndroidConfig($this->androidConfig())
                                );
                        } catch (\Exception $e) {
                            continue;
                        }
                    } elseif ($device->type == self::IOS) {


                        $dataBadge[] = $device->id;
                        try {
                            $this->ininCloud()
                                ->send(CloudMessage::withTarget('token', $device->deviceToken)
                                    ->withData($data)
                                    ->withNotification($notification)
                                    ->withApnsConfig($this->apnsConfig($device->badge + 1)));


                        } catch (\Exception $e) {
                            continue;
                        }
                    }

//                    try {
//                        ($message);

//                        $this->ininCloud()->send([
//                            'token' => $deviceId->deviceToken,
//                            'notification' => [
//                                'title' => 'Admin notification',
//                                'body' => $messageBody,
//                            ],
//                            'data' => $data,
//                            'android' => [
//                                'ttl' => '3600s',
//                                'priority' => 'normal',
//                                'notification' => [
//                                    'title' => 'Admin notification',
//                                    'body' => $messageBody,
//                                    'icon' => 'stock_ticker_update',
//                                    'color' => '#f45342',
//                                    'sound' => 'default'
//                                ],
//                            ],
//                            'apns' => [
//                                'headers' => [
//                                    'apns-priority' => '10',
//                                ],
//                                'payload' => [
//                                    'aps' => [
//                                        'alert' => [
//                                            'title' => 'Admin notification',
//                                            'body' => $messageBody,
//                                        ],
//                                        'badge' => 42,
//                                        'sound' => 'default'
//                                    ],
//                                ],
//                            ],
//                        ]);
//                    } catch (\Exception $e) {
//                        continue;
//                    }

                }
                if (!empty ($dataBadge)) {
                    Userdevices::whereIn('id', $dataBadge)
                        ->increment('badge');
                }

            } else {
                try {
                    $topic = 'global';
                    $message = CloudMessage::withTarget('topic', $topic)
                        ->withNotification($data)
                        ->withApnsConfig($this->apnsConfig(false))
                        ->withAndroidConfig($this->androidConfig())
                        ->withData($data);
                } catch (\Exception $e) {
                    dd($e);
                }

                $this->ininCloud()->send($message);
            }
        } catch (\Exception $e) {

            $responce = [
                'status' => 'error',
                'message' => $message = $e->getMessage()
            ];
        }
        return $responce;
    }

    /**
     * @param array $params
     * @return array
     */
    public function sendMessageToDB(array $params)
    {
        $responce = [
            'status' => 'success',
            'message' => 'Message sent'
        ];
        try {
            $this->initDatabase()->getReference(
                env('FIREBASE_API_PROJECT_ID') . '/messages/'
            )
                ->push(
                    [
                        'title' => array_get($params, 'title'),
                        'description' => array_get($params, 'body')
                    ]
                );
        } catch (\Exception $e) {

            $responce = [
                'status' => 'error',
                'message' => $message = $e->getMessage()
            ];
        }

        return $responce;
    }

    protected function apnsConfig($badge)
    {
        $apnsConfig = ApnsConfig::fromArray([
            'headers' => [
                'apns-priority' => '10',
            ],
            'payload' => [
                'aps' => [
                    'sound' => 'sahar_sound'
                ],
            ],
        ]);

        if ($badge != false) {
            $apnsConfig = ApnsConfig::fromArray([
                'headers' => [
                    'apns-priority' => '10',
                ],
                'payload' => [
                    'aps' => [
                        'sound' => 'sahar_sound.caf',
                        'badge' => $badge
                    ],
                ],
            ]);
        }
        return $apnsConfig;
    }

    protected function androidConfig()
    {
        return AndroidConfig::fromArray([
            'ttl' => '3600s',
            'priority' => 'normal',
//            'data' => [
//                'sound' => 'default'
//            ],
        ]);
    }
}
