<?php
/**
 * Created by PhpStorm.
 * User: hambi
 * Date: 2/26/2019
 * Time: 8:08 PM
 */

namespace App\Services;

class ImageHandlerService
{
    public $images;
    public $request;

    public function __construct(array $images, $request)
    {
        $this->images = $images;
        $this->request = $request;

    }
//[
//"https:\/\/sahar.club\/uploads\/user\/y44VbPSU1548944314.jpeg",
//"https:\/\/sahar.club\/uploads\/user\/QYrXaLZP1548944314.jpeg",
//"https:\/\/sahar.club\/uploads\/user\/jlZkOeGi1548944315.jpeg"
//]
    public function downloadImage()
    {

        $imageNames = "[";
        foreach ($this->images as $image) {
            $imageName = $this->request->file($image)->getClientOriginalName();
            $this->request->file($image)->move(
                base_path() . '/uploads/user/', $imageName
            );
//            array_push($imageNames,  base_path() . '/public/uploads/' .$imageName);
            $imageNames .= '"' . \URL::to('/') . '/uploads/user/' . $imageName . '",';
        }
        $imageNames .= "]";
        $imageNames = str_replace(",]", "]", $imageNames);
        return $imageNames;
    }


}
