<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Trait ApiTrait
 * @package App\Traits
 */
trait ApiTrait
{
    /**
     * @var int
     */
    private $statusCode = Response::HTTP_OK;

    /**
     * @return mixed
     */
    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return $this
     */
    protected function setStatusCode( $statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Abstract method for all responses.
     * Just takes response code and returns data.
     *
     * @param array $data
     * @param array $errors
     * @param array $headers
     * @return JsonResponse
     */
    private function respondAbstract(array $data = [], array $errors = [], array $headers = [])
    {
        $data = [
            'error' => !empty($errors),
            'errors' => $errors,
            'data' => $data
        ];

        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /*
	|--------------------------------------------------------------------------
	| SUCCESS RESPONSES
	|--------------------------------------------------------------------------
	*/

    /**
     * Respond 200 response code.
     * Usage: GET requests.
     *
     * @param array $data
     * @return JsonResponse
     */
//    protected function respondOK(array $data = [])
//    {
//        return $this->setStatusCode(Response::HTTP_OK)->respondAbstract($data);
//    }

    /**
     * Returns 201 response code.
     * Usage: POST requests, when entity has been created and returns data.
     *
     * @param array $data
     * @return mixed
     */
    protected function respondCreated(array $data = [])
    {
        return $this->setStatusCode(Response::HTTP_CREATED)->respondAbstract($data);
    }

    /**
     * Returns 200 response code.
     * Usage: PUT. Just an abstraction for PUT requests
     *
     * @param array $data
     * @return mixed
     */
    protected function respondUpdated(array $data = [])
    {
        return $this->setStatusCode(Response::HTTP_OK)->respondAbstract($data);
    }

    /**
     * Returns 204 response code.
     * Usage: DELETE requests, when entity has been deleted.
     * Response body should be empty(!)
     *
     * @param array $data
     * @return mixed
     */
    protected function respondDeleted()
    {
        return $this->setStatusCode(Response::HTTP_NO_CONTENT)->respondAbstract();
    }

    /*
	|--------------------------------------------------------------------------
	| ERROR RESPONSES
	|--------------------------------------------------------------------------
	*/

    /**
     * Returns 404 response code
     * Usage: GET, PUT, DELETE. Resource was not found or deleted
     *
     * @param array $errors
     * @return JsonResponse
     */
    protected function respondNotFound(array $errors = [])
    {
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)->respondAbstract([], $errors);
    }


    /**
     * Returns 403 response code.
     * Usage: GET, PUT, DELETE. Resource or entity exists but user has no access to it
     *
     * Note: Use notFound when resource does not exists or deleted
     *
     * @param array $errors
     * @return JsonResponse
     */
    protected function respondForbidden(array $errors = [])
    {
        return $this->setStatusCode(Response::HTTP_FORBIDDEN)->respondAbstract([], $errors);
    }

    /**
     * Returns 422 response code
     * Usage: POST, PUT. Request failed validation. Can be used as a helper with manual request validation
     *
     * @param array $errors
     * @return JsonResponse
     */
//    protected function respondErrors(array $errors = [], int $code = Response::HTTP_UNPROCESSABLE_ENTITY)
//    {
//        return $this->setStatusCode($code)->respondAbstract([], $errors);
//    }

    /**
     * Returns 422 response code
     * Usage POST, PUT, DELETE. Helper method for HttpException which grabs message from exception
     *
     * @param HttpException $e
     * @param int $code
     * @return mixed
     */
//    protected function respondHttpException(HttpException $e, int $code = Response::HTTP_UNPROCESSABLE_ENTITY)
//    {
//        return $this->setStatusCode($code)->respondAbstract([], $e->getMessage());
//    }

    /*
	|--------------------------------------------------------------------------
	| OTHER RESPONSES
	|--------------------------------------------------------------------------
	*/

    /**
     * @param string $pathToFile
     * @param null $name
     * @param array $headers
     * @param string $disposition
     * @return BinaryFileResponse
     */
    protected function respondDownload(string $pathToFile, $name = null, array $headers = [], $disposition = 'attachment')
    {
        return response()->download($pathToFile, $name, $headers, $disposition);
    }
}
