<?php

namespace App\Traits;

trait ImageTrait
{

    public function makeImage($requestImage)
    {

        $name = 'image' . '_' . time() . '_' . $requestImage->getClientOriginalName();
        $originalPath = base_path() . '/uploads/user/';

        if (\Intervention\Image\Facades\Image::make($requestImage)->save($originalPath . $name)) {
            return '/uploads/user/' . $name;
        }

        return '/uploads/user/notfound.png';
    }
}
