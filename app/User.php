<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The database primary key value.
     *
     * @var string
     */
//    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'firstname', 'username', 'birthday', 'age', 'location', 'latitude', 'longitude', 'peoplefor', 'userstatus', 'premiumstatus', 'facebookid', 'profileimage', 'visitors', 'lastsearch', 'agestatus', 'distancestatus', 'invisiblestatus', 'adsstatus', 'notifications', 'gender',
        'premium_from', 'premium_to'];


}
