<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Sitesettings;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::guard('admin')->check()) {
            return $next($request);
        }
        $siteSettings = Sitesettings::findorfail(1);
//        return view('login', ['siteSettings' => $siteSettings]);
        return redirect()->guest('admin');
    }
}
