<?php

namespace App\Http\Middleware;

use Closure;
use App\Classes\MyClass;
use App\Traits\DeveloperApiTrait;

class AuthUserMiddleware
{
    use DeveloperApiTrait;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = $request->header('authorization');

        $userId = !is_null($request->get('user_id')) ? $request->get('user_id') : $request->get('sender_id');

        $checkUser = Myclass::checkToken($token, $userId);

        if ($checkUser == 1) {
            return $next($request);
        }
        return $this->respondError([], 401, "Unauthorized access!");


    }
}
