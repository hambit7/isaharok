<?php

namespace App\Http\Controllers;

use App\Traits\ImageTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use App\Traits\DeveloperApiTrait;

class Controller extends BaseController
{
    use ImageTrait, DeveloperApiTrait, AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
}
