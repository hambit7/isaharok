<?php

namespace App\Http\Controllers\admin;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Models\User;
use Translit;

class WriterController extends Controller
{
    public $alphabet = array(
        // upper case
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'H',
        'ЗГ' => 'Zgh', 'Зг' => 'Zgh', 'Ґ' => 'G', 'Д' => 'D',
        'Е' => 'E', 'Є' => 'IE', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'Y', 'І' => 'I', 'Ї' => 'I', 'Й' => 'I',
        'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S',
        'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'Kh',
        'Ц' => 'Ts', 'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Shch',
        'Ь' => '', 'Ю' => 'Iu', 'Я' => 'Ia', '’' => '',
        // lower case
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'h',
        'зг' => 'zgh', 'ґ' => 'g', 'д' => 'd', 'е' => 'e',
        'є' => 'ie', 'ж' => 'zh', 'з' => 'z', 'и' => 'y',
        'і' => 'i', 'ї' => 'i', 'й' => 'i', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
        'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
        'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'ts',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ь' => '',
        'ю' => 'iu', 'я' => 'ia', '\'' => '',
    );

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function createUsersList()
    {
        if (!is_null($this->usersList())) {

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'Firstname');
            $sheet->setCellValue('B1', 'Phone number');
            $sheet->setCellValue('C1', 'Gender');
            $sheet->setCellValue('D1', 'location');
            $sheet->setCellValue('E1', 'age');
            $sheet->setCellValue('F1', 'Premium');
            $sheet->setCellValue('G1', 'Registration date');
            $sheet->setCellValue('H1', 'Recent activity');
            foreach ($this->usersList() as $key => $user) {
                $key = $key + 2;
                $sheet->getStyle('B' . $key)
                    ->getNumberFormat()
                    ->setFormatCode(
                        '00-00000-0000'
                    );
//                $correctName = mb_substr($user->firstname, 0, 1);
                $sheet->setCellValue("A$key", $this->convert($user->firstname));
                $sheet->setCellValue("B$key", $user->phone);
                $sheet->setCellValue("C$key", $user->gender);
//                $correctValue = mb_substr($user->location, 0, 1);
                $sheet->setCellValue("D$key", $this->convert($user->location));
                $sheet->setCellValue("E$key", $user->age);
                $sheet->setCellValue("F$key", $user->premiumstatus == '1' ? '+' : '-');
                $sheet->setCellValue("G$key", $user->created_at);
                $sheet->setCellValue("H$key", date('Y-m-d h:m', $user->onlinetimestamp));
            }

            header('Content-Encoding: UTF-8');
            header('Content-type: text/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename=Users_Export.csv');
            header('Pragma: no-cache');
            header('Expires: 0');

            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Csv($spreadsheet);

            $writer->save("php://output");

        }
    }

    protected function usersList()
    {
        return User::get(
            [
                'firstname',
                'phone',
                'gender',
                'location',
                'age',
                'premiumstatus',
                'created_at',
                'onlinetimestamp',
            ]
        );


    }


    public function convert($text)
    {
        return str_replace(
            array_keys($this->alphabet),
            array_values($this->alphabet),
            preg_replace(
            // use alternative variant at the beginning of a word
                array(
                    '/(?<=^|\s)Є/', '/(?<=^|\s)Ї/', '/(?<=^|\s)Й/',
                    '/(?<=^|\s)Ю/', '/(?<=^|\s)Я/', '/(?<=^|\s)є/',
                    '/(?<=^|\s)ї/', '/(?<=^|\s)й/', '/(?<=^|\s)ю/',
                    '/(?<=^|\s)я/',
                ),
                array(
                    'Ye', 'Yi', 'Y', 'Yu', 'Ya', 'ye', 'yi', 'y', 'yu', 'ya',
                ),
                $text)
        );
    }
}
