<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Sitesettings;
use App\Models\Admin;
use App\Models\Ads;
use App\Models\Currency;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class SitesettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the form for updating the SMTP configuration data.
     *
     * @return \Illuminate\View\View
     */
    public function smtpsettings()
    {
        $sitesettings = Sitesettings::findOrFail(1);

        return view('admin.sitesettings.smtpsettings', compact('sitesettings'));
    }

    /**
     * Show the form for updating the specified site data.
     *
     * @return \Illuminate\View\View
     */
    public function sitesettings()
    {
        $sitesettings = Sitesettings::findOrFail(1);

        $currencyDetails = Currency::get();
        $currencies = array();
        foreach ($currencyDetails as $currency) {
            $currencies[$currency->currency] = $currency->currency;
        }

        return view('admin.sitesettings.sitesettings', compact('sitesettings', 'currencies'));
    }

    /**
     * Show the form for updating the specified site data.
     *
     * @return \Illuminate\View\View
     */
    public function termsandcondition()
    {
        $sitesettings = Sitesettings::findOrFail(1);

        return view('admin.sitesettings.termsandcondition', compact('sitesettings'));
    }

    public function paymentsettings()
    {
        $sitesettings = Sitesettings::findOrFail(1);
        if (!empty($sitesettings['braintree_settings'])) {
            $brainTreeData = json_decode($sitesettings['braintree_settings'], true);
            $sitesettings['brainTreeType'] = $brainTreeData['brainTreeType'];
            $sitesettings['brainTreeMerchantId'] = $brainTreeData['brainTreeMerchantId'];
            $sitesettings['brainTreePublicKey'] = $brainTreeData['brainTreePublicKey'];
            $sitesettings['brainTreePrivateKey'] = $brainTreeData['brainTreePrivateKey'];
        }

        return view('admin.sitesettings.payment', compact('sitesettings'));
    }

    /**
     * Show the form for updating the Braintree configuration data.
     *
     * @return \Illuminate\View\View
     */


    public function braintreesettings()
    {
        $sitesettings = Sitesettings::findOrFail(1);

        if (!empty($sitesettings['braintree_settings'])) {
            $brainTreeData = json_decode($sitesettings['braintree_settings'], true);
            $sitesettings['brainTreeType'] = $brainTreeData['brainTreeType'];
            $sitesettings['brainTreeMerchantId'] = $brainTreeData['brainTreeMerchantId'];
            $sitesettings['brainTreePublicKey'] = $brainTreeData['brainTreePublicKey'];
            $sitesettings['brainTreePrivateKey'] = $brainTreeData['brainTreePrivateKey'];
        }

        return view('admin.sitesettings.braintreesettings', compact('sitesettings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateSitesettings(Request $request)
    {
//        echo "<pre>";
//        print_r($request->all());
        $requestData = $request->all();
        if (isset($request->brainTreeMerchantId) && $request->paymenttype == 'braintree') {
            $redirectUrl = 'admin/payment';
            $flashMessage = "Brain Tree Settings Updated";
            $validation = array(
                'brainTreeMerchantId' => 'required',
                'brainTreePublicKey' => 'required',
                'brainTreePrivateKey' => 'required',

            );
        } elseif (isset($request->termsandconditionsheading)) {
            $redirectUrl = 'admin/termsandcondition';
            $flashMessage = "Terms and Conditions Updated";
            $validation = array(
                'termsandconditionsheading' => 'required',
                'termsandconditionscontent' => 'required'
            );
        } elseif (isset($request->licensetoken) && $request->paymenttype == 'inapp') {

            $redirectUrl = 'admin/payment';
            $flashMessage = "Inapp Settings Updated";
            $validation = array(
                'licensetoken' => 'required');
        } elseif (isset($request->smtpport)) {
            $redirectUrl = 'admin/smtpsettings';
            $flashMessage = "SMTP Settings Updated";
            $validation = array(
                'smtphost' => 'required',
                'smtpport' => 'required|Numeric',
                'smtpusername' => 'required|email',
                'smtppassword' => 'required'
            );
        } else {
            $redirectUrl = 'admin/sitesettings';
            $flashMessage = "General Settings Updated";
            $validation = array(
                'sitename' => 'required',
                'maximumage' => 'required|Numeric|max:99',
                'maximumdistance' => 'required|Numeric|max:99999',
                'sitecurrency' => 'required'
            );
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect($redirectUrl)->withInput()->withErrors($validator);
        }

        $sitesettings = Sitesettings::findOrFail(1);
        if (isset($request->licensetoken) && $request->paymenttype == 'inapp') {

            $sitesettings->licensetoken = $request->licensetoken;
            $sitesettings->paymenttype = $request->paymenttype;

            $sitesettings->save();
        } elseif (isset($request->brainTreeType)) {

            $brainTreeData = array();
            $brainTreeData['brainTreeType'] = $request->brainTreeType;
            unset($request->brainTreeType);

            $brainTreeData['brainTreeMerchantId'] = $request->brainTreeMerchantId;
            unset($request->brainTreeMerchantId);
            $brainTreeData['brainTreePublicKey'] = $request->brainTreePublicKey;
            unset($request->brainTreePublicKey);
            $brainTreeData['brainTreePrivateKey'] = $request->brainTreePrivateKey;
            unset($request->brainTreePrivateKey);
            $sitesettings->braintree_settings = json_encode($brainTreeData);
            $sitesettings->save();

            $sitesettings->paymenttype = $request->paymenttype;

            $sitesettings->save();
        } else {
            $sitesettings->update($requestData);
        }


        if (isset($request->defaultimages) && !empty($request->defaultimages)) {
            $destinationPath = 'uploads/defaultuser'; // upload path
            $extension = Input::file('defaultimages')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '.' . $extension; // renameing image
            Input::file('defaultimages')->move($destinationPath, $fileName); // uploading file to given path

            $sitesettings->defaultimages = $fileName;
            $sitesettings->save();
        }

        if (isset($request->sitelogo) && !empty($request->sitelogo)) {
            $destinationPath = 'uploads/defaultuser'; // upload path
            $extension = Input::file('sitelogo')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '.' . $extension; // renameing image
            Input::file('sitelogo')->move($destinationPath, $fileName); // uploading file to given path

            $sitesettings->sitelogo = $fileName;
            $sitesettings->save();
        }

        Session::flash('flash_message', $flashMessage);

        return redirect()->intended($redirectUrl);//('admin/sitesettings');
    }

    /**
     * Show the form for updating the ads.
     *
     * @return \Illuminate\View\View
     */
    public function adssettings()
    {
        $adssettings = Ads::paginate(1);
        if (count($adssettings) > 0) {
            $adssettings = Ads::findOrFail(1);
        }
        $sitesettings = Sitesettings::findOrFail(1);

        return view('admin.sitesettings.adssettings', compact('adssettings', 'sitesettings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateAdssettings(Request $request)
    {

        $this->validate($request, [
            'adsclient' => 'required|String',
            //'adslot' => 'required|String',
        ]);

        $requestData = $request->all();

        $adssettings = Ads::paginate(1);
        if (count($adssettings) > 0) {
            $adssettings = Ads::findOrFail(1);
            $adssettings->update($requestData);
        } else {
            Ads::create($requestData);
        }

        $sitesettings = Sitesettings::findOrFail(1);
        $sitesettings->googleads = $requestData['googleads'];
        $sitesettings->save();


        $redirectUrl = 'admin/adssettings';

        Session::flash('flash_message', 'Adsense Settings updated!');

        return redirect()->intended($redirectUrl);//('admin/sitesettings');
    }

    /**
     * Show the form for updating the Admin profile data.
     *
     * @return \Illuminate\View\View
     */
    public function adminprofile()
    {
        $id = Auth::guard('admin')->id();

        $adminData = Admin::find($id);

        return view('admin.sitesettings.adminprofile', compact('adminData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateprofile(Request $request)
    {
        $requestData = $request->all();
        $redirectUrl = 'admin/profile';

        $validation = array(
            'email' => 'required|email',
            'password' => 'min:6',
        );
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect($redirectUrl)->withInput()->withErrors($validator);
        }

        $id = Auth::guard('admin')->id();

        $adminData = Admin::findOrFail($id);
        $adminData->email = $request->email;
        if (!empty($request->password) && $adminData->password != $request->password) {
            $adminData->password = Hash::make($request->password);
        }
        $adminData->save();
        Session::flash('flash_message', 'Profile updated!');
        return redirect()->intended($redirectUrl);//('admin/sitesettings');
    }

    /**
     * Show the form for updating the App of the day data.
     *
     * @return \Illuminate\View\View
     */
    public function appoftheday()
    {
        $sitesettings = Sitesettings::findOrFail(1);
        return view('admin.sitesettings.appoftheday', compact('sitesettings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateAppoftheday(Request $request)
    {
        $requestData = $request->all();
        if (isset($requestData['appimage1']) && $requestData['appimage1'] != "") {
            $this->validate($request, [
                'apptitle' => 'required|String',
                'appdesc' => 'required|String',
                'appurl' => 'required|String',
                'appiosurl' => 'required|String',
            ]);
        } else {
            $this->validate($request, [
                'apptitle' => 'required|String',
                'appdesc' => 'required|String',
                'appurl' => 'required|String',
                'appiosurl' => 'required|String',
                'app_image1' => 'required|Image',
            ]);
        }


        $sitesettings = Sitesettings::findOrFail(1);


        if (isset($requestData['app_image1']) && Input::file('app_image1')->isValid()) {
            $destinationPath = 'uploads/appimage'; // upload path
            $extension = Input::file('app_image1')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '_' . rand(1, 99999) . '.' . $extension; // renameing image
            Input::file('app_image1')->move($destinationPath, $fileName); // uploading file to given path
            $requestData['appimage1'] = $fileName;
        }
        if (isset($requestData['app_image2']) && Input::file('app_image2')->isValid()) {
            $destinationPath = 'uploads/appimage'; // upload path
            $extension = Input::file('app_image2')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '_' . rand(1, 99999) . '.' . $extension; // renameing image
            Input::file('app_image2')->move($destinationPath, $fileName); // uploading file to given path
            $requestData['appimage2'] = $fileName;
        }
        if (isset($requestData['app_image3']) && Input::file('app_image3')->isValid()) {
            $destinationPath = 'uploads/appimage'; // upload path
            $extension = Input::file('app_image3')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '_' . rand(1, 99999) . '.' . $extension; // renameing image
            Input::file('app_image3')->move($destinationPath, $fileName); // uploading file to given path
            $requestData['appimage3'] = $fileName;
        }
        if (isset($requestData['app_image4']) && Input::file('app_image4')->isValid()) {
            $destinationPath = 'uploads/appimage'; // upload path
            $extension = Input::file('app_image4')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '_' . rand(1, 99999) . '.' . $extension; // renameing image
            Input::file('app_image4')->move($destinationPath, $fileName); // uploading file to given path
            $requestData['appimage4'] = $fileName;
        }
        if (isset($requestData['app_image5']) && Input::file('app_image5')->isValid()) {
            $destinationPath = 'uploads/appimage'; // upload path
            $extension = Input::file('app_image5')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '_' . rand(1, 99999) . '.' . $extension; // renameing image
            Input::file('app_image5')->move($destinationPath, $fileName); // uploading file to given path
            $requestData['appimage5'] = $fileName;
        }
        unset($requestData['app_image1']);
        unset($requestData['app_image2']);
        unset($requestData['app_image3']);
        unset($requestData['app_image4']);
        unset($requestData['app_image5']);

        $sitesettings->update($requestData);
        Session::flash('flash_message', 'App of the day added!');
        return redirect('admin/appoftheday');

        $redirectUrl = 'admin/appoftheday';

        Session::flash('flash_message', 'App of the day settings updated!');

        return redirect()->intended($redirectUrl);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pushsettings()
    {
        $sitesettings = Sitesettings::findOrFail(1);

        return view('admin.sitesettings.pushsettings', compact('sitesettings'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePushSettings(Request $request)
    {
        $params = $request->get('push_messages_status');

        $sitesettings = Sitesettings::findOrFail(1);

        $responce = $sitesettings->update([
            $params
        ]);
        if ($responce) {
            $message = ($params == "true") ? "PushNotification is enabled" : "PushNotification is disabled";

            Session::flash('flash_message', $message);
        }
        return redirect()->back()->withInput();
    }
}
