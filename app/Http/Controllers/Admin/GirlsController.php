<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\creatingUserPushMessage;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Events\AccountActivatePushMessagingEvent;
use App\Events\NewUserEvent;
use App\Services\ImageHandlerService;
use Hash;
use Uuid;
use Illuminate\Support\Facades\Session;
use Validator;


class GirlsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $model = new User();
        $search = $request->get('search');

        if (!is_null($search)) {
            $girls = $model::where('gender', 'female')
                ->where('email', 'like', '%' . $search . '%')
                ->orWhere('firstname', 'like', '%' . $search . '%');
        } else {
            $girls = $model::where('gender', 'female');
        }
        $girls = $girls->paginate();

        return view('admin.girls.index', compact('girls'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $girls = new User();
        return view('admin.girls.create', compact('girls'));
    }


    public function store(Request $request)
    {

        $imagesKeys = [
            'firstimage',
            'secondimage',
            'thirdimage'
        ];

        $params = $request->only(
            [
                'email',
                'firstname',
                'phone',
                'age',
                'info',
                'bio',
                'password',
                'firstimage',
                'secondimage',
                'thirdimage'
            ]
        );
        $validator = Validator::make($params,
            [
                'email' => 'required|unique:hz_users',
                'firstname' => 'required',
                'phone' => 'required|unique:hz_users',
                'age' => 'required|numeric',
                'firstimage' => 'required|mimes:png,jpeg,jpg',
                'secondimage' => 'required|mimes:png,jpeg,jpg',
                'thirdimage' => 'required|mimes:png,jpeg,jpg',
                'password' => 'required|min:8',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $id = Uuid::generate();

        $staticParams = [
            'id' => $id,
            'username' => $request->get('firstname'),
            'password' => Hash::make($request->get('password')),
            'location' => "Майдан Незалежности, Киев, город Киев",
            'latitude' => 50.4504259,
            'longitude' => 30.5221657,
            'peoplefor' => 9,
            'phone_is_verified' => 1,
            'is_activated' => 1,
            'premiumstatus' => 0,
            'agestatus' => 0,
            'distancestatus' => 0,
            'invisiblestatus' => 0,
            'onlinestatus' => 0,
            'notifications' => '{"messageNotification":"1","likeNotification":"1"}',
            'searchfilter' => '{"gender":"men","people_for":"9","age":"18-80","distance":"100"}',
            'gender' => 'female',
            'token' => Uuid::generate(),
            'unauthorized' => 0,
            'premium_from' => 0,
            'userstatus' => 1,
            'profileimage' => $request->file('firstimage')->getClientOriginalName()
        ];

        $data = array_merge($params, $staticParams);

        $imageDownloadHandler = new ImageHandlerService($imagesKeys, $request);

        $images['images'] = $imageDownloadHandler->downloadImage();

        $data = array_merge($data, $images);

        User::create($data);
        $job = (new creatingUserPushMessage('female', $request->get('firstname'), $id))->delay(60 * 1);

        $this->dispatch($job);

        Session::flash('flash_message', 'Girl created!');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activateAccount(Request $request)
    {

        $model = User::find($request->get('id'));

        $responce = $model
            ->update(
                [
                    'userstatus' => $request->get('is_activated')
                ]
            );
        event(new AccountActivatePushMessagingEvent($model));

        $job = (new creatingUserPushMessage($model->gender, $model->username, $model->id))->delay(60 * 1);

        $this->dispatch($job);
        echo json_encode($responce);
    }
}
