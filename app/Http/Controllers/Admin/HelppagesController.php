<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Helppages;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class HelppagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $helppages = Helppages::sortable()->paginate(25);

        return view('admin.helppages.index', compact(['helppages']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        return view('admin.helppages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:30|min:3|unique:hz_helppages|regex:/^[\pL\s\-]+$/',
            'description' => 'required',
        ]);

        $requestData = $request->all();

        Helppages::create($requestData);

        Session::flash('flash_message', 'Help Page added!');

        return redirect('admin/helppages');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $helppages = Helppages::findOrFail($id);

        return view('admin.helppages.edit', compact(['helppages']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:30|min:3|regex:/^[\pL\s\-]+$/|unique:hz_helppages,title,' . $id,
            'description' => 'required',
        ]);
        $requestData = $request->all();

        $helppages = Helppages::findOrFail($id);

        $helppages->update($requestData);
        Session::flash('flash_message', ' Help Page Updated.!');

        return redirect('admin/helppages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            Helppages::destroy($id);

            Session::flash('flash_message', 'Help Page deleted!');

            return redirect('admin/helppages');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('flash_message', "Help Page can't be deleted");

            return redirect('admin/helppages');
        }

    }
}
