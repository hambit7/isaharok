<?php

namespace App\Http\Controllers\Admin;

use App\Classes\MyClass;
use App\Events\AccountActivatePushMessagingEvent;
use App\Http\Controllers\Controller;
use App\Jobs\creatingUserPushMessage;
use App\Models\Premium;
use App\Models\Premiumpayment;
use App\Models\Reporting;
use App\Models\Sitesettings;
use App\Models\User;
use App\Models\Userdevices;
use Config;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Services\CloudMessagingService;


class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function approved()
    {
        if (isset($_GET['search'])) {
            $search = $_GET['search'];
            $users = User::where('email', 'like', "%$search%")
                ->where('userstatus', 1)
                ->orWhere('email', 'like', "%$search%")
                ->orderBy('id', 'DESC')
                ->sortable();
            $users = $users->where('userstatus', 1)->paginate(10);
        } else {
            $search = "";
            $users = User::where('userstatus', '1')->orderBy('id', 'DESC')->sortable()->paginate(10);
        }
        return view('admin.users.approved', compact(['users', 'search']));
    }

    public function premiumuser()
    {
        if (isset($_GET['search'])) {
            $search = $_GET['search'];

            $users = User::where('firstname', 'like', "%$search%")
                ->where('premiumstatus', 1)
                //->where('userstatus',1)
                ->orWhere('email', 'like', "%$search%")
                ->orderBy('premium_from', 'DESC');
//                ->sortable();
            $users = $users->where('premiumstatus', 1)->paginate(10);
        } else {
            $search = "";
            $users = User::where('premiumstatus', 1)->orderBy('premium_from', 'DESC')
                //->where('userstatus','1')
//                ->sortable()
                ->paginate(10);
        }

        return view('admin.users.premium', compact(['users', 'search']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function unapproved()
    {
        if (isset($_GET['search'])) {
            $search = $_GET['search'];
            $users = User::where('firstname', 'like', "%$search%")
                ->where('userstatus', 0)
                ->orWhere('username', 'like', "%$search%")
                ->orderBy('id', 'DESC')
                ->sortable();
            $users = $users->where('userstatus', 0)->paginate(10);
        } else {
            $search = "";
            $users = User::where('userstatus', '0')->orderBy('id', 'DESC')->sortable()->paginate(10);
        }

        return view('admin.users.unapproved', compact(['users', 'search']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function reportedusers()
    {
        if (isset($_GET['search'])) {
            $search = $_GET['search'];

            $users = Reporting::groupBy('reportuserid')
                ->
            join('hz_users', function ($join) {
                if (isset($_GET['search'])) {
                    $search = $_GET['search'];
                } else {
                    $search = "";
                }

                    $join->on('hz_reporting.reportuserid', 'hz_users.id')->where('hz_users.username', 'LIKE', "%$search%");
            })
                ->orderBy('id', 'DESC')->paginate(10);
        } else {
            $search = "";
            $users = Reporting::groupBy('reportuserid')->orderBy('id', 'DESC')->paginate(10);
        }

        return view('admin.users.reportedusers', compact(['users', 'search']));
    }

    public function searchapproved()
    {

        $search = Input::get('search');

        if ($search) {

            $users = User::where('email', 'like', "%$search%")
                ->where('userstatus', 1)
                ->orWhere('username', 'like', "%$search%")
                ->orderBy('id', 'DESC');
            $users = $users->where('userstatus', 1)->paginate(10);
        } else {
            $search = "";
            $users = User::where('userstatus', 1)->orderBy('id', 'DESC')->paginate(10);
        }

        return view('admin.users.approved', compact(['users', 'search']));
    }

    public function searchpremium()
    {

        $search = Input::get('search');

        if ($search) {

            $users = User::where('email', 'like', "%$search%")
                ->where('premiumstatus', 1)
                ->where('userstatus', 1)
                ->orWhere('email', 'like', "%$search%")
                ->orderBy('premium_from', 'DESC');
            $users = $users->where('premiumstatus', 1)->where('userstatus', 1)->paginate(10);
        } else {
            $search = "";
            $users = User::where('premiumstatus', 1)->where('userstatus', 1)->orderBy('premium_from', 'DESC')->paginate(10);
        }

        return view('admin.users.premium', compact(['users', 'search']));
    }

    public function searchunapproved()
    {

        $search = Input::get('search');

        if ($search) {

            $users = User::where('email', 'like', "%$search%")
                ->where('userstatus', 0)
                ->orWhere('username', 'like', "%$search%")
                ->orderBy('id', 'DESC');
            $users = $users->where('userstatus', 0)->paginate(10);
        } else {
            $search = "";
            $users = User::where('userstatus', 0)->orderBy('id', 'DESC')->paginate(10);
        }

        return view('admin.users.unapproved', compact(['users', 'search']));
    }

    public function searchreportedusers()
    {

        $search = Input::get('search');
        $sender[] = ['username', 'LIKE', "'%$search%'"];

        if ($search) {
            $users = Reporting::groupBy('reportuserid')->
            join('hz_users', function ($join) {
                $search = Input::get('search');
                $join->on('hz_reporting.reportuserid', '=', 'hz_users.id')->where('hz_users.username', 'LIKE', "%$search%");
            })
                ->orderBy('id', 'DESC')->paginate(10);
        } else {
            $search = "";
            $users = Reporting::groupBy('reportuserid')->orderBy('id', 'DESC')->paginate(10);
        }

        return view('admin.users.reportedusers', compact(['users', 'search']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        User::create($requestData);

        Session::flash('flash_message', 'User added!');

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.show', compact('user'));
    }

    public function showpremiumuser($id)
    {
        $user = User::findOrFail($id);
        $user1 = Premiumpayment::where('userid', $id)
            ->first();
        $premiumid = $user1->premiumid;
        $premium = Premium::where('id', $premiumid)
            ->first();

        return view('admin.users.showpremiumuser', compact('user', 'user1', 'premium'));
    }

    public function showapproveduser($id)
    {

        $user = User::findOrFail($id);

        return view('admin.users.showapproveduser', compact('user'));
    }

    public function showunapproveduser($id)
    {

        $user = User::findOrFail($id);

        return view('admin.users.showunapproveduser', compact('user'));
    }

    /**
     * /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $user = User::findOrFail($id);
        $user->update($requestData);

        Session::flash('flash_message', 'User updated!');

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('flash_message', 'User deleted!');

        return redirect('admin/users');
    }

    /**
     * Function to enable and disable the user
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function changestatus($id, $status)
    {

        $user = User::find($id);

        if ($status == "1") {
            $user->unauthorized = 0;
        }
        $user->userstatus = $status;
        $user->save();
        if ($status == "0") {


            $data = [
                'body' => "Your account has been blocked.",
                'title' => "Admin notification",
                'type' => "user_disabled"

            ];
            $message = "Your account has been blocked.";
        } else {
            event(new AccountActivatePushMessagingEvent($user));

            $job = (new creatingUserPushMessage($user->gender, $user->username, $user->id))->delay(60 * 1);

            $this->dispatch($job);

            $data = [
                'body' => "Your account has been activated.",
                'title' => "Admin notification",
                'type' => "user_enabled"
            ];
            $message = "Your account has been activated.";
        }


        $userdevices = Userdevices::where('user_id', $id)->get();

        $pushService = new CloudMessagingService();
        if (!is_null($userdevices)) {
            $pushService->sendMessageToDevice($data, $message, $userdevices);
        }

        $users = Reporting::where('reportuserid', $id);
        $users->delete();
        if ($status == '0') {
            return redirect('admin/unapproved');
        } else {
            return redirect('admin/approved');
        }
    }

    public function ignorereporting($id)
    {
        $users = Reporting::where('reportuserid', $id);
        $users->delete();
        return redirect('admin/reportedusers');
    }

    /**
     * @param Request $request
     */
    public function destroyUser(Request $request)
    {
        $id = $request->only('id');

        $responce = [
            'status' => User::destroy($id)
        ];

        Session::flash('flash_message', 'User deleted!');

        echo json_encode($responce);
    }

    /**
     * @param Request $request
     */

}
