<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Landingpagesettings;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class LandingpagesettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the form for updating the Landing page configuration data.
     *
     * @return \Illuminate\View\View
     */
    public function generalSettings()
    {
        $landingpagesettings = Landingpagesettings::findOrFail(1);

        return view('admin.landingpagesettings.generalsettings', compact('landingpagesettings'));
    }

    /**
     * Show the form for updating the Landing page Mobile app configuration data.
     *
     * @return \Illuminate\View\View
     */
    public function mobileAppConfig()
    {
        $landingpagesettings = Landingpagesettings::findOrFail(1);

        return view('admin.landingpagesettings.mobileappconfig', compact('landingpagesettings'));
    }

    /**
     * Show the form for updating the Landing page Social link configuration data.
     *
     * @return \Illuminate\View\View
     */
    public function socialLinkConfig()
    {
        $landingpagesettings = Landingpagesettings::findOrFail(1);

        return view('admin.landingpagesettings.sociallinkconfig', compact('landingpagesettings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateLandingPage(Request $request)
    {

        $requestData = $request->all();
        $validation = array();

        if (isset($request->landingpagetitle)) {
            $redirectUrl = 'admin/landingpage/generalsettings';
            $flashMessage = "Landing Page Settings Updated";
            $validation = array(
                'landingpagetitle' => 'required|max:150',
                'landingpagesubtitle' => 'required|max:250',
                'copyrightinfo' => 'required|max:250',
            );
        } elseif (isset($request->iosapplink)) {
            $redirectUrl = 'admin/landingpage/mobileappconfig';
            $flashMessage = "Landing Page Mobile app links Updated";
        } elseif (isset($request->facebooklink)) {
            $redirectUrl = 'admin/landingpage/sociallinkconfig';
            $flashMessage = "Landing Page Social links Updated";
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect($redirectUrl)->withInput()->withErrors($validator);
        }

        $landingpagesettings = Landingpagesettings::findOrFail(1);
        // save data
        $requestData['iosapplink'] = $request->iosapplink;
        $requestData['androidapplink'] = $request->androidapplink;
        $landingpagesettings->update($requestData);

        if (isset($request->landingpagelogo) && !empty($request->landingpagelogo)) {
            $destinationPath = 'uploads/landingpage'; // upload path
            $extension = Input::file('landingpagelogo')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '-mainpage.' . $extension; // renameing image
            Input::file('landingpagelogo')->move($destinationPath, $fileName); // uploading file to given path

            $landingpagesettings->landingpagelogo = $fileName;
            $landingpagesettings->save();
        }

        if (isset($request->subpagelogo) && !empty($request->subpagelogo)) {
            $destinationPath = 'uploads/landingpage'; // upload path
            $extension = Input::file('subpagelogo')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '-subpage.' . $extension; // renameing image
            Input::file('subpagelogo')->move($destinationPath, $fileName); // uploading file to given path

            $landingpagesettings->subpagelogo = $fileName;
            $landingpagesettings->save();
        }

        Session::flash('flash_message', $flashMessage);

        return redirect()->intended($redirectUrl);
    }
}
