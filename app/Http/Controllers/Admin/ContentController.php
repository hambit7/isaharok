<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gesture;
use App\Models\Image;
use http\Url;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $images = Gesture::all();

        return view('admin.content.index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Gesture();

        $params = $request->only(
            'image',
            'gender');


        $validator = \Validator::make($params, [
            'image' => 'required|file:jpeg,jpg,png',
            'gender' => 'string',
        ]);

        if ($validator->fails()) {
            \Session::flash('flash_message', $validator->errors()->first());
            return redirect()->back();

        }

        $url = $this->makeImage($request->file('image'));

        $model->fill(
            [
                'url' => $url,
                'gender' => $request->get('gender')
            ]);
        $model->save();

        \Session::flash('flash_message', 'Photo added');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Image::where('user_id', $id)->first();
        if (!is_null($image)) {
            $imageGestune = $image->gestune()->first()->url;
            $src = \App::make('url')->to('/') . $image->url;
            $gsrc = \App::make('url')->to('/') . $imageGestune;
            return view('admin.content.show', compact('src', 'gsrc'));

        }

        $src = $gsrc = \App::make('url')->to('/') . '/uploads/notfound.png';

        return view('admin.content.show', compact('src', 'gsrc'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $model = Gesture::findOrFail($request->get('id'));
        if ($model->delete()) {
            \Session::flash('flash_message', 'Photo deleted');
        }
        return redirect()->back();
    }
}
