<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Peoplefor;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Validator;

class PeopleforController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['search'])) {
            $search = $_GET['search'];
            $peoplefor = Peoplefor::where('name', 'like', "%$search%")
                ->sortable()->paginate(25);
        } else {
            $peoplefor = Peoplefor::sortable()->paginate(25);
        }
        return view('admin.peoplefor.index', compact(['peoplefor', 'search']));
    }


    public function searchpeople()
    {

        $search = Input::get('search');

        if ($search) {

            $peoplefor = Peoplefor::where('name', 'like', "%$search%")
                ->orderBy('id', 'DESC')->paginate(25);

        } else {
            $search = "";
            $peoplefor = Peoplefor::orderBy('id', 'DESC')->paginate(25);

        }


        return view('admin.peoplefor.index', compact(['peoplefor', 'search']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.peoplefor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|min:3|unique:hz_peoplefors|regex:/^[\pL\s\-]+$/',
            'image' => 'required|Image',
        ]);
        $requestData = $request->all();
        // getting all of the post data
        $file = array('image' => Input::file('image'));
        // setting up rules
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return Redirect::to('admin/peoplefor/create')->withInput()->withErrors($validator);
        } else {
            // checking file is valid.
            if (Input::file('image')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = time() . '.' . $extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $requestData['imagename'] = $fileName;
                Peoplefor::create($requestData);
                Session::flash('flash_message', 'Peoplefor added!');
                return redirect('admin/peoplefor');
                // sending back with message
                //Session::flash('success', 'Upload successfully');
            } else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return redirect('admin/peoplefor/create');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $peoplefor = Peoplefor::findOrFail($id);

        return view('admin.peoplefor.show', compact('peoplefor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $peoplefor = Peoplefor::findOrFail($id);

        return view('admin.peoplefor.edit', compact('peoplefor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        //print_r($request);die;

        $requestData = $request->all();

        if (isset($requestData['imagename']) && $requestData['imagename'] != "") {
            $this->validate($request, [
                'name' => 'required|max:35|min:3|regex:/^[\pL\s\-]+$/|unique:hz_peoplefors,name,' . $id,
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|max:35|min:3|regex:/^[\pL\s\-]+$/',
                'image' => 'required|Image',
            ]);
        }
        $peoplefor = Peoplefor::findOrFail($id);
        if (isset($requestData['image']) && Input::file('image')->isValid()) {
            $destinationPath = 'uploads'; // upload path
            $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = time() . '.' . $extension; // renameing image
            Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
            $requestData['imagename'] = $fileName;
            // sending back with message
            //Session::flash('success', 'Upload successfully');
        } else {
            // sending back with error message.
        }

        $updateid = $id;

        $name = $request['name'];
        $peoplefordata = Peoplefor::where('name', $name)->first();
        $c = count($peoplefordata);
        if ($c == 0) {

            $peoplefor->update($requestData);

            Session::flash('flash_message', ' Peoplefor Updated.!');
        } else {
            $peopleforid = $peoplefordata->id;
            if ($updateid == $peopleforid) {

                $peoplefor->update($requestData);
                Session::flash('flash_message', 'Peoplefor Updated.!');

            } else {

                Session::flash('flash_message', 'Peoplefor name already exists.!');

            }
        }


        //$peoplefor->update($requestData);

        //Session::flash('flash_message', 'Peoplefor updated!');

        return redirect('admin/peoplefor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            Peoplefor::destroy($id);

            Session::flash('flash_message', 'Peoplefor deleted!');

            return redirect('admin/peoplefor');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('flash_message', "Peoplefor already in use, can't be deleted");

            return redirect('admin/peoplefor');
        }

    }
}
