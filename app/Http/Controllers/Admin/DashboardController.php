<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Premium;
use App\Models\Userdevices;
use App\Models\Sitesettings;
use App\Models\Premiumpayment;
use App\Models\Landingpagesettings;
use App\Classes\MyClass;
use App\Services\CloudMessagingService;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Events\NewUserEvent;
use App\Jobs\creatingUserPushMessage;
use Carbon\Carbon;
class DashboardController extends Controller
{
    public $sitesettings;
    public function __construct()
    {
        $this->middleware('admin');
        $this->sitesettings = Sitesettings::first();
    }

    public function index()
    {

//        $job = (new creatingUserPushMessage('female', 'QueueFROMAPI', '1b4ba8b0-3ab8-11e9-9c7c-874ac878f60f'))->delay(60*1);
//
//        $this->dispatch($job);
//        $this->dispatch(new creatingUserPushMessage('female', 'vasya', 'asdasd'))->delay(60);
//        event(new NewUserEvent('male', 'testUser', '092b7680-2563-11e9-ab19-23f049cb0d72'));

        $landingpagesettings = Landingpagesettings::findOrFail(1);
        $siteCurrency = explode('-', $this->sitesettings->sitecurrency);
        $totaluser = User::count('id');
        $activeusers = User::where('userstatus', '1')->count();
        $inactiveusers = User::where('userstatus', '0')->count();
        $premiumuser = User::where('premiumstatus', '1')->where('userstatus', '1')->count();

        $androidDevices = Userdevices::where('type', 1)->count();
        $iosDevices = Userdevices::where('type', 0)->count();

        $premiumTypes = Premium::get();
        $premiumUsageDetails = array();
        $totalRevenue = 0;
        foreach ($premiumTypes as $key => $premium) {
            $premiumUsageDetails[$key]['premiumName'] = $premium->premiumname;
            $premiumUsageDetails[$key]['premiumPlatform'] = $premium->platform;
            $premiumUsageDetails[$key]['premiumPrice'] = $premium->price;
            $premiumUsageDetails[$key]['premiumPeriod'] = $premium->noofdays;
            $premiumTotalCount = Premiumpayment::where('premiumid', $premium->id)->count();
            $totalPremiumRevenue = $premiumTotalCount * $premium->price;
            $premiumUsageDetails[$key]['premiumRevenue'] = $totalPremiumRevenue;
            $premiumTotalActiveCount = Premiumpayment::where('premiumid', $premium->id)->where('status', 'active')->count();
            if ($premiumuser > 0)
                $premiumUsageDetails[$key]['premiumPercentage'] = round((($premiumTotalActiveCount * 100) / $premiumuser), 2);
            else
                $premiumUsageDetails[$key]['premiumPercentage'] = 0;
            $totalRevenue += $totalPremiumRevenue;
        }
        return view('admin.dashboard', compact('activeusers', 'inactiveusers', 'totaluser', 'premiumuser', 'androidDevices', 'iosDevices',
            'premiumUsageDetails', 'totalRevenue', 'siteSettings', 'siteCurrency', 'landingpagesettings'));
    }

    public function androidConfig()
    {

        return view('admin.dashboard.androidconfig', compact($this->sitesettings));
    }

    public function iosConfig()
    {

        return view('admin.dashboard.iosconfig', compact($this->sitesettings));
    }

    public function mobileAppConfig(Request $request)
    {

        if (isset($request->androidFcmKey)) {
            $redirectUrl = 'admin/androidconfig';
            $flashMessage = "Android app settings updated";
            $validation = array(
                'androidFcmKey' => 'required',
                'android_version' => 'required',
            );
        } elseif ((isset($request->ios_update) || isset($request->ios_version)) && (!isset($request->iossandbox) && !isset($request->iosproduction))) {
            $redirectUrl = 'admin/iosconfig';
            $flashMessage = "iOS settings updated";
            $validation = array(
                'ios_update' => 'required',
                'ios_version' => 'required',
            );
        } elseif (isset($request->iossandbox) || isset($request->iosproduction)) {
            $redirectUrl = 'admin/iosconfig';
            $flashMessage = "iOS app settings updated1 ";
            $validation = array();
        } else {
            $redirectUrl = 'admin/iosconfig';
            $flashMessage = "Atleast select one file Updated";
            $validation = array();
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect($redirectUrl)->withInput()->withErrors($validator);
        }
        $sitesettings = $this->sitesettings;

        if (isset($request->androidFcmKey)) {
            $sitesettings->androidFcmKey = $request->androidFcmKey;
            $sitesettings->android_version = $request->android_version;
            $sitesettings->android_update = $request->android_update;
            $sitesettings->save();
        }

        if ((isset($request->ios_update) && isset($request->ios_version)) && (!isset($request->iosproduction) && !isset($request->iossandbox))) {
            $sitesettings->ios_update = $request->ios_update;
            $sitesettings->ios_version = $request->ios_version;
            $sitesettings->save();
        }

        if (isset($request->iossandbox) && !empty($request->iossandbox)) {
            $destinationPath = 'certificate/'; // upload path
            $extension = Input::file('iossandbox')->getClientOriginalExtension(); // getting image extension
            $fileName = 'ios_sandbox.' . $extension; // renameing image
            Input::file('iossandbox')->move($destinationPath, $fileName); // uploading file to given path
        }

        if (isset($request->iosproduction) && !empty($request->iosproduction)) {
            $destinationPath = 'certificate/'; // upload path
            $extension = Input::file('iosproduction')->getClientOriginalExtension(); // getting image extension
            $fileName = 'ios_production.' . $extension; // renameing image
            Input::file('iosproduction')->move($destinationPath, $fileName); // uploading file to given path
        }

        Session::flash('flash_message', $flashMessage);

        return redirect()->intended($redirectUrl);//('admin/sitesettings');
    }

    public function sendNotification()
    {
        $kreaitService = new  CloudMessagingService();
//        if (count($userdevicedet) > 0) {
//            foreach ($userdevicedet as $userdevice) {
//                $deviceToken = $userdevice->deviceToken;
//                $badge = $userdevice->badge;
//                $badge += 1;
//                $userdevice->badge = $badge;
//                $userdevice->deviceToken = $deviceToken;
//                $userdevice->save();
//                if (isset($deviceToken)) {
//                    $pushMessage['type'] = 'Admin_notification';
//                    $pushMessage['message'] = $_POST['notificationText'];
//                    $pushMessage['user_id'] = 0;
//                    $pushMessage['user_name'] = "Admin";
//                    $pushMessage['user_image'] = "";
//                    $pushMessage['chat_id'] = "";
//                    $pushNotification = json_encode($pushMessage);
//                    $myClass->pushnot($deviceToken, $pushNotification, $badge);
//                }
//            }
//        }
        $responce = $kreaitService->sendMessageToDevice(
            ['title' => 'Admin_notification',
                'body' => $_POST['notificationText']
            ]);

        if ($responce['status'] == 'error') {
            return 0;
        }
        return 1;

    }
}
