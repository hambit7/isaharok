<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Premium;
use Illuminate\Http\Request;
use App\Models\Sitesettings;
use Session;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\User;
use App\Models\Premiumpayment;
use App\Jobs\PremiumCanseledJob;

class PremiumController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sitesettingModel = Sitesettings::findOrFail(1);
        $sitecurrency = $sitesettingModel->sitecurrency;
        $sitecurrency = explode("-", $sitecurrency);
        $currency = $sitecurrency[1];


        if (isset($_GET['search'])) {
            $search = $_GET['search'];
            $premium = premium::where('price', 'like', "%$search%")
                ->orWhere('noofdays', 'like', "%$search%")
                ->orWhere('platform', 'like', "%$search%")
//                ->sortable()
                ->paginate(25);
        } else {
            $search = "";
            $premium = Premium::paginate(25);
        }

        return view('admin.premium.index', compact(['premium', 'currency', 'search']));
    }


    public function search()
    {

        $search = Input::get('search');


        if ($search) {

            $premium = premium::Where('price', 'like', "%$search%")
                ->orWhere('premiumname', 'like', "%$search%")
                ->orWhere('noofdays', 'like', "%$search%")
                ->orWhere('platform', 'like', "%$search%")
                ->orderBy('id', 'DESC')
                ->paginate(25);
        } else {
            $premium = Premium::orderBy('id', 'DESC')->paginate(25);
        }
        $sitesettingModel = Sitesettings::findOrFail(1);
        $sitecurrency = $sitesettingModel->sitecurrency;
        $sitecurrency = explode("-", $sitecurrency);
        $currency = $sitecurrency[1];

        return view('admin.premium.index', compact(['premium', 'search', 'currency']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sitesettingModel = Sitesettings::findOrFail(1);
        $sitecurrency = $sitesettingModel->sitecurrency;
        $sitecurrency = explode("-", $sitecurrency);
        $currency = $sitecurrency[1];
        return view('admin.premium.create', compact('currency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'premiumname' => 'required|max:35|min:3|unique:hz_premium|regex:/^[\pL\s\-]+$/',
            'price' => 'required|Numeric|between:1,9999999.99',
            'noofdays' => 'required|Numeric',
        ]);

        $requestData = $request->all();

        Premium::create($requestData);

        Session::flash('flash_message', 'Premium added!');

        return redirect('admin/premium');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $premium = Premium::findOrFail($id);
        $sitesettingModel = Sitesettings::findOrFail(1);
        $sitecurrency = $sitesettingModel->sitecurrency;
        $sitecurrency = explode("-", $sitecurrency);
        $currency = $sitecurrency[1];

        return view('admin.premium.show', compact(['premium', 'currency']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $premium = Premium::findOrFail($id);
        $sitesettingModel = Sitesettings::findOrFail(1);
        $sitecurrency = $sitesettingModel->sitecurrency;
        $sitecurrency = explode("-", $sitecurrency);
        $currency = $sitecurrency[1];

        return view('admin.premium.edit', compact(['premium', 'currency']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'premiumname' => 'required|max:35|min:3|regex:/^[\pL\s\-]+$/|unique:hz_premium,premiumname,' . $id,
            'price' => 'required|Numeric|between:1,9999999.99',
            'noofdays' => 'required|Numeric',
            'market_id' => 'required',
        ]);
        $requestData = $request->all();

        $premium = Premium::findOrFail($id);

        $premium->update($requestData);

        Session::flash('flash_message', ' Premium Updated.!');

        return redirect('admin/premium');
    }

    public function addPremium(Request $request)
    {
        $user = User::find($request->get('id'));
        $premiumStatus = $request->get('premium');
        $premiumSize = $request->get('premium_size') * 86400;
        $premiumFrom = time();
        $premiumTo = $premiumFrom + $premiumSize;

        $responce = $user->update([
            'premiumstatus' => $premiumStatus,
            'premium_from' => $premiumFrom,
            'premium_to' => $premiumTo,
        ]);

        if ($premiumStatus == '1') {

            $premiumpayment = Premiumpayment::updateOrCreate(
                ['userid' => $request->get('id')],
                ['premiumid' => 9,
                    'createddate' => $premiumFrom,
                    'status' => "active",
                ]
            );

            $job = (new PremiumCanseledJob($user, $premiumpayment))->delay($premiumSize);
            $this->dispatch($job);
        }
        echo json_encode($responce);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $id = $request->get('id');
        try {
            Premium::destroy($id);

            Session::flash('flash_message', 'Premium deleted!');

            return redirect('admin/premium');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('flash_message', "Premium already linked with some payments, can't be deleted");

            return redirect('admin/premium');
        }

    }
}
