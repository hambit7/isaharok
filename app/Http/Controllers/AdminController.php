<?php

namespace App\Http\Controllers;

use App\Models\Sitesettings;
use App\Models\Admin;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Intervention\Image\Facades\Image;
use Response;

//use Intervention\Image\Image as Img;

class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Check for the admin is already logged in or not.
     * If admin already logged in, then redirect to dashboard
     * else show the login page for admin
     *
     */
    protected function loginView()
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->intended('/admin/dashboard');
        } else {
            $siteSettings = Sitesettings::findorfail(1);
            return view('login', ['siteSettings' => $siteSettings]);
        }
    }

    /**
     * Check if the Admin login credentials are correct
     * and takes necessary action according to the given credentials
     *
     * @param  array $data
     */
    protected function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (Auth::guard('admin')->attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            //echo "Authenticated User: ".Auth::guard('admin')->user();
            return redirect()->intended('/admin/dashboard');
        } else {
            //echo "authFailed";
            //echo "</br>";
            //echo "hash for ".$password.": ".Hash::make($password);
            $request->session()->flash('flash_message', 'Email or Password Incorrect');
            //Session::flash('flash_message', 'Email or Password Incorrect');

            return redirect()->intended('/admin');
        }
    }

    protected function resizeimage($image, $width, $height)
    {
        //$imageresize = Image::make('uploads/'.$image)->resize($width, $height)->save('uploads/resized/56157.png');
        $imageresize = Image::make('uploads/user/' . $image)->fit($width, $height, function ($constraint) {
            $constraint->upsize();
        });
        return $imageresize->response();
    }

    protected function resizechatimage($image, $width, $height)
    {
        //$imageresize = Image::make('uploads/'.$image)->resize($width, $height)->save('uploads/resized/56157.png');
        $imageresize = Image::make('uploads/chat/' . $image)->fit($width, $height, function ($constraint) {
            $constraint->upsize();
        });
        return $imageresize->response();
    }
}
