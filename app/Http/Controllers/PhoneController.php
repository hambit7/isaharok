<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function verifyPhoneNumber(Request $request)
    {
        $phoneHash = $request->get('phone');

        $users = User::where('phone_is_verified', 0)->get();

        $user = $users->where('phoneHash', $phoneHash)
            ->first();

        if (!is_null($user)) {
            $user->update([
                'phone_is_verified' => true
            ]);
            return $this->respondOK([], "Phone is  verified");
        }
        return $this->respondError([], 100, "Phone is not verified");
    }
}
