<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Premium;
use App\Models\Helppages;
use App\Models\Premiumpayment;
use App\Models\Landingpagesettings;
use Session;

class UserController extends Controller
{
    /**
     * Handles the landing page request
     * Serves the langing page with the admin
     * configured data from admin panel
     *
     */
    public function landingPage()
    {
        $landingpagesettings = Landingpagesettings::findOrFail(1);

        $helppageModel = Helppages::where('homepagevisibility', 1)->get();

        return view('welcome', compact('landingpagesettings', 'helppageModel'));
    }

    /**
     * Handles the help page request
     * Serves the appropriate help page with the admin
     * configured data from admin panel
     *
     */
    public function helpPage($page)
    {
        $helpPage = str_replace("-", " ", $page);

        $helppageModel = Helppages::where('title', 'Like', $helpPage)->first();
        $allHelpPages = Helppages::get();

        if (count($helppageModel) > 0) {
            $landingpagesettings = Landingpagesettings::findOrFail(1);

            return view('helppage', compact('landingpagesettings', 'helppageModel', 'allHelpPages'));
        } else {
            return redirect('/');
        }
    }

    public function getUserStatus()
    {
        return auth()->id();
    }

    /**
     * Handles the cron job for
     * changing the active status to expired
     * if the premium is expired
     *
     */
    public function premiumCron()
    {
        $premiumModel = Premium::get();
        foreach ($premiumModel as $premium) {
            $premiumId = $premium->id;
            $premiumPeriod = $premium->noofdays;

            $todayTimeStamp = time();
            $daysBefore = strtotime('-' . $premiumPeriod . ' days', time());

            $premiumProcessModel = Premiumpayment::where([['premiumid', $premiumId], ['status', 'LIKE', 'active']])->
            where('createddate', '<', $daysBefore)->get();
            $userIds = array();
            $premiumIds = array();
            foreach ($premiumProcessModel as $premiumProcess) {
                $userIds[] = $premiumProcess->userid;
                $premiumIds[] = $premiumProcess->id;
            }

            if (!empty($userIds)) {
                User::whereIn('id', $userIds)->update(['premiumstatus' => 0, 'agestatus' => 0, 'invisiblestatus' => 0, 'adsstatus' => 0, 'distancestatus' => 0]);
                Premiumpayment::whereIn('id', $premiumIds)->update(['status' => 'expired']);
                unset($userIds);
                unset($premiumIds);
            }
        }
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int $id
     * @return Response
     */
    public function setlang($lang)
    {

        /*Session::put('locale', $lang);
        app()->setLocale(Session::get('locale'));
        //echo Session::get('locale');die;
        App::setLocale($lang);
        $language = App::getLocale();
        return \Redirect::back();*/

        Session::set('locale', $lang);
        return \Redirect::back();

        //return view('/admin');
    }

}

?>
