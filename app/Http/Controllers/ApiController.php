<?php

namespace App\Http\Controllers;

use App;
use App\Classes\MyClass;
use App\Models\Ads;
use App\Models\Chat;
use App\Models\Interests;
use App\Models\Message;
use App\Models\Peoplefor;
use App\Models\Premium;
use App\Models\Premiumpayment;
use App\Models\Reporting;
use App\Models\Requests;
use App\Models\Sitesettings;
use App\Models\User;
use App\Models\Userdevices;
use App\Models\Visitors;
use Braintree;
use Config;
use Hash;
use Mail;
use Uuid;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\BlockedUser;
use App\Jobs\creatingUserPushMessage;
use Validator;
use App\Models\Like;
use App\Models\Gesture;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('authuser',
            ['only' =>
                ['anyVisitprofile',
                    'anyVisitors',
                    'anyMatch',
                    'anyUnmatch',
                    'anyFavorite',
                    'anyPaypremium',
                    'anyManualpaypremium',
                    'anyDeleteaccount',
                    'anyReport',
                    'anyAdddeviceid',
                    'anyGetpeople',
                    'anyDeleteimage',
                    'anySetfilter',
//                    'anySetprofile',
                    'anyProfile',
                    'anyBlockchat',
                    'anyCreatechat',
                    'anyGetchat',
                    'anyGetchatmessage',
                    'anySendmessage',
                    'anyOnlinestatus',
                    'anyGetcounts',
                    'anyClearchat',
                    'anyUnfriend',
                    'anyRtcmessage',
                    'anyMissedcall',
                    'anyChangepassword',
                    'anyIosmissedalert',
                    'anyGetsupportid',
//                    'anyAddImage',
                    'anyAddLike'
                ]]
        );
    }

    public function anyAdmindatas()
    {
        $sitesettings = Sitesettings::first();

        if (!is_null($sitesettings)) {
            $peoplefors = Peoplefor::all()->toArray();
            $interests = Interests::all()->toArray();

            $resultarray = [
                'peoplefor' => $peoplefors,
                'interests' => $interests,
                'ad_unit_id' => Ads::find(1)->value('adsclient'),
                'temppayment_disable' => $sitesettings->app_payment,
                'android_version' => "$sitesettings->android_version",
                'android_update' => $sitesettings->android_update,
                'ios_version' => "$sitesettings->ios_version",
                'ios_update' => $sitesettings->ios_update,
            ];
            $resultarray = json_encode($resultarray);
            echo '{"status":"true","result":' . $resultarray . '}';
            return;
        }
        return $this->respondError([], 103, "Sitesettings is empty");

    }

    public function anyAppoftheday()
    {
        $sitesettings = Sitesettings::find(1);

        if ($sitesettings) {

            $array = $sitesettings::all()->toArray();
            $arr = $array[0];

            if ($sitesettings->apptitle != "") {
                $resultarray['title'] = $arr['apptitle'];
                $resultarray['description'] = $arr['appdesc'];
                $resultarray['view_url'] = $arr['appurl'];
                $resultarray['view_url_ios'] = $arr['appiosurl'];
            } else {
                $resultarray['title'] = "";
                $resultarray['description'] = "";
                $resultarray['view_url'] = "";
                $resultarray['view_url_ios'] = "";
            }

            for ($i = 1; $i <= count($arr); $i++) {

                if (!empty($arr["appimage$i"])) {
                    $resultarray['images'][] = url('/uploads/appimage/' . $arr["appimage$i"]);
                };
            }
            $resultarray = json_encode($resultarray);
            echo '{"status":"true","result":' . $resultarray . '}';
            return;
        }
        return $this->respondError([], 103, "Data is empty");
    }

    public function anyPremiumlist(Request $request)
    {
        $platform = $request->get('platform');
        $premiums = Premium::where('platform', $platform)->get();
        $siteSettings = Sitesettings::findorfail(1);
        $currency = $siteSettings->sitecurrency;
        $paymentType = $siteSettings->paymenttype;
        $licenseToken = $siteSettings->licensetoken;
        if (!is_null($premiums)) {
            foreach ($premiums as $key => $premium) {
                $resultarray[$key]['id'] = $premium->id;
                $resultarray[$key]['name'] = $premium->premiumname;
                $resultarray[$key]['price'] = $premium->price;
                $resultarray[$key]['days'] = $premium->noofdays;
                $resultarray[$key]['market_id'] = $premium->market_id;
            }
            $resultarray = json_encode($resultarray);
            echo '{"status":"true","currency":"' . $currency . '","payment":"' . $paymentType . '","license_key":"' . $licenseToken . '","result":' . $resultarray . '}';
            return;
        }
        return $this->respondError([], 500, "Sorry, Something went to be wrong");

    }

    public function anyConvertjsonpeoples($userId = null, $search_key = null)
    {
        if ($userId == null) {
            $userId = $_POST['user_id'];
        }
        if (isset($search_key) && $search_key != "") {
            $userModel = User::where([['id', $userId], ['userstatus', "1"], ['username', 'like', "%$search_key%"]])
                ->first();
        } else {
            $userModel = User::where([['id', $userId]])
                ->first();
        }

        if (!is_null($userModel)) {
            $userDetails = [];
            $userDetails['user_id'] = $userModel->id;
            $userDetails['user_name'] = $userModel->username;
//            $userDetails['send_match'] = $this->getFriendStatus($userId, $userModel->id);
            $userDetails['gender'] = $userModel->gender;
            $userDetails['age'] = $userModel->age;
            $userDetails['bio'] = $userModel->bio;
            $userDetails['lat'] = $userModel->latitude;
            $userDetails['lon'] = $userModel->longitude;
            $userDetails['token'] = $userModel->token;
            $userDetails['userstatus'] = $userModel->userstatus;
            $userDetails['phone'] = $userModel->phone;
            $userDetails['timestamp'] = $userModel->timestamp;
            if (isset($_POST['timestamp'])) {
                $currentTime = $_POST['timestamp'];

                if ($userModel->onlinestatus == 0) {
                    $onlineStatus = $userModel->onlinestatus;
                } else {
                    $onlineTimestamp = $userModel->onlinetimestamp;
                    $onlineSince = round(abs($currentTime - $onlineTimestamp) / 60, 2);
                    if ($onlineSince > 15) {
                        $onlineStatus = 0;
                        $onlineStatusChangeUsers = $userModel->id;
                    } else {
                        $onlineStatus = $userModel->onlinestatus;
                    }
                }
                $userDetails['online'] = $onlineStatus;

                if (isset($onlineStatusChangeUsers)) {
                    User::find($onlineStatusChangeUsers)->update(['onlinestatus' => 0]);
                }
            } else {
                $userDetails['online'] = $userModel->onlinestatus;
            }
            $userDetails['location'] = $userModel->location;
            $userDetails['info'] = $userModel->info;
            $userDetails['interest'] = $userModel->interest;
            if ($userModel->profileimage == "") {
                $userDetails['user_image'] = $userModel->gender == "male" ? url('/uploads/user/men.png') : url('/uploads/user/women.png');
            } else {
                $userDetails['user_image'] = url('/uploads/user/' . $userModel->profileimage);
            }
            if ($userModel->images == "") {
                $userDetails['images'][] = $userModel->gender == "male" ? url('/uploads/user/men.png') : url('/uploads/user/women.png');

            } else {
                $userDetails['images'] = $userModel->images;
            }
            $userDetails['show_age'] = $userModel->agestatus == null ? $userDetails['show_age'] = "" : $userModel->agestatus;
            $userDetails['show_distance'] = $userModel->distancestatus == null ? "" : $userModel->distancestatus;
            $userDetails['invisible'] = $userModel->invisiblestatus == null ? "" : $userDetails['invisible'] = $userModel->invisiblestatus;
            $userDetails['report'] = $this->getReportStatus($userId, $userModel->id);
            $premiumStatus = isset($userModel->premiumstatus) && $userModel->premiumstatus == 1 ? "true" : "false";
            $userDetails['premium_member'] = $premiumStatus;
            return $userDetails;
        }
        return '';
    }

    public function anyUnmatch(Request $request)
    {
        $params = $request->only([
            'user_id',
            'blocked_user_id'
        ]);
        $model = BlockedUser::create($params);
        if ($model) {
            return $this->respondOK([], "User is blocked");
        }
        return $this->respondError([], 500, "Sorry, Something went to be wrong");
    }

    public function anyFavorite()
    {
        if (isset($_POST['favorite_user_id']) && $_POST['favorite_user_id'] != "") {
            $userId = $_POST['user_id'];
            $favoriteUserId = $_POST['favorite_user_id'];

            $chatModel = Chat::where([['userid', $userId], ['chatuserid', $favoriteUserId]])
                ->orwhere([['userid', $favoriteUserId], ['chatuserid', $userId]])
                ->first();
            if (!is_null($chatModel)) {
                if ($chatModel->userid == $userId) {
                    if ($chatModel->fav_userid == "0") {
                        $chatModel->fav_userid = $userId;
                        $message = 'Favorited Successfully';
                    } else {
                        $chatModel->fav_userid = 0;
                        $message = 'Unfavorited Successfully';
                    }
                } elseif ($chatModel->chatuserid == $userId) {
                    if ($chatModel->fav_chatuser == "0") {
                        $chatModel->fav_chatuser = $userId;
                        $message = 'Favorited Successfully';
                    } else {
                        $chatModel->fav_chatuser = 0;
                        $message = 'Unfavorited Successfully';
                    }
                }
                $chatModel->save();
                return $this->respondOK([], $message);
            }
            return $this->respondError([], 500, "Something went to be wrong");
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }

    public function anyGenerateclienttoken()
    {
        $sitesettings = Sitesettings::first();
        $braintree_settings = $sitesettings->braintree_settings;
        $braintree_settings = json_decode($braintree_settings, true);
        $paymenttype = $braintree_settings['brainTreeType'];
        $merchantid = $braintree_settings['brainTreeMerchantId'];
        $publickey = $braintree_settings['brainTreePublicKey'];
        $privatekey = $braintree_settings['brainTreePrivateKey'];
        Braintree\Configuration::environment($paymenttype);
        Braintree\Configuration::merchantId($merchantid);
        Braintree\Configuration::publicKey($publickey);
        Braintree\Configuration::privateKey($privatekey);
        $clientToken = Braintree\ClientToken::generate();
        if ($clientToken && $clientToken != "") {
            echo '{"status":"true","token":"' . $clientToken . '"}';
            return;
        }
        echo '{"status":"false","message":"Token cannot be created now, Sorry!"}';
        return;
    }

    public function anyPaypremium(Request $request)
    {
        $userId = $request->get('user_id');
        $amount = $request->get('price');
        $nonce = $request->get("pay_nonce");
        $sitesettings = Sitesettings::first();
        $braintree_settings = $sitesettings->braintree_settings;
        $braintree_settings = json_decode($braintree_settings, true);
        $paymenttype = $braintree_settings['brainTreeType'];
        $merchantid = $braintree_settings['brainTreeMerchantId'];
        $publickey = $braintree_settings['brainTreePublicKey'];
        $privatekey = $braintree_settings['brainTreePrivateKey'];
        Braintree\Configuration::environment($paymenttype);
        Braintree\Configuration::merchantId($merchantid);
        Braintree\Configuration::publicKey($publickey);
        Braintree\Configuration::privateKey($privatekey);
        $result = Braintree\Transaction::sale(['amount' => $amount, 'paymentMethodNonce' => $nonce]);
        $result1 = Braintree\Transaction::submitForSettlement($result->transaction->id);
        if ($result->success || !is_null($result->transaction) && $result1->success == "1") {
            $premiumid = $request->get('premium_id');

            $premiumpayment = new Premiumpayment();
            $premiumpayment->premiumid = $premiumid;
            $premiumpayment->userid = $userId;
            $premiumpayment->createddate = time();
            $premiumpayment->status = "active";
            $premiumpayment->save();

            $userModel = User::find($userId);
            $userModel->premium_from = time();
            $userModel->premiumstatus = 1;
            $userModel->save();
            /* push notification */
            $this->anySendnotification($userId, $userId, "premium_purchase", "Your premium purchase has been made successful");

            return $this->respondOK([], "Premium subscription was activated successfully");
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }

    public function anyManualpaypremium(Request $request)
    {
        $userId = $request->get('user_id');

        if (($request->get('premium_id'))) {
            $premiumpayment = new Premiumpayment();
            $premiumpayment->premiumid = $request->get('premium_id');
            $premiumpayment->userid = $userId;
            $premiumpayment->createddate = time();
            $premiumpayment->status = "active";
            $premiumpayment->save();

            $userModel = User::find($userId);
            $userModel->premium_from = time();
            $userModel->premiumstatus = 1;
            $userModel->save();

            $premiumModel = Premium::find($request->get('premium_id'));

            /* push notification */
            $this->anySendnotification($userId, $userId, "premium_purchase", "Cheers! Your premium status is activated. Chat beautiful girls and have fun ;)");
            $job = (new App\Jobs\PremiumCanseledJob($userModel, $premiumpayment))->delay($premiumModel->noofdays * 3600);
            $this->dispatch($job);
            return $this->respondOK([], "Premium subscription was activated successfully");
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }

    public function anyDeleteaccount(Request $request)
    {
        $userdata = User::find($request->get('user_id'));
        if (!is_null($userdata)) {
            $userdata->userstatus = "0";
            $userdata->save();
            return $this->respondOK([], "User account disabled");
        }
        return $this->respondError([], 103, "User not found!");
    }

    public function anyReport(Request $request)
    {
        $userid = $request->get('user_id');
        $reportuserid = $request->get('report_user_id');
        $reporting = Reporting::where([['userid', $userid], ['reportuserid', $reportuserid]])->first();
        if (is_null($reporting)) {
            $reporting = new Reporting();
            $reporting->userid = $userid;
            $reporting->reportuserid = $reportuserid;
            $reporting->save();
            return $this->respondOK([], "Reported successfully");
        }
        $reporting->delete();
        return $this->respondOK([], "Undo reported");
    }

    public function anyAdddeviceid(Request $request)
    {
        $params = [];
        $params = array_add($params, 'deviceToken', $request->get('device_token'));
        $params = array_add($params, 'deviceId', $request->get('device_id'));
        $params = array_add($params, 'type', $request->get('device_type'));
        $params = array_add($params, 'mode', $request->get('device_mode'));

        $devicesModel = Userdevices::firstOrNew([
            'user_id' => $request->get('user_id'),
        ]);

        $devicesModel->fill($params);
        if ($devicesModel->save()) {
            return $this->respondOK([], "Registered successfully");
        }
        return $this->respondError([], 500, "Something went wrong, please try again later");

    }

    public function anyPushsignout(Request $request)
    {
        $deviceId = $request->get('device_id');

        $userdevicedet = Userdevices::where('deviceId', $deviceId)->get();

        if (!is_null($userdevicedet)) {
            Userdevices::where('deviceId', $deviceId)->delete();
        }
        return $this->respondOK([], "Unregistered successfully");
    }

    public function anyLogin(Request $request)
    {
        $socialId = $request->get('id');
        $email = $request->get('email');
        $userDetails = User::where('facebookid', $socialId)->orWhere('email', $email)->first();
        $jsonData['status'] = 'true';
        $jsonData['availability'] = $userDetails ? "true" : "false";
        $jsonString = json_encode($jsonData);
        echo $jsonString;
        return;
    }

    public function anySignup(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'email' => 'email|required|unique:hz_users',
                'name' => 'required',
                'phone' => 'required',
                'age' => 'required|numeric',
                'password' => 'required|min:8',
                'type' => 'required',
                'gender' => 'required',
            ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $jsonData['status'] = 'false';
            $jsonData['message'] = $errors->first();
            echo json_encode($jsonData);
            return;
        }
        $loginType = $request->get('type');
        $userName = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $password = Hash::make($request->get('password'));
        $gender = $request->get('gender');
        $searchGender = $gender == 'male' ? "women" : "men";
        $socialId = $request->get('id') ?: "";
        $images = $request->get('images') ?: "";
        $bio = $request->get('bio') ?: "";
        $age = $request->get('age') ?: 0;
        $lat = $request->get('lat') ?: 0;
        $lon = $request->get('lon') ?: 0;
        $location = $request->get('location') ?: 0;
        $info = $request->get('info') ?: 0;

        $sitesettingModel = Sitesettings::first();
        $ads = Ads::first();
        $peoplefor = Peoplefor::first();

        $userModel = User::where('email', $email)->first();

        if (is_null($userModel)) {
            $userModel = new User();
            $userModel->id = Uuid::generate();
        }

        $userModel->firstname = $userName;
        $userModel->username = $userName;
        $userModel->email = $email;
        $userModel->password = $password;
        $userModel->age = $age;
        $userModel->location = $location;
        $userModel->latitude = $lat;
        $userModel->longitude = $lon;
        $userModel->agestatus = 0;
        $userModel->distancestatus = 0;
        $userModel->premiumstatus = 0;
        $userModel->phone = $phone;
        if ($loginType == "facebook") {
            $userModel->facebookid = $socialId;
        }
        $userModel->gender = $gender;
        $userModel->peoplefor = $peoplefor->id;
        $userModel->info = $info;
        /* TOKEN GENERATION */
        $tok = Uuid::generate();
        $userModel->token = $tok;
        $userModel->userstatus = 0;
        $userModel->images = $images;
        $userModel->bio = $bio;
        $userModel->notifications = '{"messageNotification":"1","likeNotification":"1"}';
        $searchage = "18" . '-' . $sitesettingModel->maximumage;
        $userModel->searchfilter = '{"gender":"' . $searchGender . '","people_for":"' . $peoplefor->id . '","age":"' . $searchage . '","distance":"100"}';
        $userModel->save();
//
//        if ($gender == 'male') {
//            $job = (new creatingUserPushMessage($gender, $userName, $userModel->id));
//            $this->dispatch($job);
//        }

        $this->anySendmail();

        $userDetail = $this->anyConvertjsonpeoples($userModel->id);


        $userModel = User::findOrFail($userModel->id);
        $notification = !empty($userModel->notifications) ? json_decode($userModel->notifications, true) : "";
        $messageNotification = isset($notification['messageNotification']) && $notification['messageNotification'] == 1 ? "true" : "false";
        $likeNotification = isset($notification['likeNotification']) && $notification['likeNotification'] == 1 ? "true" : "false";
        $userDetail['message_notification'] = $messageNotification;
        $userDetail['like_notification'] = $likeNotification;
        $userDetail['hide_ads'] = "false";
        $userDetail['admin_enable_ads'] = $sitesettingModel->googleads == 1 ? "true" : "false";
        $userDetail['ad_unit_id'] = $ads->adsclient;
        $jsonData['status'] = 'true';
        $jsonData['peoples'] = $userDetail;
        echo json_encode($jsonData);
        return;
    }

    public function anyGetaccess(Request $request)
    {
        $userModel = User::find($request->get('id'));

        if (!is_null($userModel)) {
            if ($userModel->unauthorized < 5) {
                $postHashkey = $request->get('hash');
                $timeStamp = base64_encode($request->get('timestamp'));
                $encodeToken = base64_encode($userModel->token);
                $validatehash = $encodeToken . $timeStamp;
                $setexpiryTime = strtotime("+7 day");
                $siginattempts = $userModel->unauthorized;
                /* validate hash key*/
                if ($validatehash == $postHashkey) {
                    $jsonData['status'] = 'true';
                    $jsonData['access_token'] = substr($validatehash, 0, 12);
                    $jsonData['expiry'] = $setexpiryTime;
                    $userModel->timestamp = $setexpiryTime;
                    $userModel->access_token = substr($validatehash, 0, 12);
                    $userModel->save();
                    $jsonString = json_encode($jsonData);
                    echo $jsonString;
                    return;
                }
                $userModel->unauthorized = $siginattempts + 1;
                $userModel->save();
                return $this->respondError([], 401, "Unauthorized access.");
            }
            $userModel->userstatus = 0;
            $userModel->save();
            return $this->respondError([], 403, "Account has been locked. Kindly contact admin to get unlocked");
        }
        return $this->respondError([], 401, "Unauthorized access.");
    }

    public function anyEmaillogin(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $userModel = User::whereEmail($email)->first();

        if (!is_null($userModel)) {
            if ($userModel->phone_is_verified == false) {
                return $this->respondError([
                    'phone' => $userModel->phone,
                    'gender' => $userModel->gender,
                    'id' => $userModel->id,
                ], 100, 'Phone not verified');
            }

            if ($userModel->gender == 'female' && empty($userModel->images)) {
                return $this->respondError([], 102, 'Photo is empty');
            }
            if (Hash::check($password, $userModel->password)) {
                if ($userModel->userstatus == 1) {
                    $sitesettingModel = Sitesettings::findOrFail(1);
                    $ads = Ads::findOrFail(1);
                    $userDetail = $this->anyConvertjsonpeoples($userModel->id);
                    $userId = $userModel->id;
                    $userModel = User::findOrFail($userId);
                    $notification = !empty($userModel->notifications) ? json_decode($userModel->notifications, true) : "";
                    $messageNotification = isset($notification['messageNotification']) && $notification['messageNotification'] == 1 ? "true" : "false";
                    $likeNotification = isset($notification['likeNotification']) && $notification['likeNotification'] == 1 ? "true" : "false";
                    $userDetail['message_notification'] = $messageNotification;
                    $userDetail['like_notification'] = $likeNotification;
                    $userDetail['hide_ads'] = "false";
                    $userDetail['admin_enable_ads'] = $sitesettingModel->googleads == 1 ? "true" : "false";
                    $userDetail['ad_unit_id'] = $ads->adsclient;
                    $jsonData['status'] = 'true';
                    $jsonData['peoples'] = $userDetail;
                    $jsonString = json_encode($jsonData);
                    echo $jsonString;
                    return;
                }
                return $this->respondError([], 104, 'Your account has been disabled');
            }
        }
        return $this->respondError([], 105, 'Email or Password Incorrect');
    }

    /**
     *  Restriction Details
     *  -- filter
     *  -- location
     *  -- user should not sent any request
     *  -- invisible check for destination user
     */
    public function anyGetpeople(Request $request)
    {

        $userId = $request->get('user_id');
        $blokedUser = BlockedUser::where('user_id', $userId)
            ->get(['blocked_user_id'])->toArray();

        $likedUser = Like::where('author_id', $userId)
            ->get(['recepient_id'])->toArray();

        $userModel = User::find($userId);

        if (!is_null($userModel)) {
            $isMan = $userModel->gender == 'male';

            $offset = !is_null($request->get('offset')) ? $request->get('offset') : 0;
            $limit = !is_null($request->get('limit')) ? $request->get('limit') : 10;

            $peopleModel = User::where('invisiblestatus', 0)
                ->where('userstatus', 1)
                ->when($isMan, function ($query) {
                    return $query->where('images', '<>', '');
                })
                ->when($isMan, function ($query) {
                    return $query->where('phone_is_verified', '1');
                })
//                ->when($isWoman, function ($query)  {
//                    return $query->where('premiumstatus', '1');
//                })
                ->where('id', '<>', $userId)
                ->where('gender', '<>', $userModel->gender)
                ->where('email', '<>', 'Support@sahar.club')
                ->whereNotIn('id', $blokedUser)
                ->whereNotIn('id', $likedUser)
                ->whereBetween('age', [18, 80])
                ->latest()
                ->offset($offset)
                ->limit($limit)
                ->get();

            $peopleModel->sortBy(function ($people) use ($userModel) {
                $earthRadius = 6378245;
                $pointLatitude = deg2rad($userModel->latitude);
                $pointLongitude = deg2rad($userModel->longitude);
                $cityLatitude = deg2rad($people->latitude);
                $cityLongitude = deg2rad($people->longitude);

                return round($earthRadius * acos(cos($pointLatitude) * cos($cityLatitude) * cos($pointLongitude - $cityLongitude) + sin($pointLatitude) * sin($cityLatitude)));

            })->values()->all();

            if (!is_null($peopleModel)) {
                $userDetails = [];
                $onlineStatusChangeUsers = [];
                foreach ($peopleModel as $key => $people) {
                    $userDetails[$key] = array();
                    $userDetails[$key]['user_id'] = $people->id;
                    $userDetails[$key]['user_name'] = $people->username;
                    $userDetails[$key]['gender'] = $people->gender;
                    $userDetails[$key]['age'] = $people->age;
                    $userDetails[$key]['bio'] = $people->bio;
                    $userDetails[$key]['lat'] = $people->latitude;
                    $userDetails[$key]['lon'] = $people->longitude;
                    if ($people->onlinestatus == 0) {
                        $onlineStatus = $people->onlinestatus;
                    } else {
                        $onlineTimestamp = $people->onlinetimestamp;
                        $onlineSince = round(abs(time() - $onlineTimestamp) / 60, 2);
                        if ($onlineSince > 15) {
                            $onlineStatus = 0;
                            $onlineStatusChangeUsers[] = $people->id;
                        } else {
                            $onlineStatus = $people->onlinestatus;
                        }
                    }
                    $userDetails[$key]['online'] = $onlineStatus;
                    $userDetails[$key]['location'] = $people->location;
                    $userDetails[$key]['info'] = $people->info;
                    $userDetails[$key]['interest'] = $people->interest;
                    $userDetails[$key]['user_image'] = url('/uploads/user/' . $people->profileimage);
                    if ($people->images == "") {
                        $userDetails[$key]['images'][] = $people->gender == "male" ? url('/uploads/user/1498823355.png') : url('/uploads/user/1498815144.png');
                    } else {
                        $userDetails[$key]['images'] = $people->images;
                    }
                    $userDetails[$key]['show_age'] = $people->agestatus == 1 ? "true" : "false";
                    $userDetails[$key]['show_location'] = $people->distancestatus == 1 ? "true" : "false";
                    $userDetails[$key]['invisible'] = $people->invisiblestatus == 1 ? "true" : "false";
                    $userDetails[$key]['premium_member'] = $people->premiumstatus == 1 ? "true" : "false";
                    $userDetails[$key]['report'] = $this->getReportStatus($userId, $people->id);
                }
                if (!empty($onlineStatusChangeUsers)) {
                    User::whereIn('id', $onlineStatusChangeUsers)->update(['onlinestatus' => 0]);
                }
                $jsonData['status'] = 'true';
                $jsonData['peoples'] = $userDetails;
                $jsonString = json_encode($jsonData);
                echo $jsonString;
                return;
            }
        }
        return $this->respondError([], 404, "Users not found");
    }

    public function getReportStatus($userid, $reportuserid)
    {
        $reporting = Reporting::where([['userid', $userid], ['reportuserid', $reportuserid]])->first();
        $responce = $reporting ? true : false;
        return $responce;
    }

    public function getBasefromURL($image_url_name)
    {
        return basename($image_url_name);
    }

    public function getImagefromURL($imageUrl, $type = 'user')
    {
        $finalPath = $type == "item" ? "uploads/items/" : "uploads/user/";

        $pathinfo = pathinfo($imageUrl);
        if (isset($pathinfo['extension'])) {
            $extensionData = explode('?', $pathinfo['extension']);
            $newname = time() . "." . $extensionData[0];
        } else {
            $newname = time();
        }
        $raw = file_get_contents($imageUrl);
        if ($raw == false) {
            $ch = curl_init($imageUrl);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
            $raw = curl_exec($ch);
            curl_close($ch);
        }

        $fori = fopen($finalPath . $newname, 'wb');
        fwrite($fori, $raw);
        fclose($fori);
        chmod($finalPath . $newname, 0666);
        return $newname;
    }

    public function anyUploadimage(Request $request)
    {
        $type = $request->get('type');
        @$ftmp = $_FILES['images']['tmp_name'];
        @$oname = $_FILES['images']['name'];
        @$fname = $_FILES['images']['name'];
        @$fsize = $_FILES['images']['size'];
        @$ftype = $_FILES['images']['type'];

        $user_image_path = $type == 'chat' ? "uploads/chat/" : "uploads/user/";
        $ext = strrchr($oname, '.');
        if ($ext) {
            if (($ext != '.JPG' && $ext != '.PNG' && $ext != '.JPEG' && $ext != '.GIF' && $ext != '.jpg' && $ext != '.png' && $ext != '.jpeg' && $ext != '.gif') || $fsize > 200 * 1024 * 1024) {
            } else {
                if (isset($ftmp)) {

                    $randomImageName = MyClass::randomString(8);
                    $newname = $randomImageName . time() . $ext;
                    $newimage = $user_image_path . $newname;
                    $result = move_uploaded_file($ftmp, $newimage);
                    chmod($newimage, 0666);
                    if (empty($result)) {
                        $error["result"] = "There was an error moving the uploaded file.";
                        echo '{"status":"false","message":"Image cannot be uploaded"}';
                    } else {
                        echo '{"status":"true",
                        "Image":{
                            "Message":"Image Upload Successfully",
                            "Name" :"' . $newname . '",
                            "View_url" :"' . url('/' . $newimage) . '"
                        }
                    }';
                    }
                }
            }
        }
    }

    public function anyDeleteimage(Request $request)
    {
        $type = $request->get('type');
        $imageName = $request->get('image');
        $user_image_path = $type == 'chat' ? "uploads/chat/" : "uploads/user/";
        $imageUrl = $user_image_path . $imageName;
        if (unlink($imageUrl)) {
            return $this->respondOK([], "Deleted successfully");
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }


    public function anySetprofile()
    {
        $userId = $_POST['user_id'];
        if (isset($_POST['lat'])) {
            $lat = $_POST['lat'];
        }
        if (isset($_POST['lon'])) {
            $lon = $_POST['lon'];
        }
        if (isset($_POST['location'])) {
            $location = $_POST['location'];
        }
        if (isset($_POST['user_image'])) {
            $userImage = $_POST['user_image'];
        }
        if (isset($_POST['social_image'])) {
            $userImage = $_POST['social_image'];
        }
        if (isset($_POST['images'])) {
            $images = $_POST['images'];
        }

        $userModel = User::find($userId);
        if (!is_null($userModel)) {

            if (isset($_POST['people_for'])) {
                $userModel->peoplefor = $_POST['people_for'];
            }
            if (isset($lat)) {
                $userModel->latitude = $lat;
            }
            if (isset($lon)) {
                $userModel->longitude = $lon;
            }
            if (isset($location)) {
                $userModel->location = $location;
            }

            if (isset($userImage) && $userImage) {
                $userModel->profileimage = $this->getBasefromURL($userImage);
            }

            if (isset($_POST['social_image'])) {
                $userModel->profileimage = $this->getImagefromURL($userImage);
                $social_profile_image = url('/uploads/user/' . $userModel->profileimage);
            } else {
                $social_profile_image = "false";
            }
            if (isset($images)) {
                $userModel->images = $images;
            }
            if (isset($_POST['show_age'])) {
                $showAge = $_POST['show_age'] == 'true' ? 1 : 0;
                $userModel->agestatus = $showAge;
            }
            if (isset($_POST['show_location'])) {
                $showLocation = $_POST['show_location'] == 'true' ? 1 : 0;
                $userModel->distancestatus = $showLocation;
            }
            if (isset($_POST['invisible'])) {
                $invisible = $_POST['invisible'] == 'true' ? 1 : 0;
                $userModel->invisiblestatus = $invisible;
            }
            if (isset($_POST['hide_ads'])) {
                $hideAds = $_POST['hide_ads'] == 'true' ? 1 : 0;
                $userModel->adsstatus = $hideAds;
            }
            if (isset($_POST['message_notification'])) {
                $messageNotification = $_POST['message_notification'] == 'true' ? 1 : 0;
                $notification = !empty($userModel->notifications) ? json_decode($userModel->notifications, true) : "";
                $notification['messageNotification'] = $messageNotification;
                $userModel->notifications = json_encode($notification);
            }

            if (isset($_POST['like_notification'])) {
                $likeNotification = $_POST['like_notification'] == 'true' ? 1 : 0;
                $notification = !empty($userModel->notifications) ? json_decode($userModel->notifications, true) : "";
                $notification['likeNotification'] = $likeNotification;
                $userModel->notifications = json_encode($notification);
            }
            if (isset($_POST['interests'])) {
                $userModel->interest = $_POST['interests'];
            }
            if (isset($_POST['user_name'])) {
                $userModel->username = $_POST['user_name'];
            }

            if (isset($_POST['age'])) {
                $userModel->age = $_POST['age'];
            }

            if (isset($_POST['gender'])) {
                $userModel->gender = $_POST['gender'];
            }

            if (isset($_POST['bio'])) {
                $userModel->bio = $_POST['bio'];
            };
            $userModel->save();

            echo '{"status":"true","social_image":"' . $social_profile_image . '","message":"Profile changed successfully"}';
            return;
        }
        return $this->respondError([], 404, "User not found");
    }

    public function anyProfile()
    {
        $userId = $_POST['user_id'];
        if (isset($_POST['friend_id'])) {
            $userId = $_POST['friend_id'];
        }
        $currentTime = time();
        $userModel = User::findOrFail($userId);
        $sitesettingModel = Sitesettings::findOrFail(1);
        $ads = Ads::findOrFail(1);

        if (!empty($userModel)) {
            $notification = !empty($userModel->notifications) ? json_decode($userModel->notifications, true) : "";
            $messageNotification = isset($notification['messageNotification']) && $notification['messageNotification'] == 1 ? "true" : "false";
            $likeNotification = isset($notification['likeNotification']) && $notification['likeNotification'] == 1 ? "true" : "false";
            $userDetails['email'] = $userModel->email;
            $userDetails['user_id'] = $userModel->id;
            $userDetails['user_name'] = $userModel->username;
//            $userDetails['send_match'] = isset($_POST['friend_id']) ? $this->getFriendStatus($_POST['user_id'], $_POST['friend_id']) : "";
            $userDetails['report'] = '';
            $userDetails['gender'] = $userModel->gender;
            $userDetails['age'] = $userModel->age;
            $userDetails['bio'] = $userModel->bio;
            $userDetails['lat'] = $userModel->latitude;
            $userDetails['lon'] = $userModel->longitude;
            if ($userModel->onlinestatus == 0) {
                $onlineStatus = $userModel->onlinestatus;
            } else {
                $onlineTimestamp = $userModel->onlinetimestamp;
                $onlineSince = round(abs($currentTime - $onlineTimestamp) / 60, 2);
                if ($onlineSince > 15) {
                    $onlineStatus = 0;
                    $onlineStatusChangeUsers = $userModel->id;
                } else {
                    $onlineStatus = $userModel->onlinestatus;
                }
            }
            if (isset($onlineStatusChangeUsers)) {
                User::where('id', $onlineStatusChangeUsers)->update(['onlinestatus' => 0]);
            }
            $userDetails['online'] = $onlineStatus;
            $userDetails['location'] = $userModel->location;
            $userDetails['info'] = $userModel->info;
//            $userDetails['interest'] = $userModel->interest;
            $userDetails['user_image'] = $userModel->profileimage == "" ? url('/uploads/user/men.png') : url('/uploads/user/' . $userModel->profileimage);
            if ($userModel->images == "") {
                $userDetails['images'][] = $userModel->gender == "male" ? url('/uploads/user/men.png') : url('/uploads/user/women.png');
            } else {
                $userDetails['images'] = $userModel->images;
            }
            $userDetails['people_for'] = $userModel->peoplefor;
            $userDetails['show_age'] = $userModel->agestatus == 1 ? "true" : "false";
            $userDetails['show_location'] = $userModel->distancestatus == 1 ? "true" : "false";
            $userDetails['invisible'] = $userModel->invisiblestatus == 1 ? "true" : "false";
            $userDetails['hide_ads'] = $userModel->adsstatus == 1 ? "true" : "false";
            $userDetails['premium_member'] = $userModel->premiumstatus == 1 ? "true" : "false";
            if ($userModel->premiumstatus == 1) {
                $userDetails['premium_member'] = "true";
                $userPremiumDetails = Premiumpayment::where("userid", $userId)->where('status', 'Like', 'active')
                    ->orderBy('id', 'desc')
                    ->first();
                $premiumDetails = Premium::find($userPremiumDetails->premiumid);
                $premiumTimeLine = $premiumDetails->noofdays;
                $premiumValidityDate = strtotime("+$premiumTimeLine days", $userPremiumDetails->createddate);
                $userDetails['membership_valid'] = $premiumValidityDate;
            } else {
                $userDetails['premium_member'] = "false";
                $userDetails['membership_valid'] = "";
            }
            $userDetails['message_notification'] = $messageNotification;
            $userDetails['like_notification'] = $likeNotification;
            $userDetails['admin_enable_ads'] = $sitesettingModel->googleads == 1 ? "true" : "false";
            $userDetails['ad_unit_id'] = $ads->adsclient;
            $userDetails['filter'] = $userModel->searchfilter;
            $userDetailsJson = json_encode($userDetails);
            echo '{"status":"true","result":' . $userDetailsJson . '}';
            return;
        }
        return $this->respondError([], 404, "No user found");
    }

    public function anyBlockchat(Request $request)
    {
        $userId = $request->get('user_id');
        $blockUserId = $request->get('block_user_id');
        $chatModel = Chat::orwhere([['userid', $userId], ['chatuserid', $blockUserId]])->orwhere([['userid', $blockUserId], ['chatuserid', $userId]])->first();
        if (!is_null($chatModel)) {
            if ($chatModel->status == "0") {
                $chatModel->status = $userId;
                $message = "Unblocked successfully";
            } else {
                $chatModel->status = 0;
                $message = "Unblocked successfully";
            }
            $chatModel->save();
            return $this->respondOK([], $message);

        }
        return $this->respondError([], 500, "Something went to be wrong!");
    }

    public function anyCreatechat(Request $request)
    {
        $userId = $request->get('user_id');
        $friendId = $request->get('friend_id');
        $userModel = User::find($friendId);

        $chatDetails = Chat::orwhere([['userid', $userId], ['chatuserid', $friendId]])->orwhere([['userid', $friendId], ['chatuserid', '=', $userId]])->first();

        if (!is_null($chatDetails)) {
            $block = $chatDetails->status == 0 ? "false" : "true";
            $blocked_by_me = $chatDetails->status == $userId ? "true" : "false";
            echo '{"status":"true", "chat_id":"' . $chatDetails->id . '", "block":"' . $block . '", "blocked_by_me":"' . $blocked_by_me . '", "last_online":"' . $userModel->onlinetimestamp . '", "online":"' . $userModel->onlinestatus . '", "user_status":"' . $userModel->userstatus . '", "message":"Already a chat available"}';
        } else {
            $chatModel = new Chat;
            $chatModel->userid = $userId;
            $chatModel->chatuserid = $friendId;
            $chatModel->lastreplied = 0;
            $chatModel->lasttoread = 0;
            $chatModel->fav_userid = 0;
            $chatModel->fav_chatuser = 0;
            $chatModel->lastupdate = time();
            $chatModel->lastmessage = "";
            $chatModel->cdate = time();
            $chatModel->save();

            echo '{"status":"true", "chat_id":"' . $chatModel->id . '", "block":"false", "blocked_by_me":"false", "last_online":"' . $userModel->onlinetimestamp . '", "online":"' . $userModel->onlinestatus . '", "user_status":"' . $userModel->userstatus . '", "message":"Chat created successfully"}';
        }
    }

    public function anyGetchat()
    {
        $userId = $_POST['user_id'];
        $searchKey = $_POST['search_key'];
        $sort = $_POST['sort'];
        $currentTime = time();
        $offset = 0;
        $limit = 10;

        if (isset($_POST['offset'])) {
            $offset = $_POST['offset'];
        }
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        }
        //unread, online, favorite, all
        if ($sort == 'unread') {
            $chatModel = Chat::where('lasttoread', $userId)->with('sender')
                ->with('receiver')
                ->offset($offset)->limit($limit)->get();
        } elseif ($sort == 'online') {
            $sender[] = ['onlinestatus', '=', 1];
            $receiver[] = ['onlinestatus', '=', 1];

            $chatModel = Chat::where(function ($query) use ($userId) {
                $query->where('userid', '=', $userId)->orwhere('chatuserid', '=', $userId);
            })->with('sender')
                ->with('receiver')->whereHas('sender', function ($query) use ($sender) {
                    $query->where($sender);
                })->whereHas('receiver', function ($query) use ($receiver) {
                    $query->where($receiver);
                })->offset($offset)->limit($limit)->get();
        } elseif ($sort == 'favourite') {
            $chatModel = Chat::where(function ($query) use ($userId) {
                $query->where('fav_userid', '=', $userId)->orwhere('fav_chatuser', '=', $userId);
            })->with('sender')
                ->with('receiver')
                ->offset($offset)->limit($limit)->get();
        } else {
            if ($searchKey == 1) {
                $chatModel = Chat::where(function ($query) use ($userId) {
                    $query->where('userid', '=', $userId)->orwhere('chatuserid', '=', $userId);
                })->with('sender')
                    ->with('receiver')
                    ->orderBy('lastupdate', 'desc')
                    ->get();
            } else {
                $chatModel = Chat::where(function ($query) use ($userId) {
                    $query->where('userid', $userId)->orwhere('chatuserid', $userId);
                })->with('sender')
                    ->with('receiver')
                    ->offset($offset)->limit($limit)->orderBy('lastupdate', 'desc')
                    ->get();
            }
        }

        if (!is_null($chatModel)) {
            $resultArray['status'] = 'true';
            $resultArray['matches'] = [];
            $onlineStatusChangeUsers = [];

            foreach ($chatModel as $key => $chat) {

                $sender = $chat->sender;

                $resultArray['matches'][$key]['chat_id'] = $chat->id;
                if ($sender['id'] != $userId) {
//
                    $userModel = $chat->sender;
                } else {
                    $userModel = $chat->receiver;
                }

                $resultArray['matches'][$key]['user_id'] = $userModel['id'];
                $resultArray['matches'][$key]['user_name'] = $userModel['username'];
                $resultArray['matches'][$key]['gender'] = $userModel['gender'];
                $resultArray['matches'][$key]['user_image'] = url('/uploads/user/' . $userModel['profileimage']);
                if (!empty($chat->isDeleted)) {
                    $decode_del_users = json_decode($chat->isDeleted);
                    $deluserId = $decode_del_users[0];
                    $type = 'delete_enable';
                } else {
                    $deluserId = 0;
                    $type = 'delete_disable';
                }
                if ($deluserId != $userId && $type != 'delete_disable') {
                    $resultArray['matches'][$key]['message'] = $chat->lastmessage;
                    $resultArray['matches'][$key]['last_replied'] = $chat->lastreplied;
                    $resultArray['matches'][$key]['last_to_read'] = $chat->lasttoread;
                    $resultArray['matches'][$key]['clear_chat'] = 0;
                } else {
                    $messageModel = Message::where('chatid', $chat->id)
                        ->where('isDeleted', 0)
                        ->offset($offset)->limit($limit)->orderBy('id', 'desc')
                        ->get();
                    if (count($messageModel) > 0) {
                        $resultArray['matches'][$key]['message'] = $messageModel[0]->message;
                        if ($messageModel[0]->type == 1) {
                            $resultArray['matches'][$key]['type'] = "missed";
                        } else {
                            $resultArray['matches'][$key]['type'] = "";
                        }
                        $resultArray['matches'][$key]['last_replied'] = $chat->lastreplied;
                        $resultArray['matches'][$key]['last_to_read'] = $chat->lasttoread;
                        $resultArray['matches'][$key]['clear_chat'] = 0;
                    } else {
                        $resultArray['matches'][$key]['message'] = "";
                        $resultArray['matches'][$key]['type'] = "";
                        $resultArray['matches'][$key]['last_replied'] = 0;
                        $resultArray['matches'][$key]['last_to_read'] = 0;
                        $resultArray['matches'][$key]['clear_chat'] = 1;
                    }
                }
                $resultArray['matches'][$key]['chat_time'] = $chat->cdate;
                if ($chat->userid == $userId && $chat->fav_userid == $userId) {
                    $resultArray['matches'][$key]['favorite'] = "true";
                } elseif ($chat->chatuserid == $userId && $chat->fav_chatuser == $userId) {
                    $resultArray['matches'][$key]['favorite'] = "true";
                } else {
                    $resultArray['matches'][$key]['favorite'] = "false";
                }
                if ($userModel['onlinestatus'] == 0) {
                    $onlineStatus = $userModel['onlinestatus'];
                } else {
                    $onlineTimestamp = $userModel['onlinetimestamp'];
                    $onlineSince = round(abs($currentTime - $onlineTimestamp) / 60, 2);
                    if ($onlineSince > 15) {
                        $onlineStatus = 0;
                        $onlineStatusChangeUsers[] = $userModel['id'];
                    } else {
                        $onlineStatus = $userModel['onlinestatus'];
                    }
                }
                $resultArray['matches'][$key]['online'] = $onlineStatus;
                $resultArray['matches'][$key]['user_status'] = $userModel['onlinestatus'];
                $resultArray['matches'][$key]['last_online'] = $userModel['onlinetimestamp'];
                $resultArray['matches'][$key]['block'] = $chat->status == "0" ? "false" : "true";
                $resultArray['matches'][$key]['blocked_by_me'] = $chat->status == $userId ? "true" : "false";
            }

            if (!empty($onlineStatusChangeUsers)) {
                User::whereIn('id', $onlineStatusChangeUsers)->update(['onlinestatus' => 0]);
            }
            $result = json_encode($resultArray);
            echo $result;
            return;
        }
        return $this->respondError([], 404, "No chats found");
    }

    public function anyGetchatmessage()
    {
        $userId = $_POST['user_id'];
        $chatId = $_POST['chat_id'];

        $offset = isset($_POST['offset']) ? $_POST['offset'] : 0;
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 10;
        $chatModel = Chat::where('id', $chatId)->with('sender')
            ->with('receiver')
            ->first();

        if (!is_null($chatModel)) {
            if (!empty($chatModel->isDeleted)) {
                $decode_del_users = json_decode($chatModel->isDeleted);
                $deluserId = $decode_del_users[0];
            } else {
                $deluserId = 0;
            }
            if ($deluserId != $userId) {

                $messageModel = Message::where('chatid', $chatId)->offset($offset)->limit($limit)->orderBy('id', 'desc')
                    ->get();

                if ($chatModel->lasttoread == $userId) {
                    Chat::where('id', $chatId)->update(['lasttoread' => 0]);
                }
                if (!is_null($messageModel)) {
                    $resultArray['status'] = 'true';
                    $resultArray['chat_id'] = $chatId;
                    $resultArray['chats'] = array();
                    foreach ($messageModel as $key => $message) {
                        if ($message->type == 1) {
                            $resultArray['chats'][$key]['type'] = "missed";
                        } else {
                            $resultArray['chats'][$key]['type'] = !empty($message->imagename) ? "image" : "text";
                        }
                        $resultArray['chats'][$key]['sender_id'] = $message->userid;
                        if ($chatModel->sender->id == $message->userid) {
                            $userModel = $chatModel->sender;
                        } else {
                            $userModel = $chatModel->receiver;
                        }
                        $resultArray['chats'][$key]['user_image'] = url('/uploads/user/' . $userModel->profileimage);
                        $resultArray['chats'][$key]['chat_time'] = $message->createddate;
                        $resultArray['chats'][$key]['message'] = $message->message;
                        $resultArray['chats'][$key]['upload_image'] = !empty($message->imagename) ? url('/uploads/chat/' . $message->imagename) : "";
                    }
                    $result = json_encode($resultArray);
                    echo $result;
                } else {
                    echo '{"status":"false","message":"No chat history found"}';
                }
            } else {
                $messageModel = Message::where('chatid', $chatId)->where('isDeleted', 0)
                    ->offset($offset)->limit($limit)->orderBy('id', 'desc')
                    ->get();

                if ($chatModel->lasttoread == $userId) {
                    Chat::where('id', $chatId)->update(['lasttoread' => 0]);
                }
                if (count($messageModel) > 0) {
                    $resultArray['status'] = 'true';
                    $resultArray['chat_id'] = $chatId;
                    $resultArray['chats'] = array();
                    foreach ($messageModel as $key => $message) {
                        if ($message->type == 1) {
                            $resultArray['chats'][$key]['type'] = "missed";
                        } else {
                            $resultArray['chats'][$key]['type'] = !empty($message->imagename) ? "image" : "text";
                        }
                        $resultArray['chats'][$key]['sender_id'] = $message->userid;
                        if ($chatModel
                                ->sender->id == $message->userid) {
                            $userModel = $chatModel->sender;
                        } else {
                            $userModel = $chatModel->receiver;
                        }
                        $resultArray['chats'][$key]['user_image'] = url('/uploads/user/' . $userModel->profileimage);
                        $resultArray['chats'][$key]['chat_time'] = $message->createddate;
                        $resultArray['chats'][$key]['message'] = $message->message;
                        $resultArray['chats'][$key]['upload_image'] = !empty($message->imagename) ? url('/uploads/chat/' . $message->imagename) : "";
                    }
                    $result = json_encode($resultArray);
                    echo $result;
                } else {
                    echo '{"status":"false","message":"No chat history found"}';
                }
            }
        } else {
            echo '{"status":"false","message":"No chat history found"}';
        }
    }

    public function anySendmessage(Request $request)
    {
        $chatId = $request->get('chat_id');
        $senderId = $request->get('sender_id');
        $receiverId = $request->get('receiver_id');
        $message = $request->get('message');
        $chatTime = $request->get('chat_time');
        $uploadedImage = $request->get('upload_image');

        $chatModel = Chat::where([['userid', $senderId], ['chatuserid', $receiverId], ['status', $senderId]])->orWhere([['userid', $senderId], ['chatuserid', $receiverId], ['status', $receiverId]])->orWhere([['userid', $receiverId], ['chatuserid', $senderId], ['status', $receiverId]])->orWhere([['userid', $receiverId], ['chatuserid', $senderId], ['status', $senderId]])->first();

        if (!is_null($chatModel)) {
            return $this->respondError([], 101, "User has blocked you");
        } else {
            if ($chatId == 0) {
                $chatModel = new Chat;
                $chatModel->userid = $senderId;
                $chatModel->chatuserid = $receiverId;
                $chatModel->lastreplied = $senderId;
                $chatModel->lasttoread = $receiverId;
                $chatModel->lastupdate = $chatTime;
                $chatModel->lastmessage = $message;
                $chatModel->cdate = $chatTime;
                $chatModel->save();
                $chatId = $chatModel->id;
            } else {
                Chat::where('id', $chatId)->update(['lasttoread' => $receiverId, 'lastreplied' => $senderId, 'lastupdate' => $chatTime, 'lastmessage' => $message]);
            }

            $messageModel = new Message;
            $messageModel->chatid = $chatId;
            $messageModel->userid = $senderId;
            $messageModel->message = $message;
            $messageModel->imagename = $uploadedImage;
            $messageModel->createddate = $chatTime;
            $messageModel->save();
            if (isset($messageModel->id)) {
                $receiverModel = User::where('id', $receiverId)->first();
                $notification = !empty($receiverModel->notifications) ? json_decode($receiverModel->notifications, true) : "";
                $messageNotification = isset($notification['messageNotification']) && $notification['messageNotification'] == 1 ? "1" : "0";
                if ($messageNotification == "1") {
                    /* push notification */
                    $this->anySendnotification($senderId, $receiverId, "chat_message", $message, $chatId);
                }
                return $this->respondOK([], "Message send successfully");
            }
            return $this->respondError([], 106, "Message cannot send");
        }
    }

    public function anyOnlinestatus(Request $request)
    {
        $userId = $request->get('user_id');
        $status = $request->get('status');
//        $timestamp = $request->get('timestamp');

        $userModel = User::find($userId);
        if (!is_null($userModel)) {
            $userModel->onlinestatus = $status;
            $userModel->onlinetimestamp = time();
            $userModel->save();
            return $this->respondOK([], "Status changed successfully");
        }
        return $this->respondError([], 500, "Something went to be wrong!");
    }

    public function anyGetcounts()
    {
        $userId = $_POST['user_id'];
        $deviceId = isset($_POST['device_id']) ? $_POST['device_id'] : "";
        $sender[] = ['userstatus', 1];

        $visitors = Visitors::where('visitorid', $userId)->where('unreadstatus', 1)
            ->with('sender')
            ->whereHas('sender', function ($query) use ($sender) {
                $query->where($sender);
            })
            ->count();

        $message = Chat::where('lasttoread', $userId)->count();
        $friendRequest = Requests::where('friendid', $userId)->where('status', 2)
            ->with('sender')->whereHas('sender', function ($query) use ($sender) {
                $query->where($sender);
            })->count();
        $friends = Requests::where('friendid', $userId)->orwhere('userid', $userId)->where('status', '1')
            ->with('sender')->whereHas('sender', function ($query) use ($sender) {
                $query->where($sender);
            })->count();

        if (!empty($deviceId)) {
            $deviceDetails = Userdevices::where('deviceId', $deviceId)->count();
        }
        $resultData['status'] = "true";
        $resultData['result']['visitors'] = $visitors;
        $resultData['result']['message'] = $message;
        $resultData['result']['friend_request'] = $friendRequest;
        $resultData['result']['friends'] = $friends;
        $resultData['result']['device_registered'] = isset($deviceDetails) && $deviceDetails > 0 ? "true" : "false";

        $result = json_encode($resultData);
        echo $result;
        return;
    }

    public function anyMobiletermsandcondition()
    {
        $siteSettings = Sitesettings::first();
        if (!is_null($siteSettings) || empty($siteSettings->termsandconditionsheading) || empty($siteSettings->termsandconditionscontent)) {
            $resultArray['status'] = 'true';
            $resultArray['result']['title'] = $siteSettings->termsandconditionsheading;
            $resultArray['result']['description'] = $siteSettings->termsandconditionscontent;
            $result = json_encode($resultArray);
            echo $result;
            return;
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }

    public function anyResetbadge(Request $request)
    {
        $deviceId = $request->get('device_id');
        $deviceDetails = Userdevices::where('deviceId', $deviceId)->first();
        if (!is_null($deviceDetails)) {
            $deviceDetails->badge = 0;
            $deviceDetails->save();
            return $this->respondOK([], "Badge Reseted Successfully");
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }

//New API for v3
    public function anyClearchat(Request $request)
    {
        $userId = $request->get('user_id');
        $userModel = User::find($userId);
        if (!is_null($userModel)) {
            $chatId = $request->get('chat_id');
            $chatModel = Chat::where('id', $chatId)->first();
            $deleteUser = $chatModel->isDeleted;
            $delUsrArr = array();
            $delUsrArr[0] = $userId;

            if ($deleteUser != "") {
                $decode_del_users = json_decode($deleteUser); // decode del  user id array
                array_push($decode_del_users, $userId);
                $decode_del_users2 = array_unique($decode_del_users);
                $usrCnt = count($decode_del_users2);
                if ($usrCnt > 1) {
                    Message::where('chatid', $chatId)->delete();
                    $chatModel->lastmessage = "";
                    $chatModel->isDeleted = null;
                } else {
                    $new_del_chat_user = json_encode($decode_del_users2);
                    $chatModel->isDeleted = $new_del_chat_user; //new del user id push in table
                    $messageDtl = Message::where('chatid', $chatId)->where('isDeleted', 0)
                        ->get();
                    foreach ($messageDtl as $key => $msgModel) {
                        $msgId = $msgModel->id;
                        $modelMsg = Message::where('id', $msgId)->first();
                        $modelMsg->isDeleted = 1;
                        $modelMsg->save();
                    }
                }
            } else {
                $new_del_user = json_encode($delUsrArr);
                $chatModel->isDeleted = $new_del_user; //new delete chat user id push table
                $messageDtl = Message::where('chatid', $chatId)->where('isDeleted', 0)
                    ->get();
                foreach ($messageDtl as $key => $messageDtls) {
                    $modelMsg = Message::where('id', $messageDtls->id)
                        ->first();
                    $modelMsg->isDeleted = 1;
                    $modelMsg->save();
                }
            }
            if ($chatModel->save()) {
                return $this->respondOK([], "successfully Deleted");
            }
        }
        return $this->respondError([], 500, "Something went to be wrong");
    }

    public function anyMissedcall(Request $request)
    {
        $userId = $request->get('user_id');
        $toId = $request->get('toId');
        $type = $request->get('type');
        $chatId = $request->get('chatId');
        $chatTime = $request->get('chatTime');
        Chat::where('id', $chatId)->update(['lasttoread' => $toId, 'lastupdate' => $chatTime]);

        $messageModel = new Message;
        $messageModel->userid = $userId;
        $messageModel->chatid = $chatId;
        $messageModel->message = $type;
        $messageModel->type = 1;
        $messageModel->createddate = $chatTime;

        if ($messageModel->save()) {
            return $this->respondOK([], "Missed call added");
        }
        return $this->respondError([], 500, "Sorry, Something went to be wrong");
    }

    public function anyChangepassword(Request $request)
    {
        $userId = $request->get('user_id');
        $old_password = $request->get('old_password');
        $new_password = Hash::make($request->get('new_password'));
        $userModel = User::where('id', $userId)->first();
        if (!is_null($userModel)) {
            if (Hash::check($old_password, $userModel->password)) {
                $userModel->password = $new_password;
                $userModel->save();
                return $this->respondOK([], "Password changed successfully");
            }
            return $this->respondError([], 107, "Old password is Incorrect");
        }
        return $this->respondError([], 500, "Sorry, Something went to be wrong");
    }

    public function anyForgotpassword(Request $request)
    {
        $email = $request->get('email');
        $userModel = User::where('email', $email)->first();
        if (!is_null($userModel)) {
            $secretCode = MyClass::randomString(8);
            $secretCodehash = Hash::make($secretCode);
            $userModel->password = $secretCodehash;
            $userModel->save();

            $this->anySendmail("emails.forgotpassword", "Forgot Password Received", $userModel, $secretCode);
            return $this->respondOK([], "Password reset mail has send to your registered account");

        }
        return $this->respondError([], 108, "Email Id not registered");
    }

    public function anyIosmissedalert(Request $request)
    {
        $userId = $request->get('user_id');
        $toId = $request->get('toId');
        $type = $request->get('type');
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : "";
        $userModel = User::where('id', $userId)->first();
        if (!is_null($userModel)) {
            $userdevicedet = Userdevices::where('user_id', $toId)->get();
            $receiverModel = User::where('id', $toId)->first();

            if (!empty($userdevicedet)) {
                foreach ($userdevicedet as $userdevice) {
                    $deviceToken = $userdevice->deviceToken;
                    $badge = $userdevice->badge;
                    $badge += 1;
                    $userdevice->badge = $badge;
                    $userdevice->deviceToken = $deviceToken;
                    $userdevice->save();
                    if (isset($deviceToken)) {
                        $pushMessage['type'] = $type;
                        $pushMessage['from_id'] = $userId;
                        $pushMessage['room_id'] = $room_id;
                        $pushNotification = json_encode($pushMessage);
                        $notification = !empty($receiverModel->notifications) ? json_decode($receiverModel->notifications, true) : "";
                        $messageNotification = isset($notification['messageNotification']) && $notification['messageNotification'] == 1 ? "1" : "0";
                        if ($messageNotification == "1") {
                            $pushNotifytype = strtolower($type);
                            MyClass::pushnot($deviceToken, $pushNotification, $badge, $pushNotifytype);
                        }
                    }
                }
            }
            return $this->respondOK([], "Call disconnected successfully");
        }
    }

    public function anySendmail($template, $sub, $userModel, $secretcode = null)
    {
        $sitesettingModel = Sitesettings::first();
        if (!is_null($sitesettingModel)) {
            $smtphost = $sitesettingModel->smtphost;
            $smtpport = $sitesettingModel->smtpport;
            $smtpusername = $sitesettingModel->smtpusername;
            $smtppassword = $sitesettingModel->smtppassword;
            if ($sitesettingModel->smtpenable == 1) {
                Config::set('mail.driver', 'smtp');
                Config::set('mail.port', $smtpport);
                Config::set('mail.host', $smtphost);
                Config::set('mail.username', $smtpusername);
                Config::set('mail.password', $smtppassword);
                Config::set('mail.encryption', 'ssl');
            } else {
                Config::set('mail.driver', 'sendmail');
            }
            return Mail::send($template, ['userModel' => $userModel, 'sitesettingModel' => $sitesettingModel, 'secretCode' => $secretcode], function ($message) use ($userModel, $sub, $sitesettingModel) {
                $message->subject($sub);
                $message->from($sitesettingModel->smtpusername, $sitesettingModel->sitename);
                $message->to($userModel->email);
            });
        }
    }

    public function anyVisitprofile(Request $request)
    {
        $userId = $request->get('user_id');
        $visitorId = $request->get('visit_user_id');
        $visitors = Visitors::firstOrNew([
            'userid' => $userId,
            'visitorid' => $visitorId,
            'unreadstatus' => 1,
        ]);
        $visitors->save(['created_at' => Carbon::now()]);
        return $this->respondOK([], "Visited successfully");
    }

    public function anyVisitors(Request $request)
    {
        $userId = $request->get('user_id');
        $offset = $request->get('offset') == "" ? 0 : $request->get('offset');
        $limit = $request->get('limit') == "" ? 100 : $request->get('limit');

        $visitors = Visitors::where('visitorid', $userId)
            ->groupBy('userid')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

        $visitorsId = [];
        $visitorsIds = Visitors::where('visitorid', $userId)->get();
        if (!is_null($visitorsIds)) {
            foreach ($visitorsIds as $vid) {
                $visitorsId[] = $vid->id;
            }
        }
        Visitors::whereIn('id', $visitorsId)
            ->update(['unreadstatus' => 0]);
        $resultarray = [];
        if (!is_null($visitors)) {
            $key1 = 0;
            foreach ($visitors as $key => $visitor) {
                $result = $this->anyConvertjsonpeoples($visitor->userid);
                if ($result != "") {
                    $resultarray[$key1] = $result;
                    $key1++;
                }
            }
            $resultarray = json_encode($resultarray);
            echo '{"status":"true","peoples":' . $resultarray . '}';
            return;
        }
        return $this->respondError([], 103, 'Data is empty!');
    }

    public function anySendnotification($userid, $notifyid, $notifytype, $notifiymessage, $chatId = null)
    {
        $cloudMessage = new App\Services\CloudMessagingService();
        $loginuserModel = User::where('id', $userid)->first();
        $userdevices = Userdevices::where('user_id', $notifyid)->get();

        $data = [
            'title' => 'Sahar',
            'type' => $notifytype,
            'message' => $notifiymessage,
            'user_id' => $userid,
            'user_name' => $loginuserModel->username,
            'user_image' => url('/uploads/user/' . $loginuserModel->profileimage),
            'chat_id' => $chatId == null ? "" : $chatId,
        ];
        if (!is_null($userdevices)) {
            $cloudMessage->sendMessageToDevice($data, $notifiymessage, $userdevices);
        }
    }

    public function anyVerifyphonenumber(Request $request)
    {
        $phoneHash = $request->get('phone');

        $users = User::where('phone_is_verified', 0)->get();
        $user = $users->where('phoneHash', $phoneHash)
            ->first();
        if (!is_null($user)) {
            $user->update([
                'phone_is_verified' => true
            ]);
            return $this->respondOK([], "Phone is  verified");
        }
        return $this->respondError([], 100, "Phone is not verified");
    }

    public function anyGirldeactivate(Request $request)
    {
        $id = $request->get('id');

        $model = User::find($id);

        if (!is_null($model)) {
            $model->update([
                'userstatus' => 0
            ]);
            return $this->respondOK([], 'success');
        }
        return $this->respondError([], 103, 'user not found');
    }

    public function anyGetsupportid()
    {
        $support = User::where('email', 'Support@sahar.club')->first();

        if (!is_null($support)) {
            return $this->respondOK([
                'supportId' => $support->id,
                'firstname' => $support->firstname,
                'image' => $support->profileimage
            ], 'success');
        }
        return $this->respondError([], 103, 'user not found');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAddlike(Request $request)
    {

        $model = new Like();

        $params = $request->only('author_id',
            'recepient_id');

        $validator = \Validator::make($params, [
            'author_id' => 'required|exists:hz_users,id',
            'recepient_id' => 'required|exists:hz_users,id',
        ]);

        if ($validator->fails()) {

            return $this->respondError([], 404, ($validator->errors()->first()));

        }
        $model->fill($params);
        $model->save();

        if ($this->checkBothLike($request->get('author_id'), $request->get('recepient_id'))) {
            return $this->respondOK(['user_id' => $request->get('recepient_id'),
                'chat_info' => $this->addChat($request->get('author_id'),
                    $request->get('recepient_id'))
            ], 'Ok');
        }
        return $this->respondOK([], 'Like added');

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyImages(Request $request)
    {
        if ($request->has('gender')) {
            $list = Gesture::where('gender', $request->get('gender'))->get();
        } else {
            $list = Gesture::all();

        }

        return $this->respondOK($list->toArray(), 'all images');
    }

    /**
     * @param Request $request
     */
    public function anyAddImage(Request $request)
    {
        $model = new App\Models\Image();

        $params = $request->only('user_id',
            'gesture_id',
            'image');

        $validator = \Validator::make($params, [
            'user_id' => 'required|exists:hz_users,id',
            'gesture_id' => 'required|exists:hz_gestures,id',
            'image' => 'required|file:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {

            return $this->respondError([], 404, ($validator->errors()->first()));

        }

        $url = $this->makeImage($request->file('image'));

        $model->fill(
            [
                'user_id' => $request->get('user_id'),
                'gesture_id' => $request->get('gesture_id'),
                'url' => $url,

            ]);
        $model->save();

        return $this->respondOK([], 'image saved');
    }

    /**
     * @param $author_id
     * @param $recepient_id
     * @return bool
     */
    protected function checkBothLike($author_id, $recepient_id)
    {
        $model = Like::where('author_id', $recepient_id)
            ->where('recepient_id', $author_id)
            ->first();

        if (is_null($model)) {
            return false;
        }
        return true;
    }

    protected function addChat($userId, $friendId)
    {
        $userModel = User::find($friendId);

        $chatDetails = Chat::orwhere([['userid', $userId], ['chatuserid', $friendId]])->orwhere([['userid', $friendId], ['chatuserid', '=', $userId]])->first();

        if (!is_null($chatDetails)) {
            $block = $chatDetails->status == 0 ? "false" : "true";
            $blocked_by_me = $chatDetails->status == $userId ? "true" : "false";
            $message = '{"status":"true", "chat_id":"' . $chatDetails->id . '", "block":"' . $block . '", "blocked_by_me":"' . $blocked_by_me . '", "last_online":"' . $userModel->onlinetimestamp . '", "online":"' . $userModel->onlinestatus . '", "user_status":"' . $userModel->userstatus . '", "message":"Already a chat available"}';
        } else {
            $chatModel = new Chat;
            $chatModel->userid = $userId;
            $chatModel->chatuserid = $friendId;
            $chatModel->lastreplied = 0;
            $chatModel->lasttoread = 0;
            $chatModel->fav_userid = 0;
            $chatModel->fav_chatuser = 0;
            $chatModel->lastupdate = time();
            $chatModel->lastmessage = "";
            $chatModel->cdate = time();
            $chatModel->save();

            $message = '{"status":"true", "chat_id":"' . $chatModel->id . '", "block":"false", "blocked_by_me":"false", "last_online":"' . $userModel->onlinetimestamp . '", "online":"' . $userModel->onlinestatus . '", "user_status":"' . $userModel->userstatus . '", "message":"Chat created successfully"}';
        }
        return $message;
    }


}
