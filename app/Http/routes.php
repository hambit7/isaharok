<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array('uses' => 'UserController@landingPage'));

Route::get('/admin', array('uses' => 'AdminController@loginView'));

Route::post('/admin', array('uses' => 'AdminController@login'));

Route::controller('api', 'ApiController');
Route::get('/adminlogout', function () {
    Auth::guard('admin')->logout();
    return redirect('/admin');
});

Route::get('resize/{image}/{w}/{h}', array('uses' => 'AdminController@resizeimage'));
Route::get('resizechat/{image}/{w}/{h}', array('uses' => 'AdminController@resizechatimage'));

Route::get('setlanguage/{id}', 'UserController@setlang');

Route::resource('admin/posts', 'Admin\\PostsController');
Route::resource('admin/peoplefor/index', 'Admin\\PeopleforController');
Route::resource('admin/peoplefor', 'Admin\\PeopleforController');
//Route::resource('admin/interests/index', 'Admin\\InterestsController');
//Route::resource('admin/interests', 'Admin\\InterestsController');
//Route::resource('admin/interests/', 'Admin\\InterestsController');

Route::resource('admin/peoplefor/', 'Admin\\PeopleforController');
Route::resource('admin/premium', 'Admin\\PremiumController');
Route::resource('admin/premium/index', 'Admin\\PremiumController');
Route::resource('admin/premium', 'Admin\\PremiumController');
Route::post('admin/premium/delete', 'Admin\\PremiumController@destroy');
Route::post('admin/premium/add', 'Admin\\PremiumController@addPremium');
Route::post('admin/premium/remove', 'Admin\\PremiumController@removePremium');

Route::get('admin/writer/users', 'Admin\\WriterController@createUsersList');

Route::post('admin/updatesitesettings/', 'Admin\\SitesettingsController@updateSitesettings');
Route::post('admin/updateadssettings/', 'Admin\\SitesettingsController@updateAdssettings');
//Route::resource('admin/ads', 'Admin\\AdsController');
Route::resource('admin/users', 'Admin\\UsersController');
Route::resource('admin/approved', 'Admin\\UsersController@approved');
Route::resource('admin/unapproved', 'Admin\\UsersController@unapproved');
Route::post('admin/users/delete', 'Admin\UsersController@destroyUser');

Route::resource('admin/reportedusers', 'Admin\\UsersController@reportedusers');


// @latest update
Route::get('admin/changestatus/{id}/{status}', 'Admin\\UsersController@changestatus');
Route::get('admin/ignorereporting/{reportid}', 'Admin\\UsersController@ignorereporting');
Route::get('admin/destroy/{id}', 'Admin\\UsersController@ignorereporting');
// @latest update
Route::resource('admin/changestatus', 'Admin\\UsersController@changestatus');
Route::resource('admin/ignorereporting', 'Admin\\UsersController@ignorereporting');
Route::resource('admin/appoftheday', 'Admin\\SitesettingsController@appoftheday');

Route::post('admin/approved', 'Admin\\UsersController@searchapproved');
Route::post('admin/unapproved', 'Admin\\UsersController@searchunapproved');
Route::post('admin/reportedusers', 'Admin\\UsersController@searchreportedusers');

Route::get('admin/smtpsettings/', 'Admin\\SitesettingsController@smtpsettings');
Route::get('admin/sitesettings/', 'Admin\\SitesettingsController@sitesettings');
Route::get('admin/pushsettings/', 'Admin\\SitesettingsController@pushsettings');
Route::get('admin/braintreesettings/', 'Admin\\SitesettingsController@braintreesettings');
Route::get('admin/termsandcondition/', 'Admin\\SitesettingsController@termsandcondition');
Route::get('admin/adssettings/', 'Admin\\SitesettingsController@adssettings');
Route::post('admin/updatesitesettings/', 'Admin\\SitesettingsController@updateSitesettings');
Route::post('admin/updateappoftheday/', 'Admin\\SitesettingsController@updateappoftheday');
Route::post('admin/updatepushsettings/', 'Admin\\SitesettingsController@updatePushSettings');
Route::post('admin/premium', 'Admin\\PremiumController@search');
Route::post('admin/peoplefor', 'Admin\\PeopleforController@searchpeople');

Route::get('admin/profile/', 'Admin\\SitesettingsController@adminprofile');
Route::post('admin/updateprofile/', 'Admin\\SitesettingsController@updateprofile');


Route::get('admin/dashboard/', 'Admin\\DashboardController@index');
Route::resource('admin/premiumuser', 'Admin\\UsersController@premiumuser');
Route::post('admin/premiumuser', 'Admin\\UsersController@searchpremium');


Route::post('admin/updatelandingpage/', 'Admin\\LandingpagesettingsController@updateLandingPage');
Route::resource('admin/landingpage/generalsettings', 'Admin\\LandingpagesettingsController@generalSettings');
Route::resource('admin/landingpage/mobileappconfig', 'Admin\\LandingpagesettingsController@mobileAppConfig');
Route::resource('admin/landingpage/sociallinkconfig', 'Admin\\LandingpagesettingsController@socialLinkConfig');

Route::resource('admin/girls', 'Admin\GirlsController');
Route::resource('admin/gallery', 'Admin\GalleryController');
Route::resource('admin/content', 'Admin\ContentController');
Route::delete('admin/content', 'Admin\ContentController@destroy');
Route::post('admin/girls/activate', 'Admin\GirlsController@activateAccount');

Route::resource('admin/helppages', 'Admin\\HelppagesController');
Route::resource('admin/helppages/index', 'Admin\\HelppagesController');

Route::get('helppage/{page}', array('uses' => 'UserController@helpPage'));
Route::get('premiumcron', array('uses' => 'UserController@premiumCron'));

Route::get('admin/androidconfig/', 'Admin\\DashboardController@androidConfig');
Route::get('admin/iosconfig/', 'Admin\\DashboardController@iosConfig');
Route::resource('admin/mobileappconfig/', 'Admin\\DashboardController@mobileAppConfig');
Route::get('admin/viewapproveduser/{id}', 'Admin\\UsersController@showapproveduser');
Route::get('admin/viewpremiumuser/{id}', 'Admin\\UsersController@showpremiumuser');
Route::get('admin/viewunapproveduser/{id}', 'Admin\\UsersController@showunapproveduser');
Route::post('admin/sendnotification/', 'Admin\\DashboardController@sendNotification');

Route::get('admin/inappsettings/', 'Admin\\SitesettingsController@inappsettings');
Route::get('admin/payment/', 'Admin\\SitesettingsController@paymentsettings');






