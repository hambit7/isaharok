<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewUserEvent extends Event
{
    use SerializesModels;

    public $gender;
    public $name;
    public $userId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($gender, $name, $userId)
    {
        $this->gender = $gender;
        $this->name = $name;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
