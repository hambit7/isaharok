<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;

class AccountActivatePushMessagingEvent extends Event
{
    use SerializesModels;

    public $model;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $model)
    {

        $this->model = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
